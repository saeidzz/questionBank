package ir.rabbit.group.questionbank.controller.questions;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteMatchingQuestion;
import ir.rabbit.group.questionbank.model.compositeId.UserMatchingQuestionId;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.question.MatchingNode;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteMatchingQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.question.MatchingNodeService;
import ir.rabbit.group.questionbank.service.abstraction.question.MatchingQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
public class MatchingQuestionController {
    @Autowired
    CourseInfoService courseInfoService;
    @Autowired
    UserService userService;
    @Autowired
    MatchingQuestionService matchingQuestionService;

    @Autowired
    MatchingNodeService matchingNodeService;

    @Autowired
    FavoriteMatchingQuestionService favoriteMatchingQuestionService;


    @GetMapping("/privileged/question/addMatchingQuestion")
    public String addMatchingQuestion(Model model) {
        CourseInfo courseInfo = new CourseInfo();
        MatchingQuestion matchingQuestion = new MatchingQuestion();
        matchingQuestion.setContent("محتوی سوال را وارد نمایید.");
        matchingQuestion.setScore(1.0f);
        matchingQuestion.setCourseInfo(courseInfo);
        model.addAttribute("matchingQuestion", matchingQuestion);
        return "matchingQuestion/addMatchingQuestion";
    }

    @PostMapping(value = "/privileged/question/addMatchingQuestion")
    public String addMatchingQuestionPost(@Valid @ModelAttribute("matchingQuestion") MatchingQuestion matchingQuestion, BindingResult result, HttpServletRequest request, Model model) {
        //  if(request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

        if (result.hasErrors()) {
            return "matchingQuestion/addMatchingQuestion";
        }
        Long ExistCourseId = courseInfoService.ExistCourse(matchingQuestion.getCourseInfo());
        Long uuid = 0l;
        if (ExistCourseId != uuid) {
            matchingQuestion.setCourseInfo(courseInfoService.getCourseInfoById(ExistCourseId));
        } else if (ExistCourseId == uuid) {
            courseInfoService.addCourseInfo(matchingQuestion.getCourseInfo());
        }
        matchingQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));
        matchingQuestionService.addMatchingQuestion(matchingQuestion);

        return "redirect:/privileged/question/addMatchingNode/" + matchingQuestion.getId();
        //  }
        //   else {
        //     model.addAttribute("tryAgain","لطفا عبارت امنیتی را با دقت وارد نمایید!!!");

//            return "matchingQuestion/addMatchingQuestion";
        //      }
    }

    @GetMapping(value = "/privileged/question/addMatchingNode/{matchingQuestionId}")
    public String addMatchingNode(@PathVariable("matchingQuestionId") Long matchingQuestionId, Model model) {

        MatchingNode matchingNode = new MatchingNode();
        matchingNode.setMatchingQuestion(matchingQuestionService.getQuestionById(matchingQuestionId));
        model.addAttribute("matchingQuestionId", matchingQuestionId);
        model.addAttribute("matchingNode", matchingNode);
        return "matchingQuestion/addMatchingNode";

    }

    @PostMapping(value = "/privileged/question/addMatchingNode/{matchingQuestionId}")
    public String addMatchingNode(@PathVariable("matchingQuestionId") Long matchingQuestionId, @ModelAttribute("MatchingNode") MatchingNode matchingNode, HttpServletRequest request) {
        MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(matchingQuestionId);
        matchingNode.setMatchingQuestion(matchingQuestion);
        matchingNodeService.addMatchingnode(matchingNode);

        if (request.getParameter("isItLastOne").toLowerCase().equals("no")) {
            return "redirect:/privileged/question/addMatchingNode/" + matchingQuestionId;
        } else {
            return "redirect:/privileged/MatchingQuestionInventory";
        }
    }

    @GetMapping("/privileged/MatchingQuestionInventory")
    public String matchingQuestionInventory(Model model, HttpServletRequest request) {
        String userName = request.getRemoteUser();
        List<MatchingQuestion> matchingQuestions = matchingQuestionService.getQuestionByOwnerUser(userService.getUserByUserName(userName).getId());
        model.addAttribute("matchingQuestions", matchingQuestions);

        return "matchingQuestion/matchingQuestionInventory";
    }

    @GetMapping("/public/question/viewMatchingQuestion/{MatchingQuestionId}")
    public String viewMatchingQ(@PathVariable("MatchingQuestionId") Long MatchingQuestionId, Model model, HttpServletRequest request) {
        MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(MatchingQuestionId);
        User user = userService.getUserByUserName(request.getRemoteUser());

        List<MatchingNode> matchingNodeList = matchingNodeService.getAllmatNodesByMatchingQId(MatchingQuestionId);
        model.addAttribute("matchingNodes", matchingNodeList);
        model.addAttribute("matchingQuestion", matchingQuestion);
        return "matchingQuestion/viewMatchingQuestion";
    }

    @GetMapping("/public/question/viewMatchingQuestion/{MatchingQuestionId}/{examId}")
    public String viewMatchingQ2(@PathVariable("MatchingQuestionId") Long MatchingQuestionId, @PathVariable("examId") String examId, Model model, HttpServletRequest request) {
        MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(MatchingQuestionId);
        List<MatchingNode> matchingNodeList = matchingNodeService.getAllmatNodesByMatchingQId(MatchingQuestionId);
        model.addAttribute("matchingNodes", matchingNodeList);
        model.addAttribute("matchingQuestion", matchingQuestion);

        return "matchingQuestion/viewMatchingQuestion";
    }

    @GetMapping("/privileged/question/editMatchingQuestion/{MQid}")
    public String editMatchingQuestion(@PathVariable("MQid") Long MQid, Model model, HttpServletRequest request) {
        MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(MQid);
        User user = userService.getUserByUserName(request.getRemoteUser());

        if (matchingQuestion.getUser().equals(user)) {
            model.addAttribute(matchingQuestion);
            return "matchingQuestion/editMatchingQuestion";

        } else {
            user.setEnabled(false);
            userService.editUser(user);
            return "redirect:/";
        }
    }

    @GetMapping("/privileged/question/editMatchingQuestion")
    public String editMatchingQuestionPost(@Valid @ModelAttribute("matchingQuestion") MatchingQuestion matchingQuestion, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "matchingQuestion/editMatchingQuestion";
        }
        Long ExistOrNot = courseInfoService.ExistCourse(matchingQuestion.getCourseInfo());
        Long uuid = 0l;

        if (ExistOrNot.equals(uuid)) {
            courseInfoService.addCourseInfo(matchingQuestion.getCourseInfo());
        } else {
            matchingQuestion.setCourseInfo(courseInfoService.getCourseInfoById(ExistOrNot));
        }
        matchingQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));

        matchingQuestionService.editQuestion(matchingQuestion);
        return "redirect:/privileged/MatchingQuestionInventory";
    }

    @GetMapping("/privileged/question/deleteMatchingQuestion/{MQid}")
    public String deleteMatchingQuestion(@PathVariable("MQid") Long MQid, HttpServletRequest request) {

        MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(MQid);
        User user = userService.getUserByUserName(request.getRemoteUser());
        if (matchingQuestion.getUser().equals(user)) {
            matchingQuestionService.deleteQuestion(matchingQuestion);
        } else {
            user.setEnabled(false);
            userService.editUser(user);
            return "redirect:/";
        }

        return "redirect:/privileged/MatchingQuestionInventory";
    }

    @GetMapping("/public/user/home/MatchingQuestion/public")
    public String getUserMatchingQuestions(Model model, HttpServletRequest request) {
        List<MatchingQuestion> matchingQuestions = matchingQuestionService.getPublicMQuestions();
        List<String> favoriteMQuestionIDs = favoriteMatchingQuestionService.getFavoriteMQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteMQuestionIDs", favoriteMQuestionIDs);
        model.addAttribute("matchingQuestions", matchingQuestions);

        return "user/matchingQuestions";

    }

    @PostMapping(value = "/privileged/addMQuestionToFavorites")
    public @ResponseBody
    String addFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {

        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteMatchingQuestion matchingQuestion = new FavoriteMatchingQuestion();
            matchingQuestion.setUserMatchingQuestionId(UserMatchingQuestionId.of(QID, userId));
            favoriteMatchingQuestionService.addToMyFavoriteMatchingQuestion(matchingQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }

    @PostMapping(value = "/privileged/removeMQuestionFromFavorites")
    public @ResponseBody
    String deleteFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {
        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteMatchingQuestion matchingQuestion = new FavoriteMatchingQuestion();
            matchingQuestion.setUserMatchingQuestionId(UserMatchingQuestionId.of(QID, userId));
            favoriteMatchingQuestionService.deleteFromMyFavoriteMatchingQuestion(matchingQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }

}
