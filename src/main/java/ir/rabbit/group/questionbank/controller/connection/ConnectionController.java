package ir.rabbit.group.questionbank.controller.connection;

import io.swagger.annotations.Api;
import ir.rabbit.group.questionbank.model.connection.UserHasConnections;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.connections.UserHasConnectionsService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "connection")
public class ConnectionController {
    @Autowired
    UserService userService;

    @Autowired
    UserHasConnectionsService userHasConnectionsService;


    @GetMapping("/public/searchPeoples")
    public String getAllPeoples(Model model, HttpServletRequest request) {
        List<User> usersList = userService.getAllUsers();
        User currentUser = userService.getUserByUserName(request.getRemoteUser());
        List<User> doesNotAcceptedFollowings = userHasConnectionsService.getDoesNotAcceptedFollowings(currentUser.getId());
        List<User> acceptedFollowings = userHasConnectionsService.getAcceptedFollowings(currentUser.getId());

        model.addAttribute("users", usersList);
        model.addAttribute("doesNotAcceptedFollowings", doesNotAcceptedFollowings);
        model.addAttribute("acceptedFollowings", acceptedFollowings);
        model.addAttribute("currentUser", currentUser);


        return "connection/userList";
    }

    @GetMapping("/public/connectedToYou")
    public String getAllThisConnectedTo(Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        List<User> doesNotAcceptedFollowers = userHasConnectionsService.getDoesNotAcceptedFollowers(user.getId());
        List<User> acceptedFollowers = userHasConnectionsService.getAcceptedFollowers(user.getId());
        List<User> doesNotAcceptedFollowings = userHasConnectionsService.getDoesNotAcceptedFollowings(user.getId());
        List<User> acceptedFollowings = userHasConnectionsService.getAcceptedFollowings(user.getId());

        model.addAttribute("doesNotAcceptedFollowings", doesNotAcceptedFollowings);
        model.addAttribute("acceptedFollowings", acceptedFollowings);

        model.addAttribute("doesNotAcceptedFollowers", doesNotAcceptedFollowers);
        model.addAttribute("acceptedFollowers", acceptedFollowers);

        return "connection/connectedToYou";

    }

    @GetMapping("/public/youAreConnectedTo")
    public String getAlConnectedToThis(Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        List<User> AcceptedFollowings = userHasConnectionsService.getAcceptedFollowings(user.getId());
        model.addAttribute("AcceptedFollowings", AcceptedFollowings);
        return "connection/youAreConnectedTo";

    }

    @PostMapping(value = "/public/connectTo")
    public @ResponseBody
    String follow(@RequestBody Long userId, HttpServletRequest request) {

        User thisWanaConnectTo = userService.getUserByUserName(request.getRemoteUser());
        User connectToThis = userService.getUserById(userId);

        if (!userHasConnectionsService.getDoesNotAcceptedFollowers(connectToThis.getId()).contains(thisWanaConnectTo)) {
            UserHasConnections userHasConnections = new UserHasConnections();
            userHasConnections.setAccepted(false);
            userHasConnections.setThisConnectedTo(thisWanaConnectTo);
            userHasConnections.setConnectedToThis(connectToThis);
            userHasConnectionsService.addUserHasConnections(userHasConnections);
            return "true";
        } else {
            return "false";
        }
    }

    @PostMapping(value = "/public/acceptConnectRequest")
    public @ResponseBody
    String acceptFollowRequest(@RequestBody Long userId, HttpServletRequest request) {
        User connectToThis = userService.getUserByUserName(request.getRemoteUser());
        User thisWanaConnectTo = userService.getUserById(userId);
        UserHasConnections userHasConnections = userHasConnectionsService.getUserHasConnectionByConnectorAndConnected(connectToThis.getId(), thisWanaConnectTo.getId());
        userHasConnections.setAccepted(true);
        userHasConnectionsService.updateUserHasConnections(userHasConnections);
        return "true";
    }

    @PostMapping(value = "/public/doNotAcceptConnectRequest")
    public @ResponseBody
    String doNotAcceptFollowRequest(@RequestBody Long userId, HttpServletRequest request) {
        User connectToThis = userService.getUserByUserName(request.getRemoteUser());
        User thisWanaConnectTo = userService.getUserById(userId);
        userHasConnectionsService.deleteUserHasConnectionByConnectorAndConnected(connectToThis.getId(), thisWanaConnectTo.getId());

        return "true";
    }


}
