package ir.rabbit.group.questionbank.controller.favorites;

import io.swagger.annotations.Api;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.service.abstraction.favorit.*;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "favorite")
public class FavoriteController {


    @Autowired
    FavoriteExamService favoriteExamService;

    @Autowired
    FavoriteTestQuestionService favoriteTestQuestionService;

    @Autowired
    FavoriteTrueFalseQuestionService favoriteTrueFalseQuestionService;

    @Autowired
    FavoriteMatchingQuestionService favoriteMatchingQuestionService;

    @Autowired
    FavoriteExplainedQuestionService favoriteExplainedQuestionService;

    @Autowired
    UserService userService;


    @GetMapping("/privileged/favorites")
    public String favorites() {
        return "favorites";
    }

    @GetMapping("/privileged/myFavoriteExams")
    public String favoriteExams(Model model, HttpServletRequest request) {
        List<Exam> examList = favoriteExamService.getFavoriteExamsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        List<String> favoriteExams = favoriteExamService.getFavoriteExamsIdOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteExamsId", favoriteExams);
        model.addAttribute("examList", examList);
        return "Exam/publicExamList";
    }

    @GetMapping("/privileged/myFavoriteTQuestions")
    public String favoriteTQ(Model model, HttpServletRequest request) {
        List<TestQuestion> testQuestions = favoriteTestQuestionService.getFavoriteTQuestionsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        List<String> favoriteTQuestions = favoriteTestQuestionService.getFavoriteTQIdOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteTQuestionsId", favoriteTQuestions);
        model.addAttribute("testQuestions", testQuestions);
        return "user/testQuestions";
    }

    @GetMapping("/privileged/myFavoriteMQuestions")
    public String favoriteMQ(Model model, HttpServletRequest request) {
        List<MatchingQuestion> matchingQuestions = favoriteMatchingQuestionService.getFavoriteMQuestionsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        List<String> favoriteMQuestionIDs = favoriteMatchingQuestionService.getFavoriteMQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteMQuestionIDs", favoriteMQuestionIDs);
        model.addAttribute("matchingQuestions", matchingQuestions);
        return "user/matchingQuestions";
    }

    @GetMapping("/privileged/myFavoriteTFQuestions")
    public String favoriteTFQ(Model model, HttpServletRequest request) {
        List<TrueFalseQuestion> trueFalseQuestions = favoriteTrueFalseQuestionService.getFavoriteTFQuestionsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        List<String> favoriteTFQuestionIDs = favoriteTrueFalseQuestionService.getFavoriteTFQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteTFQuestionIDs", favoriteTFQuestionIDs);
        model.addAttribute("trueFalseQuestions", trueFalseQuestions);
        return "user/trueFalseQuestions";
    }

    @GetMapping("/privileged/myFavoriteEQuestions")
    public String favoriteEQ(Model model, HttpServletRequest request) {
        List<ExplainedQuestion> explainedQuestions = favoriteExplainedQuestionService.getFavoriteEQuestionsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        List<String> favoriteEQuestionIDs = favoriteExplainedQuestionService.getFavoriteEQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteEQuestionIDs", favoriteEQuestionIDs);
        model.addAttribute("explainedQuestions", explainedQuestions);
        return "user/explainedQuestions";
    }


}
