package ir.rabbit.group.questionbank.controller.util;

public class ArgValidator {
    private ArgValidator() {
    }

    public static boolean isAnyNull(Object... args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i] == null) {
                return true;
            }
        }
        return false;
    }

    public static boolean isNull(Object o) {
        return o == null;
    }
}
