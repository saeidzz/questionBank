package ir.rabbit.group.questionbank.controller.questions;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteExplainedQuestion;
import ir.rabbit.group.questionbank.model.compositeId.UserExplainedQuestionId;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteExplainedQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.question.ExplainedQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
public class ExplainedQuestionController {

    @Autowired
    CourseInfoService courseInfoService;
    @Autowired
    ExplainedQuestionService explainedQuestionService;
    @Autowired
    UserService userService;

    @Autowired
    FavoriteExplainedQuestionService favoriteExplainedQuestionService;

    private Path path;

    private Path path1;

    @RequestMapping("/privileged/question/addExplainedQuestion")
    public String addExplainedQuestion(Model model) {
        ExplainedQuestion explainedQuestion = new ExplainedQuestion();
        explainedQuestion.setContent("متن سوال را جایگزین کنید");
        explainedQuestion.setScore(1.0f);
        model.addAttribute("explainedQuestion", explainedQuestion);

        return "ExplainedQuestion/addExplainedQuestion";
    }

    @GetMapping(value = "/privileged/question/addExplainedQuestion")
    public String addExplainedQuestionPost(@Valid @ModelAttribute("explainedQuestion") ExplainedQuestion explainedQuestion, BindingResult result, HttpServletRequest request, Model model) {
        //   if(request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {


        if (result.hasErrors()) {
            return "ExplainedQuestion/addExplainedQuestion";
        }
        Long ExistOrNot = courseInfoService.ExistCourse(explainedQuestion.getCourseInfo());
        Long uuid = 0l;

        if (ExistOrNot.equals(uuid)) {
            courseInfoService.addCourseInfo(explainedQuestion.getCourseInfo());
        } else {
            explainedQuestion.setCourseInfo(courseInfoService.getCourseInfoById(ExistOrNot));
        }

        explainedQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));
        explainedQuestionService.addQuestion(explainedQuestion);

        saveExplainedQImages(explainedQuestion, request);


        return "redirect:/privileged/explainedQuestionInventory";
        // }
        //   else{
        //     model.addAttribute("tryAgain","لطفا عبارت امنیتی را با دقت وارد نمایید!!!");
        //   return "ExplainedQuestion/addExplainedQuestion";
        // }
    }

    private void saveExplainedQImages(@Valid @ModelAttribute("explainedQuestion") ExplainedQuestion explainedQuestion, HttpServletRequest request) {
        if (explainedQuestion.getQuestionImage() != null) {
            MultipartFile questionImage = explainedQuestion.getQuestionImage();
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "/WEB-INF/res/questionImages/" + explainedQuestion.getId() + "-q.png");

            if (questionImage != null && !questionImage.isEmpty()) {
                try {
                    questionImage.transferTo(new File(path.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("question Image saving failed !");
                }
            }
        }
        if (explainedQuestion.getAsnswerImage() != null) {
            MultipartFile answerImage = explainedQuestion.getAsnswerImage();
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "/WEB-INF/res/questionImages/" + explainedQuestion.getId() + "-a.png");

            if (answerImage != null && !answerImage.isEmpty()) {
                try {
                    answerImage.transferTo(new File(path.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("answer Image saving failed !");
                }
            }
        }
    }

    @GetMapping("/privileged/explainedQuestionInventory/editExplainedQuestion/{id}")
    public String editExplainedQuestion(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
        ExplainedQuestion question = explainedQuestionService.getQuestionById(id);
        User users = userService.getUserByUserName(request.getRemoteUser());
        if (question.getUser().equals(users)) {
            model.addAttribute("explainedQuestion", question);
            return "ExplainedQuestion/editExplainedQuestion";
        } else {
            users.setEnabled(false);
            userService.editUser(users);
            return "redirect:/";
        }
    }

    @PostMapping(value = "/privileged/explainedQuestionInventory/editExplainedQuestion")
    public String editExplainedQuestionPost(@Valid @ModelAttribute("explainedQuestion") ExplainedQuestion explainedQuestion, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "ExplainedQuestion/editExplainedQuestion";
        }
        Long ExistOrNot = courseInfoService.ExistCourse(explainedQuestion.getCourseInfo());
        Long uuid = 0l;

        if (ExistOrNot.equals(uuid)) {
            courseInfoService.addCourseInfo(explainedQuestion.getCourseInfo());
        } else {
            explainedQuestion.setCourseInfo(courseInfoService.getCourseInfoById(ExistOrNot));
        }
        explainedQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));

        explainedQuestionService.addQuestion(explainedQuestion);

        saveExplainedQImages(explainedQuestion, request);


        return "redirect:/admin/explainedQuestionInventory";
    }

    @GetMapping("/privileged/question/deleteExplainedQuestion/{id}")
    public String deleteExplainedQuestion(@PathVariable Long id, HttpServletRequest request) {
        User users = userService.getUserByUserName(request.getRemoteUser());
        ExplainedQuestion explainedQuestion = explainedQuestionService.getQuestionById(id);
        if (explainedQuestion.getUser().equals(users)) {
            String rootDir = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDir + "/WEB-INF/res/questionImages/" + id + "-a.png");
            path1 = Paths.get(rootDir + "/WEB-INF/res/questionImages/" + id + "-q.png");


            if (Files.exists(path)) {
                try {
                    Files.delete(path);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (Files.exists(path1)) {
                try {
                    Files.delete(path1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            explainedQuestionService.deleteQuestion(explainedQuestion);
            return "redirect:/privileged/explainedQuestionInventory";

        } else {
            users.setEnabled(false);
            userService.editUser(users);
            return "redirect:/";
        }
    }

    @GetMapping(value = "/privileged/question/addExplainedQuestionSet")
    public String ExplainedQuestionDynamicAddGet() {
        return "ExplainedQuestion/addExplainedQuestionSet";

    }

    @PostMapping(value = "/privileged/question/addExplainedQuestionSet")
    public String ExplainedQuestionDynamicAdd(HttpServletRequest request, Model model) {
        // if(request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

        String title = request.getParameter("optone");

        String seasons = request.getParameter("seasons");
        String subjectOrCourseTitle = request.getParameter("subjectOrCourseTitle");

        CourseInfo courseInfo = new CourseInfo();
        courseInfo.setTitle(title);


        Long ExistCourseId = courseInfoService.ExistCourse(courseInfo);
        Long uuid = 0l;

        if (ExistCourseId != uuid) {
            courseInfo = courseInfoService.getCourseInfoById(ExistCourseId);

        } else {
            courseInfoService.addCourseInfo(courseInfo);
        }


        long i = 1;
        while (request.getParameter("qContent" + i) != null) {
            ExplainedQuestion explainedQuestion = new ExplainedQuestion();
            explainedQuestion.setCourseInfo(courseInfo);
            explainedQuestion.setSubjectOrCourseTitle(subjectOrCourseTitle);
            String qcontent = request.getParameter("qContent" + i);
            String qOption1 = request.getParameter("optionOne" + i);
            String qLevel = request.getParameter("level" + i);
            String isPublic = request.getParameter("ispublic" + i);
            String score = request.getParameter("score" + i);


            explainedQuestion.setContent(qcontent);
            explainedQuestion.setAnswer(qOption1);
            explainedQuestion.setLevel(Integer.parseInt(qLevel));
            explainedQuestion.setSeasons(seasons);
            explainedQuestion.setPublic(Boolean.parseBoolean(isPublic.trim()));
            explainedQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));
            explainedQuestion.setScore(Float.parseFloat(score.trim()) > 0 ? Float.parseFloat(score.trim()) : 1.0f);

            explainedQuestionService.addQuestion(explainedQuestion);

            i++;
        }
        return "redirect:/privileged/explainedQuestionInventory";
        ///   }else {
        //    model.addAttribute("tryAgain","لطفا عبارت امنیتی را با دقت وارد نمایید!!!");
        //  return "ExplainedQuestion/addExplainedQuestionSet";
        //}
    }

    @GetMapping("/public/question/viewExplainedQuestion/{questionId}")
    public String viewExQuestion(@PathVariable Long questionId, Model model, HttpServletRequest request) throws IOException {
        ExplainedQuestion question = explainedQuestionService.getQuestionById(questionId);
        model.addAttribute("explainedQuestion", question);
        return "ExplainedQuestion/viewExplainedQuestion";
    }


    @GetMapping("/privileged/explainedQuestionInventory")
    public String explainedQuestionInventory(Model model, HttpServletRequest request) {
        String userName = request.getRemoteUser();
        List<ExplainedQuestion> questionList = explainedQuestionService.getByOwnerUser(userService.getUserByUserName(userName).getId());
        model.addAttribute("explainedQuestions", questionList);
        return "ExplainedQuestion/explainedQuestionInventory";
    }

    @GetMapping("/public/user/home/explainedQuestions/public")
    public String getPublicExplainedQuestions(Model model, HttpServletRequest request) {
        List<ExplainedQuestion> userExplainedList = explainedQuestionService.getPublicExplainedQuestions();
        List<String> favoriteEQuestionIDs = favoriteExplainedQuestionService.getFavoriteEQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteEQuestionIDs", favoriteEQuestionIDs);

        model.addAttribute("explainedQuestions", userExplainedList);

        return "user/explainedQuestions";

    }

    @GetMapping(value = "/privileged/addEQuestionToFavorites")
    public @ResponseBody
    String addFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {

        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteExplainedQuestion explainedQuestion = new FavoriteExplainedQuestion();
            explainedQuestion.setUserExplainedQuestionId(UserExplainedQuestionId.of(QID, userId));
            favoriteExplainedQuestionService.addToMyFavoriteExplainedQuestions(explainedQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }

    @PostMapping(value = "/privileged/removeEQuestionFromFavorites")
    public @ResponseBody
    String deleteFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {
        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteExplainedQuestion explainedQuestion = new FavoriteExplainedQuestion();
            explainedQuestion.setUserExplainedQuestionId(UserExplainedQuestionId.of(QID, userId));
            favoriteExplainedQuestionService.deleteFromMyFavoriteExplainedQuestions(explainedQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }


}
