package ir.rabbit.group.questionbank.controller.user;

import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.question.*;
import ir.rabbit.group.questionbank.model.user.*;
import ir.rabbit.group.questionbank.service.abstraction.exam.*;
import ir.rabbit.group.questionbank.service.abstraction.question.*;
import ir.rabbit.group.questionbank.service.abstraction.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/guestUser")
public class GuestUserController {


    @Autowired
    ExamService examService;
    @Autowired
    GuestUserParticipateInExamService guestUserParticipateInExamService;
    @Autowired
    GuestUserAnswersTestQuestionService guestUserAnswersTestQuestionService;
    @Autowired
    TestQuestionService testQuestionService;
    @Autowired
    TrueFalseQuestionService trueFalseQuestionService;
    @Autowired
    GuestUserAnswersTrueFalseQuestionService guestUserAnswersTrueFalseQuestionService;
    @Autowired
    MatchingQuestionService matchingQuestionService;
    @Autowired
    MatchingNodeService matchingNodeService;
    @Autowired
    GuestUserAnswersMatchingQuestionService guestUserAnswersMatchingQuestionService;
    @Autowired
    ExplainedQuestionService explainedQuestionService;
    @Autowired
    GuestUserAnswersExplainedQuestionService guestUserAnswersExplainedQuestionService;
    @Autowired
    ExamHasTrueFalseQuestionService examHasTrueFalseQuestionService;
    @Autowired
    ExamHasTestQuestionService examHasTestQuestionService;
    @Autowired
    ExamHasExplainedQuestionService examHasExplainedQuestionService;
    @Autowired
    ExamHasMatchingQuestionService examHasMatchingQuestionService;

    //  static int numberOfCalls=0;
    @RequestMapping(value = "/examLink/open/{examId}", method = RequestMethod.GET)
    public String examLink(@PathVariable("examId") Long examId, Model model, HttpServletRequest request) throws IOException {
        Exam exam = examService.getExamById(examId);
        // numberOfCalls++;
        model.addAttribute("started", exam.isStarted());
        model.addAttribute("finished", exam.isFinished());
        model.addAttribute("id", exam.getId());


        // model.addAttribute("numberOfCalls",request.getHeader("X-FORWARDED-FOR")==null?request.getRemoteAddr():request.getHeader("X-FORWARDED-FOR"));


        return "Exam/loginAsGuest";
    }

    @PostMapping(value = "/examLink/open/{examId}")
    public String examLinkPost(@PathVariable("examId") Long examId, Model model, HttpServletRequest request) {
        Exam exam = examService.getExamById(examId);

        if (request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {


            String pass = request.getParameter("password");
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String phone = request.getParameter("phone");
            String message = request.getParameter("message");

            GuestUser guestUser;
            guestUser = new GuestUser();
            if (pass.toLowerCase().equals(exam.getPassword().toLowerCase())) {
                guestUser.setExam(exam);
                if (!email.equals("") && !name.equals("")) {

                    guestUser.setEmail(email.toLowerCase());
                    guestUser.setMessage(message);
                    guestUser.setName(name);
                    guestUser.setPhoneNumber(phone);
                    guestUser.setDone(false);
                    guestUser.setExamSeen(false);

                    guestUserParticipateInExamService.addGUPIE(guestUser);

                } else {
                    model.addAttribute("started", exam.isStarted());
                    model.addAttribute("finished", exam.isFinished());
                    model.addAttribute("id", exam.getId());
                    return "Exam/loginAsGuest";
                }


            } else {
                model.addAttribute("started", exam.isStarted());
                model.addAttribute("finished", exam.isFinished());
                model.addAttribute("id", exam.getId());
                return "Exam/loginAsGuest";
            }

            return "redirect:/guestUser/participateAsGuest/" + examId + "/" + guestUser.getId();
        } else {
            model.addAttribute("tryAgain", "لطفا عبارت امنیتی را با دقت وارد نمایید!!!");
            model.addAttribute("started", exam.isStarted());
            model.addAttribute("finished", exam.isFinished());
            model.addAttribute("id", exam.getId());
            return "Exam/loginAsGuest";

        }
    }

    @GetMapping(value = "/participateAsGuest/{examID}/{guserId}")
    public String participateInExamAsGuest(@PathVariable("guserId") Long guserId, @PathVariable("examID") Long examID, Model model, HttpServletRequest request) {

        Exam exam = examService.getExamById(examID);

        Date d = new Date();
        GuestUser guestUser = guestUserParticipateInExamService.getGuesUserById(guserId);

        if (!guestUser.isExamSeen()) {
            if (exam.getLongTime() == 0 || exam.getEndTime().getTime() > d.getTime()) {
                guestUser.setExamSeen(true);
                guestUserParticipateInExamService.addGUPIE(guestUser);

                model.addAttribute("exam", exam);

                List<TestQuestion> testQuestionList = new ArrayList<>();
                examHasTestQuestionService.getExamHasTestQuestionByExamID(examID).stream().parallel().forEach(examHasTestQuestions ->
                        testQuestionList.add(examHasTestQuestions.getTestQuestion()));

                Collections.shuffle(testQuestionList);

                List<ExplainedQuestion> explainedQuestions = new ArrayList<>();
                examHasExplainedQuestionService.getExamHasExpalinQuestionsByExam(examID).stream().parallel().forEach(examHasExpalinQuestions ->
                        explainedQuestions.add(examHasExpalinQuestions.getExplainedQuestion()));

                Collections.shuffle(explainedQuestions);

                List<TrueFalseQuestion> trueFalseQuestionList = new ArrayList<>();

                examHasTrueFalseQuestionService.getExamHasTrueFalseQuestionByExamID(examID).stream().parallel().forEach(examHasTrueFalseQuestion ->
                        trueFalseQuestionList.add(examHasTrueFalseQuestion.getTrueFalseQuestion()));

                Collections.shuffle(trueFalseQuestionList);

                List<MatchingQuestion> matchingQuestionsList = new ArrayList<>();
                List<List<MatchingNode>> alllMatchingNodes = new ArrayList<List<MatchingNode>>();

                examHasMatchingQuestionService.getExamHasMQuestionByExamID(examID).stream().parallel().forEach(examHasMatchingQuestions -> {
                    matchingQuestionsList.add(examHasMatchingQuestions.getMatchingQuestion());
                    alllMatchingNodes.add(matchingQuestionService.getMatchingNodeList(examHasMatchingQuestions.getMatchingQuestion().getId()));

                });


                Date date = new Date();

                double elapsed = date.getTime() - exam.getStartTime().getTime();
                double remained = exam.getEndTime().getTime() - date.getTime();
                double total = exam.getEndTime().getTime() - exam.getStartTime().getTime();


                model.addAttribute("matchingQuestion", matchingQuestionsList);
                model.addAttribute("listOfMatchingNodeList", alllMatchingNodes);
                model.addAttribute("trueFalseQuestions", trueFalseQuestionList);
                model.addAttribute("testQuestions", testQuestionList);
                model.addAttribute("explainedQuestionsList", explainedQuestions);
                model.addAttribute("elapsed", elapsed);
                model.addAttribute("remained", remained);
                model.addAttribute("userID", guserId);
                model.addAttribute("width", ((elapsed / total) * 100));
                return "Exam/participateInExamAsGuest";

            } else {
                model.addAttribute("exam", exam);
                return "Exam/examEndedMessage";
            }
        } else {
            return "redirect:/guestUser/examResult/" + examID + "/" + guestUser.getId();
        }


    }

    @PostMapping(value = "/participateAsGuest/{examID}/{guserId}")
    public String participateInExamAsGuestPost(@PathVariable("examID") Long examID, @PathVariable("guserId") Long guserId, HttpServletRequest request) {
        Date date = new Date();
        GuestUser guestUser = guestUserParticipateInExamService.getGuesUserById(guserId);
        if (examService.getExamById(examID).getLongTime() != 0 && date.getTime() > (examService.getExamById(examID).getEndTime().getTime() + 1200000)) {
            return "redirect:/user/delay/examEndedMessage/" + examID;
        } else {
            int i = 1;
            GuestUserAnswersTestQuestion gUserAnswersTQuestion;
            double d = 0;
            String userAnswer = "***بدون پاسخ**";
            while (request.getParameter("q-" + i) != null && !request.getParameter("q-" + i).equals("")) {
                gUserAnswersTQuestion = new GuestUserAnswersTestQuestion();
                if (request.getParameter("tQA-" + i) != null && !request.getParameter("tQA-" + i).equals("")) {
                    userAnswer = request.getParameter("tQA-" + i);
                }
                TestQuestion testQuestion = testQuestionService.getQuestionById(Long.valueOf(request.getParameter("q-" + i).trim()));

                gUserAnswersTQuestion.setTestQuestion(testQuestion);
                gUserAnswersTQuestion.setGuestUser(guestUserParticipateInExamService.getGuesUserById(guserId));
                gUserAnswersTQuestion.setUserAnswer(userAnswer);
                guestUserAnswersTestQuestionService.addGuestUserAnswersQuestion(gUserAnswersTQuestion);

                if (gUserAnswersTQuestion.getUserAnswer().equals(gUserAnswersTQuestion.getTestQuestion().getRightOption())) {
                    d += testQuestion.getScore();

                }
                i++;
            }


            GuestUserAnswersExplainedQuestion guestUserAnswersExplainedQuestion;
            while (request.getParameter("eq-" + i) != null && !request.getParameter("eq-" + i).equals("")) {
                guestUserAnswersExplainedQuestion = new GuestUserAnswersExplainedQuestion();
                if (request.getParameter("eqa-" + i) != null && !request.getParameter("eqa-" + i).equals("")) {
                    userAnswer = request.getParameter("eqa-" + i);
                }
                ExplainedQuestion explainedQuestion = explainedQuestionService.getQuestionById(Long.valueOf(request.getParameter("eq-" + i).trim()));
                guestUserAnswersExplainedQuestion.setExplainedQuestion(explainedQuestion);
                guestUserAnswersExplainedQuestion.setUserAnswer(userAnswer.trim().toLowerCase());
                guestUserAnswersExplainedQuestion.setGuestUser(guestUserParticipateInExamService.getGuesUserById(guserId));
                guestUserAnswersExplainedQuestionService.addGuestUserAnswersEQuestion(guestUserAnswersExplainedQuestion);

                if (guestUserAnswersExplainedQuestion.getUserAnswer().equals(guestUserAnswersExplainedQuestion.getExplainedQuestion().getAnswer())) {
                    d += explainedQuestion.getScore();
                }
                i++;
            }

            GuestUserAnswersTrueFalseQuestion guestUserAnswersTrueFalseQuestion;
            while (request.getParameter("tfq-" + i) != null && !request.getParameter("tfq-" + i).equals("")) {
                guestUserAnswersTrueFalseQuestion = new GuestUserAnswersTrueFalseQuestion();
                if (request.getParameter("tfQA-" + i) != null && !request.getParameter("tfQA-" + i).equals("")) {
                    userAnswer = request.getParameter("tfQA-" + i);
                }
                TrueFalseQuestion trueFalseQuestion = trueFalseQuestionService.getQuestionById(Long.valueOf(request.getParameter("tfq-" + i).trim()));
                guestUserAnswersTrueFalseQuestion.setTrueFalseQuestion(trueFalseQuestion);
                guestUserAnswersTrueFalseQuestion.setUserAnswer(userAnswer.trim().toLowerCase());
                guestUserAnswersTrueFalseQuestion.setGuestUser(guestUserParticipateInExamService.getGuesUserById(guserId));
                guestUserAnswersTrueFalseQuestionService.addGuestUserAnswersTrueFalseQuestion(guestUserAnswersTrueFalseQuestion);

                if (Boolean.parseBoolean(guestUserAnswersTrueFalseQuestion.getUserAnswer()) == guestUserAnswersTrueFalseQuestion.getTrueFalseQuestion().getAnswer()) {
                    d += trueFalseQuestion.getScore();
                }
                i++;
            }

            GuestUserAnswersMatchingQuestion guserAnswersMatchingQuestions;
            List<MatchingNode> matchingNodeList;
            while (request.getParameter("mq-" + i) != null && !request.getParameter("mq-" + i).equals("")) {
                guserAnswersMatchingQuestions = new GuestUserAnswersMatchingQuestion();
                Long MQID = Long.valueOf(request.getParameter("mq-" + i).trim());
                MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(MQID);

                guserAnswersMatchingQuestions.setMatchingQuestion(matchingQuestion);
                matchingNodeList = matchingQuestionService.getMatchingNodeList(matchingQuestion.getId());

                String answer = "";
                for (int j = 0; j < matchingNodeList.size(); j++) {
                    if (request.getParameter("MNID-" + j) != null && !request.getParameter("MNID-" + j).equals("") && request.getParameter("node-" + j) != null && !request.getParameter("node-" + j).equals("")) {
                        if (matchingNodeService.getMatchingnodeById(
                                Long.valueOf(request.getParameter("MNID-" + j).trim())).getMatchString()
                                .equals((request.getParameter("node-" + j)))) {
                            d += (matchingQuestion.getScore() / matchingNodeList.size());
                        }
                        answer += (request.getParameter("node-" + j) + "-");
                    }
                }
                guserAnswersMatchingQuestions.setUserAnswer(answer);
                guserAnswersMatchingQuestions.setGuestUser(guestUserParticipateInExamService.getGuesUserById(guserId));
                guserAnswersMatchingQuestions.setMatchingQuestion(matchingQuestion);
                guestUserAnswersMatchingQuestionService.addGuestUserAnswersMQuestion(guserAnswersMatchingQuestions);

                i++;
            }


            guestUser.setScore(d);
            guestUser.setDone(true);
            guestUserParticipateInExamService.addGUPIE(guestUser);
        }
        return "redirect:/guestUser/examResult/" + examID + "/" + guestUser.getId();
    }

    @GetMapping("/examResult/{examID}/{userId}")
    public String examResultForGuestUser(@PathVariable("userId") Long userId, @PathVariable("examID") Long examID, Model model, HttpServletRequest request) {
        GuestUser guestUser = guestUserParticipateInExamService.getGuesUserById(userId);
        List<GuestUserAnswersTestQuestion> guestUserAnswersTestQuestionList = guestUserAnswersTestQuestionService.getAllGuestUsersAnswersTestQuestionsByGPIEId(userId);
        List<GuestUserAnswersExplainedQuestion> guestUserAnswersEQuestionList = guestUserAnswersExplainedQuestionService.getAllGuestUsersAnswersExplainedQuestionsByGPIEId(userId);
        List<GuestUserAnswersTrueFalseQuestion> guestUserAnswersTFQuestionList = guestUserAnswersTrueFalseQuestionService.getAllGuestUsersAnswersTrueFalseQuestionsByGPIEId(userId);
        List<GuestUserAnswersMatchingQuestion> guestUserAnswersMQuestionList = guestUserAnswersMatchingQuestionService.getAllGuestUsersAnswersMatchingQuestionsByGPIEId(userId);

        model.addAttribute("guestUserAnswersTestQuestion", guestUserAnswersTestQuestionList);
        model.addAttribute("guestUserAnswersExplainedQuestion", guestUserAnswersEQuestionList);
        model.addAttribute("guestUserAnswersTrueFalseQuestion", guestUserAnswersTFQuestionList);
        model.addAttribute("guestUserAnswersMatchingQuestion", guestUserAnswersMQuestionList);
        model.addAttribute("userParticipateInExam", guestUser);


        model.addAttribute("examWholeScore", examService.getExamById(examID).getScore());
        return "Exam/examResultForGuestUser";
    }

    @PostMapping(value = "/validateExamPass/{examId}")
    public @ResponseBody
    String passValidate(@PathVariable("examId") Long examId, @RequestBody String pass) {
        Exam exam = examService.getExamById(examId);
        return (exam.getPassword().equals(pass)) + "";

    }


}
