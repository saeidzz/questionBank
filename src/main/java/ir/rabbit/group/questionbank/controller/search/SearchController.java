package ir.rabbit.group.questionbank.controller.search;

import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.exam.ExamFile;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.connections.UserHasConnectionsService;
import ir.rabbit.group.questionbank.service.abstraction.favorit.*;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.search.SearchService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class SearchController {

    @Autowired
    UserService userService;

    @Autowired
    CourseInfoService courseInfoService;

    @Autowired
    SearchService searchService;

    @Autowired
    UserHasConnectionsService userHasConnectionsService;

    @Autowired
    FavoriteExamService favoriteExamService;

    @Autowired
    FavoriteTestQuestionService favoriteTestQuestionService;

    @Autowired
    FavoriteTrueFalseQuestionService favoriteTrueFalseQuestionService;

    @Autowired
    FavoriteMatchingQuestionService favoriteMatchingQuestionService;

    @Autowired
    FavoriteExplainedQuestionService favoriteExplainedQuestionService;


    @GetMapping("/public/search")
    public String search() {
        return "Search/search";
    }

    @PostMapping(value = "/public/search")
    public String searchPage(HttpServletRequest request, Model model) {

        if (request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

            if (request.getParameter("optone").equals("")) {
                model.addAttribute("tryagin", "لطفا یک زمینه تحصیلی انتخاب کنید");
                return "Search/search";
            } else {
                CourseInfo courseInfo = new CourseInfo();
                courseInfo.setTitle(request.getParameter("optone"));
                Long res = courseInfoService.ExistCourse(courseInfo);
                Long uuid = 0l;

                if (res != uuid) {
                    courseInfo.setId(res);
                }

                int entitiy = Integer.parseInt(request.getParameter("entities").trim());
                String phrase = request.getParameter("phrase");

                switch (entitiy) {
                    case 0:
                        List<TestQuestion> testQuestions = searchService.searchTestQuestion(phrase, courseInfo);
                        List<String> favoriteTQuestions = favoriteTestQuestionService.getFavoriteTQIdOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
                        model.addAttribute("favoriteTQuestionsId", favoriteTQuestions);
                        model.addAttribute("testQuestions", testQuestions);
                        return "user/testQuestions";
                    case 1:
                        List<ExplainedQuestion> explainedQuestions = searchService.searchExplainedQuestion(phrase, courseInfo);
                        List<String> favoriteEQuestionIDs = favoriteExplainedQuestionService.getFavoriteEQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
                        model.addAttribute("favoriteEQuestionIDs", favoriteEQuestionIDs);
                        model.addAttribute("explainedQuestions", explainedQuestions);

                        return "user/explainedQuestions";

                    case 2:
                        List<TrueFalseQuestion> trueFalseQuestions = searchService.searchTrueFalseQuestion(phrase, courseInfo);
                        model.addAttribute("trueFalseQuestions", trueFalseQuestions);
                        List<String> favoriteTFQuestionIDs = favoriteTrueFalseQuestionService.getFavoriteTFQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
                        model.addAttribute("favoriteTFQuestionIDs", favoriteTFQuestionIDs);
                        return "user/trueFalseQuestions";

                    case 3:
                        List<MatchingQuestion> matchingQuestions = searchService.searchMatchingQuestion(phrase, courseInfo);
                        List<String> favoriteMQuestionIDs = favoriteMatchingQuestionService.getFavoriteMQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
                        model.addAttribute("favoriteMQuestionIDs", favoriteMQuestionIDs);
                        model.addAttribute("matchingQuestions", matchingQuestions);
                        return "user/matchingQuestions";

                    case 4:
                        List<ExamFile> examFiles = searchService.searchExamFiles(phrase, courseInfo);
                        model.addAttribute("examFiles", examFiles);
                        return "ExamFile/publicExamFiles";

                    case 5:
                        List<Exam> exams = searchService.searchExams(phrase, courseInfo);
                        List<String> favoriteExams = favoriteExamService.getFavoriteExamsIdOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
                        model.addAttribute("favoriteExamsId", favoriteExams);
                        model.addAttribute("examList", exams);
                        return "Exam/publicExamList";

                    case 6:
                        List<User> users = searchService.searchUsers(phrase);
                        model.addAttribute("users", users);
                        return "connection/userList";

                    default:
                        return "Search/search";
                }

            }
        } else {
            model.addAttribute("tryagin", "لطفا عبارت امنیتی را با دقت وارد نمایید");
            return "Search/search";
        }
    }

    @GetMapping(value = "/public/specificSearch")
    public String searchPeople() {
        return "Search/searchPeoples";
    }

    @PostMapping(value = "/public/specificSearch")
    public String searchPeoples(HttpServletRequest request, Model model) {
        if (request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {
            User currentUser = userService.getUserByUserName(request.getRemoteUser());
            String phrase = request.getParameter("phrase");
            List<User> doesNotAcceptedFollowings = userHasConnectionsService.getDoesNotAcceptedFollowings(currentUser.getId());
            List<User> acceptedFollowings = userHasConnectionsService.getAcceptedFollowings(currentUser.getId());
            if (phrase != null && !phrase.equals("")) {
                List<User> users = searchService.searchUsers(phrase);

                model.addAttribute("users", users);
                model.addAttribute("doesNotAcceptedFollowings", doesNotAcceptedFollowings);
                model.addAttribute("acceptedFollowings", acceptedFollowings);
                model.addAttribute("currentUser", currentUser);


                return "connection/userList";
            } else {
                model.addAttribute("tryagin", "عبارت مورد جستجو نباید خالی باشد");
                return "Search/searchPeoples";
            }
        } else {
            model.addAttribute("tryagin", "لطفا عبارت امنیتی را با دقت وارد نمایید");
            return "Search/searchPeoples";

        }

    }

}

