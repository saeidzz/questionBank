package ir.rabbit.group.questionbank.controller.questions;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteTrueFalseQuestion;
import ir.rabbit.group.questionbank.model.compositeId.UserTrueFalseQuestionId;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteTrueFalseQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.question.MatchingNodeService;
import ir.rabbit.group.questionbank.service.abstraction.question.TrueFalseQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
public class TruFalseQuestionController {
    @Autowired
    CourseInfoService courseInfoService;
    @Autowired
    UserService userService;
    @Autowired
    TrueFalseQuestionService trueFalseQuestionService;

    @Autowired
    MatchingNodeService matchingNodeService;

    @Autowired
    FavoriteTrueFalseQuestionService favoriteTrueFalseQuestionService;

    @GetMapping("/privileged/question/addTrueFalseQuestion")
    public String addTrueFalseQuestion(Model model) {
        CourseInfo courseInfo = new CourseInfo();
        TrueFalseQuestion trueFalseQuestion = new TrueFalseQuestion();
        trueFalseQuestion.setContent("محتوی سوال را وارد نمایید.");
        trueFalseQuestion.setScore(1.0f);
        trueFalseQuestion.setCourseInfo(courseInfo);
        model.addAttribute("trueFalseQuestion", trueFalseQuestion);
        return "trueFalseQuestion/addTrueFalseQuestion";
    }

    @PostMapping(value = "/privileged/question/addTrueFalseQuestion")
    public String addTrueFalseQuestionPost(@Valid @ModelAttribute("trueFalseQuestion") TrueFalseQuestion trueFalseQuestion, BindingResult result, HttpServletRequest request, Model model) {
        //  if (request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

        if (result.hasErrors()) {
            return "trueFalseQuestion/addTrueFalseQuestion";
        }

        Long ExistOrNot = courseInfoService.ExistCourse(trueFalseQuestion.getCourseInfo());
        Long uuid = 0l;

        if (ExistOrNot == uuid) {
            courseInfoService.addCourseInfo(trueFalseQuestion.getCourseInfo());
        } else {
            trueFalseQuestion.setCourseInfo(courseInfoService.getCourseInfoById(ExistOrNot));
        }

        trueFalseQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));
        trueFalseQuestionService.addTrueFalseQuestion(trueFalseQuestion);


        return "redirect:/privileged/TrueFalseQuestionInventory";
        //    } else {
        //      model.addAttribute("tryAgain", "لطفا عبارت امنیتی را با دقت وارد نمایید!!!");
        //    return "trueFalseQuestion/addTrueFalseQuestion";
        // }
    }


    @GetMapping("/privileged/TrueFalseQuestionInventory")
    public String trueFalseQuestionInventory(Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        List<TrueFalseQuestion> trueFalseQuestions = trueFalseQuestionService.getQuestionByOwnerUser(user.getId());
        model.addAttribute("trueFalseQuestions", trueFalseQuestions);
        return "trueFalseQuestion/trueFalseQuestionInventory";
    }


    @GetMapping("/privileged/question/editTrueFalseQuestion/{tfQid}")
    public String editTrueFalseQuestion(@PathVariable("tfQid") Long tfQid, Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        TrueFalseQuestion trueFalseQuestion = trueFalseQuestionService.getQuestionById(tfQid);

        if (trueFalseQuestion.getUser().equals(user)) {
            model.addAttribute("trueFalseQuestion", trueFalseQuestion);
            return "trueFalseQuestion/editTrueFalseQuestion";
        } else {
            user.setEnabled(false);
            userService.editUser(user);
            return "redirect:/";
        }
    }

    @GetMapping("/privileged/question/editTrueFalseQuestion")
    public String editTrueFalseQuestionPost(@Valid @ModelAttribute("trueFalseQuestion") TrueFalseQuestion trueFalseQuestion, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "trueFalseQuestion/editTrueFalseQuestion";
        }
        Long ExistOrNot = courseInfoService.ExistCourse(trueFalseQuestion.getCourseInfo());
        Long uuid = 0l;

        if (ExistOrNot.equals(uuid)) {
            courseInfoService.addCourseInfo(trueFalseQuestion.getCourseInfo());
        } else {
            trueFalseQuestion.setCourseInfo(courseInfoService.getCourseInfoById(ExistOrNot));
        }
        trueFalseQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));

        String userName = request.getRemoteUser();
        trueFalseQuestionService.editQuestion(trueFalseQuestion);
        return "redirect:/privileged/TrueFalseQuestionInventory/" + userName;
    }

    @GetMapping("/privileged/question/deleteTrueFalseQuestion/{tfQid}")
    public String deleteTrueFalseQuestion(@PathVariable("tfQid") Long tfQid, HttpServletRequest request) {
        TrueFalseQuestion trueFalseQuestion = trueFalseQuestionService.getQuestionById(tfQid);
        User user = userService.getUserByUserName(request.getRemoteUser());
        if (trueFalseQuestion.getUser().equals(user)) {
            trueFalseQuestionService.deleteQuestion(trueFalseQuestion);
            return "redirect:/privileged/TrueFalseQuestionInventory";
        } else {
            user.setEnabled(false);
            userService.editUser(user);
            return "redirect:/";
        }

    }


    @GetMapping(value = "/privileged/question/addTrueFalseQuestionSet")
    public String trueFalseQuestionDynamicAddGet() {

        return "trueFalseQuestion/addTrueFalseQuestionSet";

    }

    @PostMapping(value = "/privileged/question/addTrueFalseQuestionSet")
    public String trueFalseQuestionDynamicAdd(HttpServletRequest request) {
        //   if (request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

        String title = request.getParameter("optone");
        String seasons = request.getParameter("seasons");
        String subjectOrCourseTitle = request.getParameter("subjectOrCourseTitle");

        CourseInfo courseInfo = new CourseInfo();
        courseInfo.setTitle(title);


        Long ExistCourseId = courseInfoService.ExistCourse(courseInfo);
        Long uuid = 0l;

        if (ExistCourseId != uuid) {
            courseInfo = courseInfoService.getCourseInfoById(ExistCourseId);

        } else {
            courseInfoService.addCourseInfo(courseInfo);
        }


        long i = 1;
        while (request.getParameter("trueFalseQuestion" + i) != null) {
            TrueFalseQuestion trueFalseQuestion = new TrueFalseQuestion();
            trueFalseQuestion.setCourseInfo(courseInfo);
            trueFalseQuestion.setSubjectOrCourseTitle(subjectOrCourseTitle);
            String qcontent = request.getParameter("trueFalseQuestion" + i);
            String qOption1 = request.getParameter("trueFalseAnswer" + i);
            String qLevel = request.getParameter("level" + i);
            String isPublic = request.getParameter("ispublic" + i);
            String score = request.getParameter("score" + i);

            trueFalseQuestion.setContent(qcontent);
            trueFalseQuestion.setAnswer(Boolean.parseBoolean(qOption1.trim().toLowerCase()));
            trueFalseQuestion.setLevel(Integer.parseInt(qLevel));
            trueFalseQuestion.setSeasons(seasons);
            trueFalseQuestion.setPublic(Boolean.parseBoolean(isPublic.trim()));
            trueFalseQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));
            trueFalseQuestion.setScore(Float.parseFloat(score.trim()) > 0 ? Float.parseFloat(score.trim()) : 1.0f);
            trueFalseQuestionService.addTrueFalseQuestion(trueFalseQuestion);

            i++;
        }
        return "redirect:/privileged/TrueFalseQuestionInventory";
        // }else{
        //   model.addAttribute("tryAgain", "لطفا عبارت امنیتی را با دقت وارد نمایید!!!");
        // return "trueFalseQuestion/addTrueFalseQuestionSet";

        // }
    }


    @GetMapping("/public/user/home/TrueFalseQuestion/public")
    public String getUserTrueFalseQuestions(Model model, HttpServletRequest request) {
        List<TrueFalseQuestion> trueFalseQuestions = trueFalseQuestionService.getPublicTFQuestions();
        List<String> favoriteTFQuestionIDs = favoriteTrueFalseQuestionService.getFavoriteTFQuestionIdsOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteTFQuestionIDs", favoriteTFQuestionIDs);
        model.addAttribute("trueFalseQuestions", trueFalseQuestions);

        return "user/trueFalseQuestions";

    }

    @PostMapping(value = "/privileged/addTFQuestionToFavorites")
    public @ResponseBody
    String addFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {

        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteTrueFalseQuestion favoriteTrueFalseQuestion = new FavoriteTrueFalseQuestion();
            favoriteTrueFalseQuestion.setUserTrueFalseQuestionId(UserTrueFalseQuestionId.of(QID, userId));
            favoriteTrueFalseQuestionService.addToMyFavoriteTrueFalseQuestions(favoriteTrueFalseQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }

    @PostMapping(value = "/privileged/removeTFQuestionFromFavorites")
    public @ResponseBody
    String deleteFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {
        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteTrueFalseQuestion favoriteTrueFalseQuestion = new FavoriteTrueFalseQuestion();
            favoriteTrueFalseQuestion.setUserTrueFalseQuestionId(UserTrueFalseQuestionId.of(QID, userId));
            favoriteTrueFalseQuestionService.deleteFromMyFavoriteTrueFalseQuestions(favoriteTrueFalseQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }

}
