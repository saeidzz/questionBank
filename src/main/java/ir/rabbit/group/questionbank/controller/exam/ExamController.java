package ir.rabbit.group.questionbank.controller.exam;

import io.swagger.annotations.Api;
import ir.rabbit.group.questionbank.exception.CanNotCreateExamFileException;
import ir.rabbit.group.questionbank.model.Favorit.FavoriteExam;
import ir.rabbit.group.questionbank.model.compositeId.UserExamId;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.exam.*;
import ir.rabbit.group.questionbank.model.question.*;
import ir.rabbit.group.questionbank.model.user.GuestUser;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.connections.UserHasConnectionsService;
import ir.rabbit.group.questionbank.service.abstraction.exam.*;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteExamService;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.question.ExplainedQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.question.MatchingQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.question.TestQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.question.TrueFalseQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.user.GuestUserParticipateInExamService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "exam")
public class ExamController {

    @Autowired
    ExamService examService;

    @Autowired
    UserService userService;

    @Autowired
    CourseInfoService courseInfoService;

    @Autowired
    TestQuestionService testQuestionService;

    @Autowired
    ExplainedQuestionService explainedQuestionService;

    @Autowired
    ExamHasUserService examHasUserService;

    @Autowired
    ExamHasTestQuestionService examHasTestQuestionService;

    @Autowired
    ExamHasExplainedQuestionService examHasExplainedQuestionsService;

    @Autowired
    GuestUserParticipateInExamService guestUserParticipateInExamService;

    @Autowired
    MatchingQuestionService matchingQuestionService;

    @Autowired
    ExamHasMatchingQuestionService examHasMatchingQuestionService;

    @Autowired
    ExamHasTrueFalseQuestionService examHasTrueFalseQuestionService;

    @Autowired
    TrueFalseQuestionService trueFalseQuestionService;

    @Autowired
    UserHasConnectionsService userHasConnectionsService;

    @Autowired
    FavoriteExamService favoriteExamService;


    @GetMapping("/privileged/createExam")
    public String createExam(Model model) {
        Exam exam = new Exam();
        exam.setTitle("عنوان آزمون را اینجا وارد کنید.");
        model.addAttribute("exam", exam);
        return "Exam/createExam";
    }

    @PostMapping(value = "/privileged/createExam")
    public String createExamPost(@Valid @ModelAttribute("exam") Exam exam, BindingResult result, HttpServletRequest request, Model model) {
        if (request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

            if (result.hasErrors()) {
                return "Exam/createExam";
            }
            Long ExistCourseId = courseInfoService.ExistCourse(exam.getCourseInfo());

            Long uuid = 0L;
            if (ExistCourseId != uuid) {
                exam.setCourseInfo(courseInfoService.getCourseInfoById(ExistCourseId));
            } else {
                courseInfoService.addCourseInfo(exam.getCourseInfo());
            }
            exam.setUser(userService.getUserByUserName(request.getRemoteUser()));
            exam.setStarted(false);
            exam.setFinished(false);
            exam.setExported(false);
            examService.addExam(exam);
            exam.setLink("/guestUser/examLink/open/" + exam.getId());
            examService.addExam(exam);
            model.addAttribute("examID", exam.getId());

            return "Exam/addQuestionAndUserToExam";
        } else {
            model.addAttribute("tryagin", "لطفا عبارت امنیتی را با دقت وارد نمایید");
            return "Exam/createExam";
        }
    }

    @GetMapping(value = "/privileged/addTestQToExam/{examID}/{userName}")
    public String addTestQuestionToExam(@PathVariable("examID") Long examID, @PathVariable("userName") String userName, Model model) {
        CourseInfo courseInfo = examService.getExamById(examID).getCourseInfo();
        List<TestQuestion> questionList = testQuestionService.getTestQuestionForExam(userService.getUserByUserName(userName).getId(), courseInfo.getId());
        List<TestQuestion> addedTQuestions = examHasTestQuestionService.getExamTestQuestions(examID);

        model.addAttribute("addedBefore", addedTQuestions);
        model.addAttribute("testQuestionList", questionList);
        model.addAttribute("examID", examID);
        return "Exam/addTestQuestionToExam";
    }

    @PostMapping(value = "/privileged/addTestQToExam/{examID}")
    public String addTestQuestionToExamPost(@PathVariable("examID") Long examID, HttpServletRequest request) {
        Exam exam = examService.getExamById(examID);
        Long tesssst;
        ///list for holding user selected questions
        List<TestQuestion> examTestQuestions = new ArrayList<TestQuestion>();


        ////get all selected questions and add to exam
        int i = 0;
        float examScore = exam.getScore();
        while (request.getParameter("testq" + i) != null) {
            tesssst = Long.valueOf(request.getParameter("testq" + i).trim());
            TestQuestion testQuestion = testQuestionService.getQuestionById(tesssst);
            examTestQuestions.add(testQuestion);
            i++;
        }
        // List<ExamHasTestQuestion> examHasTestQuestionsList = new ArrayList<>();

        ///temp variables for adding new exam to question exam list
        List<ExamHasTestQuestion> temp_question_exam_list;
        ExamHasTestQuestion examHasTestQuestion;
        List<ExamHasTestQuestion> exam_examHasTestQuestion = new ArrayList<>();

        for (TestQuestion question : examTestQuestions) {
            examHasTestQuestion = new ExamHasTestQuestion();
            examHasTestQuestion.setExam(exam);
            examHasTestQuestion.setTestQuestion(question);
            exam_examHasTestQuestion.add(examHasTestQuestion);
            examScore += question.getScore();
            examHasTestQuestionService.addExamHasTestQuestion(examHasTestQuestion);


            //update question

        }
        //update exam

        exam.setScore(examScore);
        examService.addExam(exam);


        return "redirect:/privileged/addQuestionAndUserToExam/" + examID;
    }

    @GetMapping("/privileged/addMQToExam/{examID}/{userName}")
    public String addMQuestionToExam(@PathVariable("examID") Long examID, @PathVariable("userName") String userName, Model model) {
        CourseInfo courseInfo = examService.getExamById(examID).getCourseInfo();

        List<MatchingQuestion> matchingQuestionList = matchingQuestionService.getMQuestionForExam(userService.getUserByUserName(userName).getId(), courseInfo.getId());
        List<MatchingQuestion> addedQuestions = examHasMatchingQuestionService.getExamMQuestions(examID);

        model.addAttribute("matchingQuestionList", matchingQuestionList);
        model.addAttribute("examID", examID);
        model.addAttribute("addedBefore", addedQuestions);

        return "Exam/addMatchingQuestionToExam";
    }

    @PostMapping(value = "/privileged/addMQToExam/{examID}")
    public String addMQToExamPost(@PathVariable("examID") Long examID, HttpServletRequest request) {
        Exam exam = examService.getExamById(examID);
        Long tesssst;
        List<MatchingQuestion> matchingQuestions = new ArrayList<>();

        int i = 0;
        float examScore = exam.getScore();
        while (request.getParameter("matchingQ" + i) != null) {
            tesssst = Long.valueOf(request.getParameter("matchingQ" + i).trim());
            MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(tesssst);
            matchingQuestions.add(matchingQuestion);
            i++;
        }
        ExamHasMatchingQuestion examHasMatchingQuestion;


        for (MatchingQuestion question : matchingQuestions) {
            examHasMatchingQuestion = new ExamHasMatchingQuestion();
            examHasMatchingQuestion.setMatchingQuestion(question);
            examHasMatchingQuestion.setExam(exam);
            examHasMatchingQuestionService.addExamHasMatchingQuestion(examHasMatchingQuestion);
            examScore += question.getScore();


            //     matchingQuestionService.addMatchingQuestion(question);
        }

        exam.setScore(examScore);
        examService.addExam(exam);
        return "redirect:/privileged/addQuestionAndUserToExam/" + examID;

    }

    @GetMapping("/privileged/addTFQToExam/{examID}/{userName}")
    public String addTFQuestionToExam(@PathVariable("examID") Long examID, @PathVariable("userName") String userName, Model model) {
        CourseInfo courseInfo = examService.getExamById(examID).getCourseInfo();
        List<TrueFalseQuestion> trueFalseQuestionList = trueFalseQuestionService.getTFQuestionForExam(userService.getUserByUserName(userName).getId(), courseInfo.getId());
        List<TrueFalseQuestion> addedQuestions = examHasTrueFalseQuestionService.getExamTQuestions(examID);

        model.addAttribute("trueFalseQuestion", trueFalseQuestionList);
        model.addAttribute("addedBefore", addedQuestions);
        model.addAttribute("examID", examID);
        return "Exam/addTrueFalseQuestionToExam";
    }

    @PostMapping(value = "/privileged/addTFQToExam/{examID}")
    public String addTFQToExamPost(@PathVariable("examID") Long examID, HttpServletRequest request) {
        Exam exam = examService.getExamById(examID);
        Long tesssst;
        ///list for holding user selected questions
        List<TrueFalseQuestion> trueFalseQuestionsList = new ArrayList<>();


        ////get all selected questions and add to exam
        int i = 0;
        float examScore = exam.getScore();
        while (request.getParameter("trueFalseQ" + i) != null) {
            tesssst = Long.valueOf(request.getParameter("trueFalseQ" + i).trim());
            TrueFalseQuestion trueFalseQuestion = trueFalseQuestionService.getQuestionById(tesssst);
            trueFalseQuestionsList.add(trueFalseQuestion);
            i++;
        }

        ExamHasTrueFalseQuestion examHasTrueFalseQuestion;


        for (TrueFalseQuestion question : trueFalseQuestionsList) {
            examHasTrueFalseQuestion = new ExamHasTrueFalseQuestion();
            examHasTrueFalseQuestion.setTrueFalseQuestion(question);
            examHasTrueFalseQuestion.setExam(exam);
            examScore += question.getScore();
            examHasTrueFalseQuestionService.addExamHasTrueFalseQuestion(examHasTrueFalseQuestion);

            // trueFalseQuestionService.addTrueFalseQuestion(question);
        }

        exam.setScore(examScore);
        examService.addExam(exam);

        return "redirect:/privileged/addQuestionAndUserToExam/" + examID;

    }

    @GetMapping("/privileged/addEQToExam/{examID}/{userName}")
    public String addExplainedQuestionToExam(@PathVariable("examID") Long examID, @PathVariable("userName") String userName, Model model) {
        CourseInfo courseInfo = examService.getExamById(examID).getCourseInfo();

        List<ExplainedQuestion> explainedQuestionList = explainedQuestionService.getEQuestionForExam(userService.getUserByUserName(userName).getId(), courseInfo.getId());
        List<ExplainedQuestion> addedQuestions = examHasExplainedQuestionsService.getExamEQuestions(examID);

        model.addAttribute("explainedQuestionList", explainedQuestionList);
        model.addAttribute("addedBefore", addedQuestions);
        model.addAttribute("examID", examID);

        return "Exam/addExplainedQuestionToExam";
    }

    @PostMapping(value = "/privileged/addEQToExam/{examID}")
    public String addExplainedQToExamPost(@PathVariable("examID") Long examID, HttpServletRequest request) {
        Exam exam = examService.getExamById(examID);
        Long tesssst;
        ///list for holding user selected questions
        List<ExplainedQuestion> examExplainedQuestions = new ArrayList<>();


        ////get all selected questions and add to exam
        int i = 0;
        float examScore = exam.getScore();
        while (request.getParameter("explainedQ" + i) != null) {
            tesssst = Long.valueOf(request.getParameter("explainedQ" + i).trim());
            ExplainedQuestion explainedQuestion = explainedQuestionService.getQuestionById(tesssst);
            examExplainedQuestions.add(explainedQuestion);
            i++;
        }
        ExamHasExplainedQuestion examHasExpalinQuestions;


        for (ExplainedQuestion question : examExplainedQuestions) {
            examHasExpalinQuestions = new ExamHasExplainedQuestion();
            examHasExpalinQuestions.setExam(exam);
            examHasExpalinQuestions.setExplainedQuestion(question);
            examScore += question.getScore();
            examHasExplainedQuestionsService.addExamHasExpalinQuestions(examHasExpalinQuestions);
            //   explainedQuestionService.addQuestion(question);
        }

        exam.setScore(examScore);
        examService.addExam(exam);

        return "redirect:/privileged/addQuestionAndUserToExam/" + examID;

    }

    @GetMapping("/privileged/addQuestionAndUserToExam/{examID}")
    public String addUserAndQToExam(@PathVariable("examID") String examID, Model model) {
        model.addAttribute("examID", examID);
        return "Exam/addQuestionAndUserToExam";
    }

    @GetMapping(value = "/privileged/addUserToExam/{examID}")
    public String addUsersToExam(@PathVariable("examID") Long examID, Model model, HttpServletRequest request) {
        List<User> usersList = userHasConnectionsService.
                getAcceptedFollowers(userService.getUserByUserName(request.getRemoteUser()).getId());
        List<User> addedBefore = examHasUserService.getExamAddedUsers(examID);
        model.addAttribute("users", usersList);
        model.addAttribute("examID", examID);
        model.addAttribute("addedBefore", addedBefore);
        return "Exam/addStudentToExam";
    }

    @PostMapping(value = "/privileged/addUserToExam/{examID}")
    public String addUsersToExamPost(@PathVariable("examID") Long examID, HttpServletRequest request) {

        Exam exam = examService.getExamById(examID);
        Long tesssst;
        List<User> examUsers = new ArrayList<>();
        long i = 0;
        while (request.getParameter("user" + i) != null) {
            tesssst = Long.valueOf(request.getParameter("user" + i).trim());
            examUsers.add(userService.getUserById(tesssst));
            i++;
        }
        for (User user : examUsers) {
            ExamHasUser examHasUser = new ExamHasUser();
            examHasUser.setExam(exam);
            examHasUser.setUser(user);
            examHasUserService.addExamHasUser(examHasUser);

        }
        return "redirect:/privileged/addQuestionAndUserToExam/" + examID;
    }

    @GetMapping("/privileged/examInventory")
    public String examInventory(Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        List<Exam> examList = examService.getExamByCreatorUser(user.getId());
        model.addAttribute("examList", examList);
        return "Exam/examInventory";
    }

    @GetMapping("/public/exam/examList/public")
    public String publicExams(Model model, HttpServletRequest request) {
        List<Exam> examList = examService.getPublicExams();
        List<String> favoriteExams = favoriteExamService.getFavoriteExamsIdOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteExamsId", favoriteExams);
        model.addAttribute("examList", examList);
        return "Exam/publicExamList";
    }


    @PostMapping(value = "/privileged/addExamToFavorites")
    public @ResponseBody
    String addFavoriteExam(@RequestBody Long examId, HttpServletRequest request) {

        Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
        FavoriteExam favoriteExam = new FavoriteExam();
        favoriteExam.setUserExamId(UserExamId.of(examId, userId));
        favoriteExamService.addToMyFavoriteExams(favoriteExam);

        return "true";
    }

    @PostMapping(value = "/privileged/removeExamFromFavorites")
    public @ResponseBody
    String deleteFavoriteExam(@RequestBody Long examId, HttpServletRequest request) {

        Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
        FavoriteExam favoriteExam = favoriteExamService.getMyFavoriteExamById(userId, examId);
        favoriteExamService.deleteFromMyFavoriteExams(favoriteExam);

        return "true";
    }


    @PostMapping(value = "/privileged/startExam")
    public @ResponseBody
    String startExam(@RequestBody Long examID) {
        Exam exam = examService.getExamById(examID);
        if (!exam.isStarted()) {
            Date date = new Date();
            exam.setStartTime(date);
            exam.setStarted(true);
            Date date2 = new Date();
            date2.setTime(date.getTime() + exam.getLongTime() * 60000);
            exam.setEndTime(date2);
            examService.addExam(exam);
            return "true";
        } else {
            return "false";
        }
    }

    @PostMapping(value = "/privileged/examFinished")
    public @ResponseBody
    String stopExam(@RequestBody Long examID) {
        Exam exam = examService.getExamById(examID);
        if (!exam.isFinished()) {
            exam.setFinished(true);
            examService.addExam(exam);
            return "true";
        } else {
            return "false";
        }
    }


    @GetMapping("/privileged/examState/{examID}")
    public String examState(@PathVariable("examID") Long examID, Model model) {
        List<ExamHasUser> examHasUserList = examHasUserService.getExamHasUsersByExamID(examID);
        List<GuestUser> guestUserParticipateInExamsList = guestUserParticipateInExamService.getGuestUserByExamId(examID);
        model.addAttribute("examHasUsersList", examHasUserList);
        model.addAttribute("guestUserParticipateInExam", guestUserParticipateInExamsList);
        model.addAttribute("examWholeScore", examService.getExamById(examID).getScore());

        return "Exam/examState";
    }

    @GetMapping("/privileged/examExport/{examID}")
    public String examExport(@PathVariable("examID") Long examID, Model model, HttpServletRequest request) {
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");

        Exam exam = examService.getExamById(examID);

        if (!exam.isExported()) {
            ///////////write to document//////////
            XWPFDocument doc = new XWPFDocument();
            XWPFParagraph paragraph = doc.createParagraph();
            XWPFRun run;

            CTP ctp = paragraph.getCTP();
            CTPPr ctppr;
            ctppr = ctp.addNewPPr();
            ctppr.addNewBidi().setVal(STOnOff.ON);


            int cout = 1;

            for (ExamHasExplainedQuestion examHasExpalinQuestions : examHasExplainedQuestionsService.getExamHasExpalinQuestionsByExam(exam.getId())) {
                run = paragraph.createRun();
                run.setText("-" + cout + examHasExpalinQuestions.getExplainedQuestion().getContent());
                run.addBreak();

                File image = new File(rootDirectory + "/WEB-INF/res/questionImages/" + examHasExpalinQuestions.getExplainedQuestion().getId() + "-q.png");
                // Working addPicture Code below...
                if (image.exists()) {
                    String imgFile = rootDirectory + "/WEB-INF/res/questionImages/" + examHasExpalinQuestions.getExplainedQuestion().getId() + "-q.png";
                    XWPFRun r = paragraph.createRun();

                    addPicture(imgFile, r);
                }
                cout++;
            }
            try {
                doc.write(new FileOutputStream(request.getSession().getServletContext().getRealPath("/") + "/WEB-INF/res/exams/" + "exam-" + examID + ".docx"));
            } catch (IOException e) {
                throw new CanNotCreateExamFileException(e.getMessage());
            }


            for (ExamHasTestQuestion examHasTestQuestion : examHasTestQuestionService.getExamHasTestQuestionByExamID(examID)) {
                run = paragraph.createRun();
                run.setText("-" + cout + examHasTestQuestion.getTestQuestion().getContent());
                run.addBreak();
                run.addBreak();
                run.setText(examHasTestQuestion.getTestQuestion().getOptionA());
                run.addBreak();
                run.setText(examHasTestQuestion.getTestQuestion().getOptionB());
                run.addBreak();
                run.setText(examHasTestQuestion.getTestQuestion().getOptionC());
                run.addBreak();
                run.setText(examHasTestQuestion.getTestQuestion().getOptionD());
                run.addBreak();


                File image = new File(rootDirectory + "/WEB-INF/res/questionImages/" + examHasTestQuestion.getTestQuestion().getId() + "test-q.png");

                if (image.exists()) {
                    String imgFile = rootDirectory + "/WEB-INF/res/questionImages/" + examHasTestQuestion.getTestQuestion().getId() + "test-q.png";
                    XWPFRun r = paragraph.createRun();

                    addPicture(imgFile, r);
                }
                cout++;
            }
            try {
                doc.write(new FileOutputStream(request.getSession().getServletContext().getRealPath("/") + "/WEB-INF/res/exams/" + "exam-" + examID + ".docx"));
            } catch (IOException e) {
                throw new CanNotCreateExamFileException(e.getMessage());
            }

            for (ExamHasTrueFalseQuestion examHasTrueFalseQuestion : examHasTrueFalseQuestionService.getExamHasTrueFalseQuestionByExamID(examID)) {
                run = paragraph.createRun();
                run.setText("-" + cout + examHasTrueFalseQuestion.getTrueFalseQuestion().getContent());
                run.addBreak();
                run.addBreak();
                run.setText("()صحیح");
                run.addBreak();
                run.setText("()غلط");
                run.addBreak();


                cout++;
            }
            try {
                doc.write(new FileOutputStream(request.getSession().getServletContext().getRealPath("/") + "/WEB-INF/res/exams/" + "exam-" + examID + ".docx"));
            } catch (IOException e) {
                throw new CanNotCreateExamFileException(e.getMessage());
            }


            for (ExamHasMatchingQuestion examHasMatchingQuestion : examHasMatchingQuestionService.getExamHasMQuestionByExamID(examID)) {
                run = paragraph.createRun();
                run.setText("-" + cout + examHasMatchingQuestion.getMatchingQuestion().getContent());
                run.addBreak();
                List<MatchingNode> ShuffledMatchingNodeList = new ArrayList<>(matchingQuestionService.getMatchingNodeList(examHasMatchingQuestion.getMatchingQuestion().getId()));
                Collections.shuffle(ShuffledMatchingNodeList);
                int cc = 0;
                for (MatchingNode matchingNode : matchingQuestionService.getMatchingNodeList(examHasMatchingQuestion.getMatchingQuestion().getId())) {
                    run.setText("(-)" + matchingNode.getMatchString());
                    run.addTab();
                    run.addTab();
                    run.setText("-" + cc + ShuffledMatchingNodeList.get(cc).getClue());
                    cc++;
                    run.addBreak();
                }
                cout++;
            }
            try {
                doc.write(new FileOutputStream(request.getSession().getServletContext().getRealPath("/") + "/WEB-INF/res/exams/" + "exam-" + examID + ".docx"));

                doc.close();
            } catch (IOException e) {
                throw new CanNotCreateExamFileException(e.getMessage());
            }
            makeKeyFiles(exam, rootDirectory);

            exam.setExported(true);
            examService.addExam(exam);

        }
        model.addAttribute("exam", exam);
        return "Exam/examExport";
    }

    private void addPicture(String imgFile, XWPFRun r) {
        int format = XWPFDocument.PICTURE_TYPE_PNG;
        //  r.setText(imgFile);
        r.addBreak();
        try {
            r.addPicture(new FileInputStream(imgFile), format, "", Units.toEMU(200), Units.toEMU(200)); // 200x200 pixels
        } catch (IOException | InvalidFormatException e) {
            throw new CanNotCreateExamFileException(e.getMessage());
        }
        r.addBreak();
    }

    private void makeKeyFiles(Exam exam, String rootDirectory) {
        if (!exam.isExported()) {
            ///////////write to document//////////
            XWPFDocument xwpfDocument = new XWPFDocument();
            XWPFParagraph paragraph = xwpfDocument.createParagraph();
            XWPFRun run = paragraph.createRun();

            CTP ctp = paragraph.getCTP();
            CTPPr ctppr;
            ctppr = ctp.addNewPPr();
            ctppr.addNewBidi().setVal(STOnOff.ON);

            ///////////////////////making key file
            run = paragraph.createRun();
            int cout = 1;

            for (ExamHasExplainedQuestion examHasExpalinQuestions : examHasExplainedQuestionsService.getExamHasExpalinQuestionsByExam(exam.getId())) {
                run.setText("-" + cout);
                run.addBreak();
                run.addBreak();
                run.setText(examHasExpalinQuestions.getExplainedQuestion().getAnswer());
                run.addBreak();


                File image1 = new File(rootDirectory + "/WEB-INF/res/questionImages/" + examHasExpalinQuestions.getExplainedQuestion().getId() + "-a.png");
                if (image1.exists()) {
                    String imgFile = rootDirectory + "/WEB-INF/res/questionImages/" + examHasExpalinQuestions.getExplainedQuestion().getId() + "-a.png";
                    addPicture(imgFile, run);
                    run.addBreak();
                }
                cout++;
            }

            for (ExamHasTestQuestion examHasTestQuestion : examHasTestQuestionService.getExamHasTestQuestionByExamID(exam.getId())) {
                run.setText("-" + cout);
                run.addBreak();
                run.addBreak();
                run.setText(examHasTestQuestion.getTestQuestion().getRightOption());
                run.addBreak();
                File image1 = new File(rootDirectory + "/WEB-INF/res/questionImages/" + examHasTestQuestion.getTestQuestion().getId() + "test-a.png");
                if (image1.exists()) {
                    String imgFile = rootDirectory + "/WEB-INF/res/questionImages/" + examHasTestQuestion.getTestQuestion().getId() + "test-a.png";
                    addPicture(imgFile, run);
                }
                cout++;
            }

            for (ExamHasTrueFalseQuestion examHasTrueFalseQuestion : examHasTrueFalseQuestionService.getExamHasTrueFalseQuestionByExamID(exam.getId())) {
                run = paragraph.createRun();
                run.setText("-" + cout + examHasTrueFalseQuestion.getTrueFalseQuestion().getContent());
                run.addBreak();
                if (examHasTrueFalseQuestion.getTrueFalseQuestion().getAnswer()) {
                    run.setText("()صحیح");
                } else {
                    run.setText("()غلط");
                }

                run.addBreak();
                run.addBreak();

                cout++;
            }
            try {
                xwpfDocument.write(new FileOutputStream(rootDirectory + "/WEB-INF/res/exams/" + "examkey-" + exam.getId() + ".docx"));
            } catch (IOException e) {
                throw new CanNotCreateExamFileException(e.getMessage());
            }


            for (ExamHasMatchingQuestion examHasMatchingQuestion : examHasMatchingQuestionService.getExamHasMQuestionByExamID(exam.getId())) {
                run = paragraph.createRun();
                run.setText("-" + cout + examHasMatchingQuestion.getMatchingQuestion().getContent());
                run.addBreak();
                for (MatchingNode matchingNode : matchingQuestionService.getMatchingNodeList(examHasMatchingQuestion.getMatchingQuestion().getId())) {
                    run.setText("(-)" + matchingNode.getMatchString());
                    run.addTab();
                    run.addTab();
                    run.setText("(-)" + matchingNode.getClue());

                    run.addBreak();
                }
                cout++;
            }


            try {
                xwpfDocument.write(new FileOutputStream(rootDirectory + "/WEB-INF/res/exams/" + "examkey-" + exam.getId() + ".docx"));
                xwpfDocument.close();
            } catch (IOException e) {
                throw new CanNotCreateExamFileException(e.getMessage());

            }


        }

    }

    @GetMapping("/privileged/editExam/{examId}")
    public String editExam(Model model, @PathVariable("examId") Long examId) {
        Exam exam = examService.getExamById(examId);
        model.addAttribute("exam", exam);
        return "Exam/EditExam";
    }

    @PostMapping(value = "/privileged/editExam")
    public String editExam(@Valid @ModelAttribute("exam") Exam exam, BindingResult result, HttpServletRequest request, Model model) {
        if (result.hasErrors()) {
            return "Exam/EditExam";
        }
        Long ExistCourseId = courseInfoService.ExistCourse(exam.getCourseInfo());
        Long uuid = 0l;

        if (!ExistCourseId.equals(uuid)) {
            exam.setCourseInfo(courseInfoService.getCourseInfoById(ExistCourseId));
        } else {
            courseInfoService.addCourseInfo(exam.getCourseInfo());
        }
        exam.setUser(userService.getUserByUserName(request.getRemoteUser()));
        examService.addExam(exam);

        return "redirect:/privileged/examInventory";
    }

    @GetMapping("/public/thisUserPublicExams/{userId}")
    public String spcificUserPublicExams(@PathVariable("userId") Long userID, Model model) {
        List<Exam> examList = examService.getSpecificUserPublicExam(userID);
        model.addAttribute("examList", examList);
        return "Exam/publicExamList";
    }
}
