package ir.rabbit.group.questionbank.controller.mail;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by saeid on 8/17/17.
 */
//@CController
//@RequestMapping("/sendEmail.do")
public class sendMailController {

    //@Autowired
    //  private JavaMailSender mailSender;

  //  @RequestMapping(method = RequestMethod.POST)
    public String doSendEmail(HttpServletRequest request) {
        // takes input from e-mail form
        String recipientAddress = request.getParameter("recipient");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");

        // prints debug info
        System.out.println("To: " + recipientAddress);
        System.out.println("Subject: " + subject);
        System.out.println("HelloMessage: " + message);

        // creates a simple e-mail object
//        SimpleMailMessage email = new SimpleMailMessage();
//        email.setTo(recipientAddress);
//        email.setSubject(subject);
//        email.setText(message);

        // sends the e-mail
        // mailSender.send(email);

        // forwards to the view named "Result"
        return "didn'tUsed/Result";
    }
}

