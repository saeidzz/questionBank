package ir.rabbit.group.questionbank.controller.loginAndRegister;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by saeid on 9/3/17.
 */

@RestController
public class loginController {
    @GetMapping("/login")
    public ModelAndView login(@RequestParam(value = "error", required = false) String error
            , @RequestParam(value = "logout", required = false)
                                      String logout) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "نام کاربری و پسورد نامعتبر!");
        }
        if (logout != null) {
            model.addObject("msg", "شما با موفقیت خارج شدید !");
        }
        model.setViewName("login");

        return model;

    }
}

