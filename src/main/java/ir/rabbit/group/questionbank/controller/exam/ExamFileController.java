package ir.rabbit.group.questionbank.controller.exam;

import io.swagger.annotations.Api;
import ir.rabbit.group.questionbank.exception.CanNotCreateExamFileException;
import ir.rabbit.group.questionbank.exception.FileSizLimitExceededException;
import ir.rabbit.group.questionbank.model.exam.ExamFile;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamFileService;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@Api(tags = "examFile")
public class ExamFileController {

    @Autowired
    ExamFileService examFileService;
    @Autowired
    CourseInfoService courseInfoService;
    @Autowired
    UserService userService;
    private Path path;
    private Path path1;

    @GetMapping("/privileged/exam/addExamFile")
    public String addExamFile(Model model) {
        ExamFile examFile = new ExamFile();
        examFile.setDate("1396-07-06");
        model.addAttribute(examFile);

        return "ExamFile/examFileAdd";
    }

    @PostMapping(value = "/privileged/exam/addExamFile")
    public String addExamFilePost(@Valid @ModelAttribute("examFile") ExamFile examFile, BindingResult result, HttpServletRequest request) {

        if (result.hasErrors()) {
            return "ExamFile/examFileAdd";
        }

        Long ExistCourseId = courseInfoService.ExistCourse(examFile.getCourseInfo());
        Long uuid = 0l;

        if (ExistCourseId != uuid) {
            examFile.setCourseInfo(courseInfoService.getCourseInfoById(ExistCourseId));
        } else {
            courseInfoService.addCourseInfo(examFile.getCourseInfo());
        }
        User user = userService.getUserByUserName(request.getRemoteUser());
        examFile.setUser(user);
        MultipartFile questionImage = examFile.getQuestionsFile();
        MultipartFile answer = examFile.getAnswersFile();
        long userUploaded = user.getUploadedSize();

        try {
            userUploaded += questionImage.getBytes().length;
        } catch (IOException e) {
            throw new CanNotCreateExamFileException(e.getMessage());

        }
        try {
            userUploaded += answer.getBytes().length;
        } catch (IOException e) {
            throw new CanNotCreateExamFileException(e.getMessage());
        }


        if (10485760 <= userUploaded) {
            throw new FileSizLimitExceededException("fileSize greaterThan or equals : 10485760");

        } else {
            user.setUploadedSize(userUploaded);
            userService.editUser(user);

            String qFileExtension = questionImage.getOriginalFilename().split("\\.")[questionImage.getOriginalFilename().split("/.").length];
            String ansFileExtension = answer.getOriginalFilename().split("\\.")[answer.getOriginalFilename().split("/.").length];


            examFile.setQuestionFileExt(qFileExtension);
            examFile.setAnswerFileExt(ansFileExtension);
            examFileService.addExamFile(examFile);

            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "/WEB-INF/res/examFiles/" + examFile.getId() + "questionFile." + qFileExtension);
            path1 = Paths.get(rootDirectory + "/WEB-INF/res/examFiles/" + examFile.getId() + "answerFile." + ansFileExtension);

            if (!questionImage.isEmpty()) {
                try {
                    questionImage.transferTo(new File(path.toString()));
                } catch (Exception e) {
                    throw new CanNotCreateExamFileException(e.getMessage());

                }
            }
            if (!answer.isEmpty()) {
                try {
                    answer.transferTo(new File(path1.toString()));
                } catch (Exception e) {
                    throw new CanNotCreateExamFileException(e.getMessage());

                }
            }

            return "redirect:/privileged/exam/examFileInventory";
        }
    }

    @GetMapping("/privileged/exam/deleteExamFile/{id}")
    public String deleteExamFile(@PathVariable Long id, HttpServletRequest request) throws IOException {
        String rootDir = request.getSession().getServletContext().getRealPath("/");
        User user = userService.getUserByUserName(request.getRemoteUser());

        commonFileExtDeleter(id, rootDir, user);
        examFileService.deleteExamFile(examFileService.getExamFileById(id));

        return "redirect:/privileged/exam/examFileInventory";

    }

    @GetMapping("/public/exam/viewExamFile/{id}")
    public String viewExamFile(@PathVariable Long id
            , HttpServletRequest request, Model model) throws IOException {

        ExamFile examFile = examFileService.getExamFileById(id);

        model.addAttribute("examFile", examFile);
        return "ExamFile/viewExamFile";

    }

    private void commonFileExtDeleter(Long id, String rootDir, User user) throws IOException {
        boolean Afounded = false;
        boolean qfounded = false;

        long uploadedSize = user.getUploadedSize();

        if (Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.png"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.png"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.png"));

                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.jpg"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.jpg"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.jpg"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.jpeg"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.jpeg"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.jpeg"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.docx"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.docx"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.docx"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.doc"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.doc"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.doc"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.pdf"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.pdf"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.pdf"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.xlsx"))) {
            try {
                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.xlsx"));
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.xlsx"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.txt"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.txt"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.txt"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.ppt"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.ppt"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.ppt"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!Afounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.pptx"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.pptx"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "answerFile.pptx"));
                Afounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.png"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.png"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.png"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.jpg"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.jpg"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.jpg"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.jpeg"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.jpeg"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.jpeg"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.docx"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.docx"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.docx"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.doc"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.doc"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.doc"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.pdf"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.pdf"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.pdf"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.xlsx"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.xlsx"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.xlsx"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.txt"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.txt"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.txt"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.ppt"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.ppt"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.ppt"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!qfounded && Files.exists(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.pptx"))) {
            try {
                uploadedSize -= Files.size(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.pptx"));

                Files.delete(Paths.get(rootDir + "/WEB-INF/res/examFiles/" + id + "questionFile.pptx"));
                qfounded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        user.setUploadedSize(uploadedSize);
        userService.editUser(user);
    }


    @GetMapping("/privileged/exam/examFileInventory")
    public String examFiles(Model model, HttpServletRequest request) {
        User users = userService.getUserByUserName(request.getRemoteUser());
        List<ExamFile> examFiles = examFileService.getExamFilesByOwnerUser(users.getId());
        model.addAttribute("examFiles", examFiles);
        return "ExamFile/examInventory";
    }

    @GetMapping("/public/examFileList")
    public String examFileList(Model model) {
        List<ExamFile> examFiles = examFileService.getAllExamFiles();
        model.addAttribute("examFiles", examFiles);

        return "ExamFile/examList";
    }

    @GetMapping("/public/examFile/public")
    public String examFileListPublic(Model model) {
        List<ExamFile> examFiles = examFileService.getPublicExamFiles();
        model.addAttribute("examFiles", examFiles);
        return "ExamFile/examList";
    }

    @GetMapping("privileged/fileUploadLimited")
    public String fileUploadLimited() {
        return "ExamFile/fileUploadLimited";
    }
}
