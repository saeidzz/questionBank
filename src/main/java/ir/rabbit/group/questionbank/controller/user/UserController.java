package ir.rabbit.group.questionbank.controller.user;


import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.exam.ExamHasUser;
import ir.rabbit.group.questionbank.model.question.*;
import ir.rabbit.group.questionbank.model.user.*;
import ir.rabbit.group.questionbank.service.abstraction.connections.UserHasConnectionsService;
import ir.rabbit.group.questionbank.service.abstraction.exam.*;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.question.*;
import ir.rabbit.group.questionbank.service.abstraction.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    ExplainedQuestionService explainedQuestionService;

    @Autowired
    TestQuestionService testQuestionService;

    @Autowired
    ExamService examService;

    @Autowired
    ExamHasUserService examHasUserService;

    @Autowired
    UserAswersQuestionService userAswersQuestionService;

    @Autowired
    MatchingQuestionService matchingQuestionService;

    @Autowired
    TrueFalseQuestionService trueFalseQuestionService;

    @Autowired
    UserAnswersTrueFalseQuestionService userAnswersTrueFalseQuestionService;

    @Autowired
    MatchingNodeService matchingNodeService;

    @Autowired
    UserAnswersMatchingQuestionService userAnswersMatchingQuestionService;

    @Autowired
    CourseInfoService courseInfoService;

    @Autowired
    UserAnswersExplainedQuestionService userAnswersExplainedQuestionService;

    @Autowired
    UserHasConnectionsService userHasConnectionsService;

    @Autowired
    ExamHasTrueFalseQuestionService examHasTrueFalseQuestionService;
    @Autowired
    ExamHasTestQuestionService examHasTestQuestionService;
    @Autowired
    ExamHasExplainedQuestionService examHasExplainedQuestionService;
    @Autowired
    ExamHasMatchingQuestionService examHasMatchingQuestionService;

    Path path;

    @GetMapping("/public/home")
    public String userHomePage(Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        int followers = userHasConnectionsService.countFollowers(user.getId());
        int followings = userHasConnectionsService.countFollowings(user.getId());
        int doesntAcceptedRequests = userHasConnectionsService.countFollowRequests(user.getId());

        model.addAttribute("doesNotAcceptedRequests", doesntAcceptedRequests);
        model.addAttribute("followersNumber", followers);
        model.addAttribute("followingsNumber", followings);
        model.addAttribute("user", user);

        List<Exam> publicexams = examService.getPublicExams();
        Collections.shuffle(publicexams);

        model.addAttribute("publicExams", publicexams.subList(0, publicexams.size() > 8 ? 7 : publicexams.size()));
        /**
         *
         List<Users> usersList=userService.getAllUsers();
         if (usersList.size()>8){
         usersList=usersList.subList(0,7);
         }
         Users currentUser=userService.getUserByUserName(request.getRemoteUser());

         model.addAttribute("users",usersList);
         model.addAttribute("currentUser",currentUser);
         */

        return "user/home";
    }


    @GetMapping("/public/userExams")
    public String getUserExams(Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        List<ExamHasUser> examHasUserList = examHasUserService.getExamHasUserByUserID(user.getId());
        List<Exam> examList = new ArrayList<Exam>();
        for (ExamHasUser examHasUser : examHasUserList) {
            examList.add(examService.getExamById(examHasUser.getExam().getId()));
        }
        model.addAttribute("userExams", examList);
        return "user/ExamList";

    }

    @GetMapping(value = "/public/participateInExam/{examID}")
    public String userParticipateInExam(@PathVariable("examID") Long examID, Model model, HttpServletRequest request) {
        Exam exam = examService.getExamById(examID);
        Date d = new Date();
        User user = userService.getUserByUserName(request.getRemoteUser());
        ExamHasUser examHasUser = examHasUserService.getExamHasUserByUserAndExamID(user.getId(), examID);
        if (!examHasUser.isExamSeen()) {
            if (exam.getLongTime() == 0 || exam.getEndTime().getTime() > d.getTime()) {

                examHasUser.setExamSeen(true);
                examHasUserService.addExamHasUser(examHasUser);
                model.addAttribute("exam", exam);
                model.addAttribute("university", user.getCourseInfo().getTitle());

                List<TestQuestion> testQuestionList = new ArrayList<>();
                examHasTestQuestionService.getExamHasTestQuestionByExamID(examID).stream().parallel().forEach(examHasTestQuestions ->
                        testQuestionList.add(examHasTestQuestions.getTestQuestion()));

                Collections.shuffle(testQuestionList);

                List<TrueFalseQuestion> trueFalseQuestionList = new ArrayList<>();

                examHasTrueFalseQuestionService.getExamHasTrueFalseQuestionByExamID(examID).stream().parallel().forEach(examHasTrueFalseQuestion ->
                        trueFalseQuestionList.add(examHasTrueFalseQuestion.getTrueFalseQuestion()));
                Collections.shuffle(trueFalseQuestionList);


                List<ExplainedQuestion> explainedQuestionList = new ArrayList<>();

                examHasExplainedQuestionService.getExamHasExpalinQuestionsByExam(examID).stream().parallel().forEach(examHasExplainQuestions ->
                        explainedQuestionList.add(examHasExplainQuestions.getExplainedQuestion()));
                Collections.shuffle(explainedQuestionList);

                List<MatchingQuestion> matchingQuestionsList = new ArrayList<>();
                List<List<MatchingNode>> alllMatchingNodes = new ArrayList<List<MatchingNode>>();

                examHasMatchingQuestionService.getExamHasMQuestionByExamID(examID).stream().parallel().forEach(examHasMatchingQuestions -> {
                    matchingQuestionsList.add(examHasMatchingQuestions.getMatchingQuestion());
                    alllMatchingNodes.add(matchingQuestionService.getMatchingNodeList(examHasMatchingQuestions.getMatchingQuestion().getId()));

                });


                Date date = new Date();

                double elapsed = date.getTime() - exam.getStartTime().getTime();
                double remained = exam.getEndTime().getTime() - date.getTime();
                double total = exam.getEndTime().getTime() - exam.getStartTime().getTime();


                model.addAttribute("matchingQuestion", matchingQuestionsList);
                model.addAttribute("listOfMatchingNodeList", alllMatchingNodes);
                model.addAttribute("trueFalseQuestion", trueFalseQuestionList);
                model.addAttribute("testQuestions", testQuestionList);
                model.addAttribute("explainedQuestion", explainedQuestionList);
                model.addAttribute("elapsed", elapsed);
                model.addAttribute("remained", remained);
                model.addAttribute("width", ((elapsed / total) * 100));
                return "Exam/participateInExam";
            } else {
                model.addAttribute("exam", exam);
                return "Exam/examEndedMessage";
            }
        } else {
            return "redirect:/public/examResult/" + examID;

        }
    }

    @PostMapping(value = "/public/participateInExam/{examID}")
    public String userParticipateInExamPost(@PathVariable("examID") Long examID, HttpServletRequest request) {
        Date date = new Date();
        if (examService.getExamById(examID).getLongTime() != 0 && date.getTime() > (examService.getExamById(examID).getEndTime().getTime() + 1200000)) {
            return "redirect:/public/delay/examEndedMessage/" + examID;
        } else {
            int i = 1;
            User user = userService.getUserByUserName(request.getRemoteUser());
            ExamHasUser examHasUser1 = examHasUserService.getExamHasUserByUserAndExamID(user.getId(), examID);
            //isDone means user participates in this exam  once and can not participate once again
            if (!examHasUser1.isDone()) {
                UserAnswersTestQuestion userAnswersQuestion;
                double d = 0;
                String userAnswer = "***بدون پاسخ**";
                while (request.getParameter("q-" + i) != null && !request.getParameter("q-" + i).equals("")) {
                    userAnswersQuestion = new UserAnswersTestQuestion();
                    if (request.getParameter("tQA-" + i) != null && !request.getParameter("tQA-" + i).equals("")) {
                        userAnswer = request.getParameter("tQA-" + i);
                    }
                    TestQuestion testQuestion = testQuestionService.getQuestionById(Long.valueOf(request.getParameter("q-" + i).trim()));
                    userAnswersQuestion.setExam(examService.getExamById(examID));
                    userAnswersQuestion.setTestQuestion(testQuestion);
                    userAnswersQuestion.setUser(user);
                    userAnswersQuestion.setUserAnswer(userAnswer);
                    userAswersQuestionService.addUserAnswersQuestions(userAnswersQuestion);
                    if (userAnswersQuestion.getUserAnswer().equals(userAnswersQuestion.getTestQuestion().getRightOption())) {
                        d += testQuestion.getScore();
                    }
                    i++;
                }

                UserAnswersExplainedQuestion userAnswersExplainedQuestion;
                while (request.getParameter("eQ-" + i) != null && !request.getParameter("eQ-" + i).equals("")) {
                    userAnswersExplainedQuestion = new UserAnswersExplainedQuestion();
                    if (request.getParameter("eQA-" + i) != null && !request.getParameter("eQA-" + i).equals("")) {
                        userAnswer = request.getParameter("eQA-" + i);
                    }
                    ExplainedQuestion explainedQuestion = explainedQuestionService.getQuestionById(Long.valueOf(request.getParameter("eQ-" + i).trim()));
                    userAnswersExplainedQuestion.setExam(examService.getExamById(examID));
                    userAnswersExplainedQuestion.setExplainedQuestion(explainedQuestion);
                    userAnswersExplainedQuestion.setUser(user);
                    userAnswersExplainedQuestion.setUserAnswer(userAnswer.trim().toLowerCase());
                    userAnswersExplainedQuestionService.addUserAnswersExplainedQuestions(userAnswersExplainedQuestion);

                    if (userAnswersExplainedQuestion.getUserAnswer().equals(userAnswersExplainedQuestion.getExplainedQuestion().getAnswer())) {
                        d += explainedQuestion.getScore();
                    }
                    i++;
                }


                UserAnswersTrueFalseQuestion userAnswersTrueFalseQuestion;
                while (request.getParameter("tfq-" + i) != null && !request.getParameter("tfq-" + i).equals("")) {
                    userAnswersTrueFalseQuestion = new UserAnswersTrueFalseQuestion();
                    if (request.getParameter("tfQA-" + i) != null && !request.getParameter("tfQA-" + i).equals("")) {
                        userAnswer = request.getParameter("tfQA-" + i);
                    }
                    TrueFalseQuestion trueFalseQuestion = trueFalseQuestionService.getQuestionById(Long.valueOf(request.getParameter("tfq-" + i).trim()));
                    userAnswersTrueFalseQuestion.setExam(examService.getExamById(examID));
                    userAnswersTrueFalseQuestion.setTrueFalseQuestion(trueFalseQuestion);
                    userAnswersTrueFalseQuestion.setUser(user);
                    userAnswersTrueFalseQuestion.setUserAnswer(userAnswer.trim().toLowerCase());
                    userAnswersTrueFalseQuestionService.addUserAnswersTrueFalseQuestions(userAnswersTrueFalseQuestion);

                    if (Boolean.parseBoolean(userAnswersTrueFalseQuestion.getUserAnswer()) == userAnswersTrueFalseQuestion.getTrueFalseQuestion().getAnswer()) {
                        d += trueFalseQuestion.getScore();
                    }
                    i++;
                }

                UserAnswersMatchingQuestions userAnswersMatchingQuestions;
                List<MatchingNode> matchingNodeList;
                while (request.getParameter("mq-" + i) != null && !request.getParameter("mq-" + i).equals("")) {
                    userAnswersMatchingQuestions = new UserAnswersMatchingQuestions();
                    Long MQID = Long.valueOf(request.getParameter("mq-" + i).trim());
                    MatchingQuestion matchingQuestion = matchingQuestionService.getQuestionById(MQID);

                    userAnswersMatchingQuestions.setMatchingQuestion(matchingQuestion);
                    matchingNodeList = matchingQuestionService.getMatchingNodeList(matchingQuestion.getId());

                    String answer = "";
                    for (int j = 0; j < matchingNodeList.size(); j++) {
                        if (request.getParameter("MNID-" + j) != null && !request.getParameter("MNID-" + j).equals("") && request.getParameter("node-" + j) != null && !request.getParameter("node-" + j).equals("")) {
                            if (matchingNodeService.getMatchingnodeById(
                                    Long.valueOf(request.getParameter("MNID-" + j).trim())).getMatchString()
                                    .equals((request.getParameter("node-" + j)))) {
                                d += (matchingQuestion.getScore() / matchingNodeList.size());
                            }
                            answer += (request.getParameter("node-" + j) + "-");
                        }
                    }
                    userAnswersMatchingQuestions.setUserAnswer(answer);
                    userAnswersMatchingQuestions.setUser(user);
                    userAnswersMatchingQuestions.setExam(examService.getExamById(examID));
                    userAnswersMatchingQuestionService.addUserAnswersMatchingQuestions(userAnswersMatchingQuestions);

                    i++;
                }


                ExamHasUser examHasUser = examHasUserService.getExamHasUserByUserAndExamID(user.getId(), examID);
                examHasUser.setScore(d);
                examHasUser.setDone(true);
                examHasUserService.addExamHasUser(examHasUser);

            }

            return "redirect:/public/examResult/" + examID;
        }
    }

    @GetMapping("/public/examResult/{examID}")
    public String getExamResultForUser(@PathVariable("examID") Long examID, Model model, HttpServletRequest request) {

        Long userID = userService.getUserByUserName(request.getRemoteUser()).getId();
        List<UserAnswersTestQuestion> userAnswersQuestion = userAswersQuestionService.getUserAnswerQuestionByExamAndUserId(userID, examID);
        List<UserAnswersTrueFalseQuestion> userAnswersTFQuestion = userAnswersTrueFalseQuestionService.getUserAnswersTrueFalseQuestions(userID, examID);
        List<UserAnswersExplainedQuestion> userAnswersEQuestion = userAnswersExplainedQuestionService.getUserAnswersExplainedQuestions(userID, examID);
        List<UserAnswersMatchingQuestions> userAnswersMQuestion = userAnswersMatchingQuestionService.getUserAnswerMatchingQuestionByExamAndUserId(userID, examID);


        ExamHasUser examHasUser = examHasUserService.getExamHasUserByUserAndExamID(userID, examID);
        model.addAttribute("showAnswers", false);
        model.addAttribute("examWholeScore", examService.getExamById(examID).getScore());
        model.addAttribute("score", examHasUser.getScore());
        model.addAttribute("uATQ", userAnswersQuestion);
        model.addAttribute("uATFQ", userAnswersTFQuestion);
        model.addAttribute("uEQ", userAnswersEQuestion);
        model.addAttribute("uAMQ", userAnswersMQuestion);


        return "Exam/examResult";
    }

    @GetMapping("/public/examResult/{examID}/{userId}")
    public String getExamResultForExamCreator(@PathVariable("examID") Long examID, @PathVariable("userId") Long userId, Model model, HttpServletRequest request) {

        List<UserAnswersTestQuestion> userAnswersQuestion = userAswersQuestionService.getUserAnswerQuestionByExamAndUserId(userId, examID);
        List<UserAnswersTrueFalseQuestion> userAnswersTFQuestion = userAnswersTrueFalseQuestionService.getUserAnswersTrueFalseQuestions(userId, examID);
        List<UserAnswersExplainedQuestion> userAnswersEQuestion = userAnswersExplainedQuestionService.getUserAnswersExplainedQuestions(userId, examID);
        List<UserAnswersMatchingQuestions> userAnswersMQuestion = userAnswersMatchingQuestionService.getUserAnswerMatchingQuestionByExamAndUserId(userId, examID);

        ExamHasUser examHasUser = examHasUserService.getExamHasUserByUserAndExamID(userId, examID);

        model.addAttribute("showAnswers", true);
        model.addAttribute("examWholeScore", examService.getExamById(examID).getScore());
        model.addAttribute("score", examHasUser.getScore());
        model.addAttribute("uATQ", userAnswersQuestion);
        model.addAttribute("uATFQ", userAnswersTFQuestion);
        model.addAttribute("uEQ", userAnswersEQuestion);
        model.addAttribute("uAMQ", userAnswersMQuestion);


        return "Exam/examResult";
    }


    @GetMapping("/public/delay/examEndedMessage/{examId}")
    public String wellformedMessage(@PathVariable("examId") Long examId, Model model) {
        model.addAttribute("exam", examService.getExamById(examId));
        model.addAttribute("message", "تاخیر شما بیش از 20 دقیقه بوده و متاسفانه آزمون شما بررسی نمی شود.");
        return "Exam/examEndedMessage";
    }

    @GetMapping(value = "/public/user/editUserInfo")
    public String editUserInfo(HttpServletRequest request, Model model) {
        String uname = request.getRemoteUser();
        User users = userService.getUserByUserName(uname);
        model.addAttribute("Users", users);
        return "user/editUserInfo";
    }

    @PostMapping(value = "/public/user/editUserInfo")
    public String editUserInfoPost(@ModelAttribute("Users") @Valid User user, BindingResult result, HttpServletRequest request) {

        if (result.hasErrors()) {
            return "user/editUserInfo";
        }

        Long ExistCourseId = courseInfoService.ExistCourse(user.getCourseInfo());
        Long uuid = 0l;

        if (ExistCourseId != uuid) {
            user.setCourseInfo(courseInfoService.getCourseInfoById(ExistCourseId));
        } else {
            courseInfoService.addCourseInfo(user.getCourseInfo());
        }

        if (user.getUserPic() != null) {
            MultipartFile userPic = user.getUserPic();
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "\\WEB-INF\\res\\userPics\\" + user.getId() + ".png");

            if (userPic != null && !userPic.isEmpty()) {
                try {
                    userPic.transferTo(new File(path.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("user Image saving failed !");
                }
            }
        }

        userService.editUser(user);

        return "redirect:/public/home";

    }


    @GetMapping("/public/delay/thisUserPublicExams/{userId}")
    public String userPublicExams(Model model, @PathVariable("userId") Long userId) {

        List<Exam> exams = examService.getSpecificUserPublicExam(userId);
        model.addAttribute("examList", exams);

        return "Exam/publicExamList";
    }


    @GetMapping("/public/thisUserPublicTestQuestions/{userId}")
    public String userPublicTestQuestions(Model model, @PathVariable("userId") Long userId) {

        List<TestQuestion> testQuestions = testQuestionService.getSpecificUserPublicTestQuestions(userId);

        model.addAttribute("testQuestions", testQuestions);
        return "user/testQuestions";
    }

    @GetMapping("/public/thisUserPublicTrueFalseQuestions/{userId}")
    public String userPublicTrueFalseQuestions(Model model, @PathVariable("userId") Long userId) {

        List<TrueFalseQuestion> trueFalseQuestions = trueFalseQuestionService.getSpecificUserPublicTrueFalseQuestion(userId);
        model.addAttribute("trueFalseQuestions", trueFalseQuestions);
        return "user/trueFalseQuestions";
    }

    @GetMapping("/public/thisUserPublicExplainedQuestions/{userId}")
    public String userPublicExplainedQuestions(Model model, @PathVariable("userId") Long userId) {

        List<ExplainedQuestion> explainedQuestions = explainedQuestionService.getSpecificUserPublicExplainedQuestions(userId);

        model.addAttribute("explainedQuestions", explainedQuestions);
        return "user/explainedQuestions";
    }

    @GetMapping("/public/thisUserPublicMatchingQuestions/{userId}")
    public String userPublicMatchingQuestions(Model model, @PathVariable("userId") Long userId) {
        List<MatchingQuestion> matchingQuestions = matchingQuestionService.getSpecificUserPublicMatchingQuestions(userId);
        model.addAttribute("matchingQuestions", matchingQuestions);
        return "user/matchingQuestions";
    }


}
