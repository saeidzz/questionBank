package ir.rabbit.group.questionbank.controller.questions;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteTestQuestion;
import ir.rabbit.group.questionbank.model.compositeId.UserTestQuestionId;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteTestQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.question.TestQuestionService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
public class TestQuestionController {
    @Autowired
    CourseInfoService courseInfoService;
    @Autowired
    TestQuestionService testQuestionService;
    @Autowired
    UserService userService;
    @Autowired
    FavoriteTestQuestionService favoriteTestQuestionService;
    private Path path;

    @GetMapping("/privileged/question/addTestQuestion")
    public String addTestQuestion(Model model) {
        CourseInfo courseInfo = new CourseInfo();
        TestQuestion question = new TestQuestion();
        question.setContent("محتوای سوال را اینجا وارد کنید");
        question.setScore(1.0f);
        question.setCourseInfo(courseInfo);
        model.addAttribute("testQuestion", question);

        return "TestQuestion/addTestQuestion";
    }

    @PostMapping(value = "/privileged/question/addTestQuestion")
    public String addTestQuestionPost(@Valid @ModelAttribute("testQuestion") TestQuestion question, BindingResult result, HttpServletRequest request, Model model) {
        //    if(request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

        if (result.hasErrors()) {
            return "TestQuestion/addTestQuestion";
        }
        Long ExistOrNot = courseInfoService.ExistCourse(question.getCourseInfo());
        Long uuid = 0l;

        if (ExistOrNot == uuid) {
            courseInfoService.addCourseInfo(question.getCourseInfo());
        } else {
            question.setCourseInfo(courseInfoService.getCourseInfoById(ExistOrNot));
        }


        question.setUser(userService.getUserByUserName(request.getRemoteUser()));

        testQuestionService.addQuestion(question);

        saveTestQImages(question, request);

        return "redirect:/privileged/questionTestInventory";
        //   }else{
        //     model.addAttribute("tryAgain","لطفا عبارت امنیتی را با دقت وارد نمایید!!!");

        //   return "TestQuestion/addTestQuestion";
        // }
    }

    @GetMapping("/privileged/questionTestInventory/editTestQuestion/{id}")
    public String editTestQuestion(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        TestQuestion question = testQuestionService.getQuestionById(id);
        if (question.getUser().equals(user)) {
            model.addAttribute("testQuestion", question);
            return "TestQuestion/editTestQuestion";
        } else {
            user.setEnabled(false);
            userService.editUser(user);
            return "redirect:/";
        }
    }

    @PostMapping(value = "/privileged/questionTestInventory/editTestQuestion")
    public String editTestQuestionPost(@Valid @ModelAttribute("testQuestion") TestQuestion testQuestion, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "TestQuestion/editTestQuestion";
        }

        Long ExistOrNot = courseInfoService.ExistCourse(testQuestion.getCourseInfo());
        Long uuid = 0l;

        if (ExistOrNot.equals(uuid)) {
            courseInfoService.addCourseInfo(testQuestion.getCourseInfo());
        } else {
            testQuestion.setCourseInfo(courseInfoService.getCourseInfoById(ExistOrNot));
        }
        testQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));
        testQuestionService.editQuestion(testQuestion);
        saveTestQImages(testQuestion, request);
        return "redirect:/privileged/questionTestInventory";
    }

    @GetMapping("/privileged/question/deleteTestQuestion/{id}")
    public String deleteTestQuestion(@PathVariable Long id, HttpServletRequest request) {
    /*
     String rootDir = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDir+ "/WEB-INF/res/productsImage/" +id+ ".png");

        if (Files.exists(path)){
            try{
                Files.delete(path);
            }catch (Exception e){
                e.prlongStackTrace();
            }
        }
     */
        User user = userService.getUserByUserName(request.getRemoteUser());
        TestQuestion testQuestion = testQuestionService.getQuestionById(id);
        if (testQuestion.getUser().equals(user)) {
            testQuestionService.deleteQuestion(testQuestion);
        } else {
            user.setEnabled(false);
            userService.editUser(user);
        }
        return "redirect:/privileged/questionTestInventory";
    }

    private void saveTestQImages(@Valid @ModelAttribute("saveTestQuestion") TestQuestion testQuestion, HttpServletRequest request) {
        if (testQuestion.getQuestionImage() != null) {
            MultipartFile questionImage = testQuestion.getQuestionImage();
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "/WEB-INF/res/questionImages/" + testQuestion.getId() + "test-q.png");

            if (questionImage != null && !questionImage.isEmpty()) {
                try {
                    questionImage.transferTo(new File(path.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("question Image saving failed !");
                }
            }
        }
        if (testQuestion.getAsnswerImage() != null) {
            MultipartFile answerImage = testQuestion.getAsnswerImage();
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "/WEB-INF/res/questionImages/" + testQuestion.getId() + "test-a.png");

            if (answerImage != null && !answerImage.isEmpty()) {
                try {
                    answerImage.transferTo(new File(path.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("answer Image saving failed !");
                }
            }
        }
    }

    @GetMapping(value = "/privileged/question/addTestQuestionSet")
    public String DynamicAddGet() {
        return "TestQuestion/addTestQuestionSet";

    }

    @PostMapping(value = "/privileged/question/addTestQuestionSet")
    public String DynamicTestQuestionAdd(HttpServletRequest request) {
        //    if(request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

        long i = 1;

        String title = request.getParameter("optone");

        String seasons = request.getParameter("seasons");
        String subjectOrCourseTitle = request.getParameter("subjectOrCourseTitle");

        CourseInfo courseInfo = new CourseInfo();
        courseInfo.setTitle(title);


        Long ExistCourseId = courseInfoService.ExistCourse(courseInfo);
        Long uuid = 0l;

        if (ExistCourseId != uuid) {
            courseInfo = courseInfoService.getCourseInfoById(ExistCourseId);
        } else if (ExistCourseId == uuid) {
            courseInfoService.addCourseInfo(courseInfo);
        }


        while (request.getParameter("qContent" + i) != null) {
            TestQuestion testQuestion = new TestQuestion();
            testQuestion.setCourseInfo(courseInfo);
            testQuestion.setSubjectOrCourseTitle(subjectOrCourseTitle);

            String qcontent = request.getParameter("qContent" + i);
            String qOption1 = request.getParameter("optionOne" + i);
            String qOption2 = request.getParameter("optionTwo" + i);
            String qOption3 = request.getParameter("optionThree" + i);
            String qOption4 = request.getParameter("optionFour" + i);
            String score = request.getParameter("score" + i);
            String qRightAnswer = request.getParameter("optionFive" + i);
            String qLevel = request.getParameter("level" + i);
            String ispublic = request.getParameter("ispublic" + i);

            testQuestion.setContent(qcontent);
            testQuestion.setOptionA(qOption1);
            testQuestion.setOptionB(qOption2);

            if (!qOption3.equals("") && qOption3 != "" && qOption3 != null) {
                testQuestion.setOptionC(qOption3);
            }
            if (!qOption4.equals("") && qOption4 != "" && qOption4 != null) {
                testQuestion.setOptionD(qOption4);
            }
            testQuestion.setRightOption(qRightAnswer);
            testQuestion.setLevel(Integer.parseInt(qLevel));
            testQuestion.setSeasons(seasons);
            testQuestion.setUser(userService.getUserByUserName(request.getRemoteUser()));
            testQuestion.setPublic(ispublic.trim().equals("true"));
            testQuestion.setScore(Float.parseFloat(score.trim()) > 0 ? Float.parseFloat(score.trim()) : 1.0f);
            testQuestionService.addQuestion(testQuestion);

            i++;
        }

        return "redirect:/privileged/questionTestInventory";
        //   }else {
        //     model.addAttribute("tryAgain","لطفا عبارت امنیتی را با دقت وارد نمایید!!!");

//            return "TestQuestion/addTestQuestionSet";
        //      }
    }

    @GetMapping("/public/question/viewTestQuestion/{questionId}")
    public String viewQuestion(@PathVariable Long questionId, Model model) throws IOException {
        TestQuestion question = testQuestionService.getQuestionById(questionId);
        model.addAttribute("question", question);
        return "TestQuestion/viewTestQuestion";

    }


    @GetMapping("/privileged/questionTestInventory")
    public String questionInventory(Model model, HttpServletRequest request) {
        User user = userService.getUserByUserName(request.getRemoteUser());
        List<TestQuestion> questions = testQuestionService.getByOwnerUser(user.getId());
        model.addAttribute("testQuestions", questions);
        return "TestQuestion/questionTestInventory";
    }

    @GetMapping("/public/user/home/testQuestions/public")
    public String getUserTestQuestions(Model model, HttpServletRequest request) {
        List<TestQuestion> userTestList = testQuestionService.getPublicTestQuestions();
        List<String> favoriteTQuestions = favoriteTestQuestionService.getFavoriteTQIdOfSpecificUser(userService.getUserByUserName(request.getRemoteUser()).getId());
        model.addAttribute("favoriteTQuestionsId", favoriteTQuestions);
        model.addAttribute("testQuestions", userTestList);

        return "user/testQuestions";

    }

    @PostMapping(value = "/privileged/addTQuestionToFavorites")
    public @ResponseBody
    String addFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {

        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteTestQuestion favoriteTestQuestion = new FavoriteTestQuestion();
            favoriteTestQuestion.setUserTestQuestionId(UserTestQuestionId.of(QID, userId));
            favoriteTestQuestionService.addToMyFavoriteTestQuestions(favoriteTestQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }

    @PostMapping(value = "/privileged/removeTQuestionFromFavorites")
    public @ResponseBody
    String deleteFavoriteQuestion(@RequestBody Long QID, HttpServletRequest request) {
        String res = "true";
        try {
            Long userId = userService.getUserByUserName(request.getRemoteUser()).getId();
            FavoriteTestQuestion favoriteTestQuestion = new FavoriteTestQuestion();
            favoriteTestQuestion.setUserTestQuestionId(UserTestQuestionId.of(QID, userId));
            favoriteTestQuestionService.deleteFromMyFavoriteTestQuestions(favoriteTestQuestion);
        } catch (Exception e) {
            res = "false";
        }
        return res + "";
    }


}
