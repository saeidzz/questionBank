package ir.rabbit.group.questionbank.controller.captcha;

import io.swagger.annotations.Api;
import ir.rabbit.group.questionbank.exception.CanNotBuildCaptchaException;
import ir.rabbit.group.questionbank.service.abstraction.captcha.CaptchaService;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@Api(tags = "captcha")
public class CaptchaController {

    @Autowired
    CourseInfoService courseInfoService;

    @Autowired
    UserService userService;

    @Autowired
    CaptchaService captchaService;

    @GetMapping(value = "/captcha")
    public Map captchaGenerator(HttpServletResponse response) {
        String generatedStrValue = captchaService.generateCaptchaText(6);
        Map<String, String> captchaMap = new HashMap<String, String>();
        captchaMap.put("captchaValue", generatedStrValue);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {

            ImageIO.write(captchaService.renderImage(generatedStrValue), "jpg", os);

            captchaMap.put("captchaImage", new String(Base64.encodeBase64(os.toByteArray()), "UTF-8"));
        } catch (IOException e) {
            throw new CanNotBuildCaptchaException(e.getMessage());
        }
        return captchaMap;
    }


}
