package ir.rabbit.group.questionbank.controller.highPrivilege;

import io.swagger.annotations.Api;
import ir.rabbit.group.questionbank.model.compositeId.*;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.occuredException.OccurredException;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.reported.*;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.occuredException.OccuredExceptionService;
import ir.rabbit.group.questionbank.service.abstraction.reported.*;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "superAdmin")
public class SuperAdminController {

    @Autowired
    UserService userService;

    @Autowired
    OccuredExceptionService occuredExceptionService;

    @Autowired
    ReportedExamService reportedExamService;

    @Autowired
    ReportedExplainedQuestionService reportedExplainedQuestionService;

    @Autowired
    ReportedMatchingQuestionService reportedMatchingQuestionService;

    @Autowired
    ReportedTestQuestionService reportedTestQuestionService;

    @Autowired
    ReportedTrueFalseQuestionService reportedTrueFalseQuestionService;


    @GetMapping("/highPrivilege/allUsers")
    public String allUsers(Model model) {
        List<User> usersList = userService.getAllUsers();
        model.addAttribute("allUsuers", usersList);
        return "superAdmin/allUsers";
    }

    @GetMapping("/highPrivilege/deleteUser/{id}")
    public String deleteUser(@PathVariable("id") Long id, Model model) {
        userService.deleteUser(userService.getUserById(id));
        List<User> usersList = userService.getAllUsers();
        model.addAttribute("allUsuers", usersList);
        return "superAdmin/allUsers";
    }

    @GetMapping("/highPrivilege/allExceptions")
    public String allExceptions(Model model) {
        List<OccurredException> exceptions = occuredExceptionService.getAllOccuredExceptions();
        model.addAttribute("allExceptions", exceptions);
        return "superAdmin/allExceptions";
    }

    @GetMapping("/highPrivilege/allReportedEQ")
    public String allReportedEQ(Model model) {
        List<ExplainedQuestion> explainedQuestions = reportedExplainedQuestionService.getAllUserReportedExplainedQuestions();

        model.addAttribute("explainedQuestions", explainedQuestions);
        return "superAdmin/explainedQuestionInventory";
    }

    @GetMapping("/highPrivilege/allReportedMQ")
    public String allReportedMQ(Model model) {
        List<MatchingQuestion> matchingQuestions = reportedMatchingQuestionService.getAllUserReportedMatchingQuestions();


        model.addAttribute("matchingQuestions", matchingQuestions);
        return "superAdmin/matchingQuestionInventory";
    }


    @GetMapping("/highPrivilege/allReportedTQ")
    public String allReportedETQ(Model model) {
        List<TestQuestion> testQuestions = reportedTestQuestionService.getAllUserReportedTestQuestion();

        model.addAttribute("testQuestions", testQuestions);
        return "superAdmin/questionTestInventory";
    }

    @GetMapping("/highPrivilege/allReportedTFQ")
    public String allReportedTFQ(Model model) {
        List<TrueFalseQuestion> trueFalseQuestions = reportedTrueFalseQuestionService.getAllUserReportedTrueFalseQuestions();

        model.addAttribute("trueFalseQuestions", trueFalseQuestions);
        return "superAdmin/trueFalseQuestionInventory";
    }

    @GetMapping("/highPrivilege/allReportedExams")
    public String allReportedExams(Model model) {
        List<Exam> exams = reportedExamService.getAllUserReportedExams();

        model.addAttribute("examList", exams);
        return "superAdmin/examInventory";
    }


    @PostMapping(value = "/highPrivilege/reportEQ")
    public @ResponseBody
    String addReportedEQ(@RequestBody Long QID, HttpServletRequest request) {

        String res = "true";
        try {
            ReportedExplainedQuestion reportedExplainedQuestion = new ReportedExplainedQuestion();
            reportedExplainedQuestion.setUserExplainedQuestionId(UserExplainedQuestionId.of(QID,userService.getUserByUserName(request.getRemoteUser()).getId()));


            reportedExplainedQuestionService.addToMyReportedExplainedQuestions(reportedExplainedQuestion);

        } catch (Exception e) {
            e.printStackTrace();
            res = "false";
        }
        return res;
    }

    @PostMapping(value = "/highPrivilege/reportMQ")
    public @ResponseBody
    String addReportedMQ(@RequestBody Long QID, HttpServletRequest request) {
        String res = "true";
        try {
            ReportedMatchingQuestion reportedMatchingQuestion = new ReportedMatchingQuestion();
            reportedMatchingQuestion.setUserMatchingQuestionId(UserMatchingQuestionId.of(QID,userService.getUserByUserName(request.getRemoteUser()).getId()));

            reportedMatchingQuestionService.addToMyReportedMatchingQuestion(reportedMatchingQuestion);

        } catch (Exception e) {
            e.printStackTrace();
            res = "false";
        }
        return res;
    }

    @PostMapping(value = "/highPrivilege/reportTQ")
    public @ResponseBody
    String addReportedTQ(@RequestBody Long QID, HttpServletRequest request) {

        String res = "true";
        try {
            ReportedTestQuestion reportedTestQuestion = new ReportedTestQuestion();
            reportedTestQuestion.setUserTestQuestionId(UserTestQuestionId.of(QID,userService.getUserByUserName(request.getRemoteUser()).getId()));

            reportedTestQuestionService.addToMy(reportedTestQuestion);

        } catch (Exception e) {
            e.printStackTrace();
            res = "false";
        }
        return res;

    }

    @PostMapping(value = "/highPrivilege/reportTFQ")
    public @ResponseBody
    String addReportedTFQ(@RequestBody Long QID, HttpServletRequest request) {

        String res = "true";
        try {
            ReportedTrueFalseQuestion reportedTrueFalseQuestion = new ReportedTrueFalseQuestion();
            reportedTrueFalseQuestion.setUserTrueFalseQuestionId(UserTrueFalseQuestionId.of(QID,userService.getUserByUserName(request.getRemoteUser()).getId()));

            reportedTrueFalseQuestionService.addToMyReportedTrueFalseQuestions(reportedTrueFalseQuestion);

        } catch (Exception e) {
            e.printStackTrace();
            res = "false";
        }
        return res;

    }

    @PostMapping(value = "/highPrivilege/reportExam")
    public @ResponseBody
    String addReportedExam(@RequestBody Long examID, HttpServletRequest request) {


        String res = "true";
        try {
            ReportedExam reportedExam = new ReportedExam();
            reportedExam.setUserExamId(UserExamId.of(examID,userService.getUserByUserName(request.getRemoteUser()).getId()));
            reportedExamService.addToMyReportedExams(reportedExam);

        } catch (Exception e) {
            e.printStackTrace();
            res = "false";
        }
        return res;
    }


}
