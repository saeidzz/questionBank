package ir.rabbit.group.questionbank.controller.loginAndRegister;

import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class RegisterController {
    @Autowired
    UserService usersService;
    private Path path;
    @Autowired
    private CourseInfoService courseInfoService;


    @GetMapping("/Register")
    public String registerStudent(Model model) {
        User user = new User();


        model.addAttribute("Users", user);
        return "user/Register";

    }

    @PostMapping(value = "/Register")
    public String registerStudentPost(@ModelAttribute("Users") @Valid User user, BindingResult result,
                                      HttpServletRequest request, Model model) {
        if (request.getSession().getAttribute("captchaValue").toString().toLowerCase().equals(request.getParameter("captcha").toLowerCase())) {

            if (result.hasErrors()) {
                return "user/Register";
            }

            Long ExistCourseId = courseInfoService.ExistCourse(user.getCourseInfo());
            Long uuid = 0L;

            if (ExistCourseId != uuid) {
                user.setCourseInfo(courseInfoService.getCourseInfoById(ExistCourseId));
            } else {
                courseInfoService.addCourseInfo(user.getCourseInfo());
            }

            user.setEnabled(true);
            usersService.addUser(user);

            if (user.getUserPic() != null) {
                MultipartFile userPic = user.getUserPic();
                String rootDirectory = request.getSession().getServletContext().getRealPath("/");
                path = Paths.get(rootDirectory + "/WEB-INF/res/userPics/" + user.getId() + ".png");

                if (userPic != null && !userPic.isEmpty()) {
                    try {
                        userPic.transferTo(new File(path.toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException("user Image saving failed !");
                    }
                }
            }


            return "redirect:/public/home";
        } else {
            model.addAttribute("tryAgain", "لطفا عبارت امنیتی را با دقت وارد نمایید!!!");
            return "user/Register";
        }
    }


    @PostMapping(value = "/test/user/validateEmail")
    public @ResponseBody
    String emailValidate(@RequestBody String mail) {
        boolean mailExist = usersService.existMailAdd(mail);
        return mailExist + "";
    }

    @PostMapping(value = "/test/user/validateUserName")
    public @ResponseBody
    String userNameValidator(@RequestBody String userName) {
        boolean userNameExist = usersService.existUserName(userName);

        return userNameExist + "";
    }
}

