package ir.rabbit.group.questionbank.repository.exam;

import ir.rabbit.group.questionbank.model.exam.*;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;


@Repository
public interface ExamRepository extends JpaRepository<Exam,Long> {



    List<Exam> findAll();

    List<Exam> findByUser_Id(Long userId);

    List<Exam> findByIsPublicTrue();

    List<Exam> findByIsPublicTrueAndUser_Id(Long userId);
}

