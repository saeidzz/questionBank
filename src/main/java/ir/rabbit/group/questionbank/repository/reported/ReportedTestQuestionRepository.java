package ir.rabbit.group.questionbank.repository.reported;

import ir.rabbit.group.questionbank.model.compositeId.UserTestQuestionId;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedTestQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface ReportedTestQuestionRepository extends JpaRepository<ReportedTestQuestion, UserTestQuestionId> {

}
