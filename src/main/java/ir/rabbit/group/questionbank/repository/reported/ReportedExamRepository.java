package ir.rabbit.group.questionbank.repository.reported;

import ir.rabbit.group.questionbank.model.compositeId.UserExamId;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.reported.ReportedExam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface ReportedExamRepository extends JpaRepository<ReportedExam, UserExamId> {
}
