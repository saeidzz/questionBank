package ir.rabbit.group.questionbank.repository.exam;

import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.exam.ExamFile;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;


@Repository
public interface ExamFileRepository extends JpaRepository<ExamFile,Long> {

    List<ExamFile> findAll();

    void deleteById(Long id);

    List<ExamFile> findByUser_Id(Long userId);

    List<ExamFile> findByIsPublicTrue();
}
