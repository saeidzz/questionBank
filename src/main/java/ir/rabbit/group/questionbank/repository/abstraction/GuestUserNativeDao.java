package ir.rabbit.group.questionbank.repository.abstraction;

import java.util.List;

public interface GuestUserNativeDao {
    List<String> getAllGuestUsersMails(Long examId);
}
