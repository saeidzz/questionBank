package ir.rabbit.group.questionbank.repository.abstraction;

import ir.rabbit.group.questionbank.model.user.User;

import java.util.List;

public interface UserNativeDao {
    List<User> getDoesNotAcceptedFollowers(Long userId);

    List<User> getDoesNotAcceptedFollowings(Long userId);

    List<User> getAcceptedFollowers(Long userId);

    List<User> getAcceptedFollowings(Long userId);

    List<User> getExamAddedUsers(Long examId);
}
