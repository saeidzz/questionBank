package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersTestQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface UserAnswersQuestionRepository extends JpaRepository<UserAnswersTestQuestion,Long> {
    List<UserAnswersTestQuestion> findByUser_IdAndExam_Id(Long userId, Long examId);


}
