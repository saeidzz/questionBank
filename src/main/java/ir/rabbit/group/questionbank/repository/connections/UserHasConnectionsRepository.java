package ir.rabbit.group.questionbank.repository.connections;

import ir.rabbit.group.questionbank.model.connection.UserHasConnections;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import org.springframework.stereotype.Repository;


@Repository
public interface UserHasConnectionsRepository extends JpaRepository<UserHasConnections,Long> {


    List<UserHasConnections> findAll();

    void deleteById(Long id);

    int countUserHasConnectionsByAcceptedTrueAndConnectedToThis_Id(Long id);

    int countUserHasConnectionsByAcceptedTrueAndThisConnectedTo_Id(Long id);

    UserHasConnections findByConnectedToThis_IdAndThisConnectedTo_Id(Long connectToThisId, Long thisConnectToID);

    void deleteByConnectedToThis_IdAndThisConnectedTo_Id(Long connectToThisId, Long thisWanaConnectToID);

    int countUserHasConnectionsByAcceptedFalseAndConnectedToThis_Id(Long id);

}
