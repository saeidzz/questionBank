package ir.rabbit.group.questionbank.repository.impl;

import ir.rabbit.group.questionbank.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class UserNativeDaoImpl implements ir.rabbit.group.questionbank.repository.abstraction.UserNativeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<User> getDoesNotAcceptedFollowers(Long userId) {
        Query query = entityManager.createQuery("from User u where u.id IN (select c.thisConnectedTo.id  from UserHasConnections c where c.connectedToThis.id=? and c.accepted=?)");
        query.setParameter(0, userId);
        query.setParameter(1, false);
        return query.getResultList();
    }

    @Override
    public List<User> getDoesNotAcceptedFollowings(Long userId) {
        Query query = entityManager.createQuery("from User u where u.id In (select c.connectedToThis.id from UserHasConnections c where c.thisConnectedTo.id=? and c.accepted=?)");
        query.setParameter(0, userId);
        query.setParameter(1, false);
        return query.getResultList();
    }

    @Override
    public List<User> getAcceptedFollowers(Long userId) {
        Query query = entityManager.createQuery("from User u where u.id IN (select c.thisConnectedTo.id  from UserHasConnections c where c.connectedToThis.id=? and c.accepted=?)");
        query.setParameter(0, userId);
        query.setParameter(1, true);
        return query.getResultList();
    }

    @Override
    public List<User> getAcceptedFollowings(Long userId) {
        Query query = entityManager.createQuery("from User u where u.id In (select c.connectedToThis.id from UserHasConnections c where c.thisConnectedTo.id=? and c.accepted=?)");
        query.setParameter(0, userId);
        query.setParameter(1, true);
        return query.getResultList();
    }

    @Override
    public List<User> getExamAddedUsers(Long examId) {
        Query query = entityManager.createQuery("from User u where u.id in (select ehu.user.id from ExamHasUser ehu where ehu.exam.id=?)");
        query.setParameter(0, examId);
        return query.getResultList();
    }
}
