package ir.rabbit.group.questionbank.repository.occuredException;

import ir.rabbit.group.questionbank.model.occuredException.OccurredException;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface OccurredExceptionRepository extends JpaRepository<OccurredException,Long> {

    List<OccurredException> findAll();
}
