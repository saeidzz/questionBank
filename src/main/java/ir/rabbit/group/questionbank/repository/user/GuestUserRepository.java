package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.GuestUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface GuestUserRepository extends JpaRepository<GuestUser,Long> {

    List<GuestUser> findByExam_Id(Long examId);

    List<GuestUser> findAll();


}
