package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersTestQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface GuestUserAnswersTestQuestionRepository extends JpaRepository<GuestUserAnswersTestQuestion,Long> {

    List<GuestUserAnswersTestQuestion> findAll();

    List<GuestUserAnswersTestQuestion> findByGuestUser_Id(Long userId);


}
