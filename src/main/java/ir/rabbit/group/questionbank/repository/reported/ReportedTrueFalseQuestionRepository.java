package ir.rabbit.group.questionbank.repository.reported;

import ir.rabbit.group.questionbank.model.compositeId.UserTrueFalseQuestionId;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedTrueFalseQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface ReportedTrueFalseQuestionRepository extends JpaRepository<ReportedTrueFalseQuestion, UserTrueFalseQuestionId> {

}
