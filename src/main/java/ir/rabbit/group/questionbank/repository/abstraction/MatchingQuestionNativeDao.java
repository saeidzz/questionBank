package ir.rabbit.group.questionbank.repository.abstraction;

import ir.rabbit.group.questionbank.model.question.MatchingQuestion;

import java.util.List;

public interface MatchingQuestionNativeDao {
    List<MatchingQuestion> getExamMatchingQuestions(Long examID);

    List<MatchingQuestion> getFavoriteMQuestionsOfSpecificUser(Long userId);

    List<MatchingQuestion> getAllUserReportedMatchingQuestions();

    List<MatchingQuestion> getMQuestionForExam(Long userId, Long courseId);
}
