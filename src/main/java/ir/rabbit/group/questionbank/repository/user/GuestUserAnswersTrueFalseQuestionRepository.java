package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersTrueFalseQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface GuestUserAnswersTrueFalseQuestionRepository extends JpaRepository<GuestUserAnswersTrueFalseQuestion,Long> {
 List<GuestUserAnswersTrueFalseQuestion> findAll();

    List<GuestUserAnswersTrueFalseQuestion> findByGuestUser_Id(Long userId);

}
