package ir.rabbit.group.questionbank.repository.question;

import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface MatchingQuestionRepository extends JpaRepository<MatchingQuestion, Long> {
    List<MatchingQuestion> findAll();

    List<MatchingQuestion> findByUser_Id(Long userId);

    List<MatchingQuestion> findByCourseInfo_Id(Long courseID);

    List<MatchingQuestion> findByIsPublicTrue();

    List<MatchingQuestion> findByIsPublicTrueAndUser_Id(Long userId);


}
