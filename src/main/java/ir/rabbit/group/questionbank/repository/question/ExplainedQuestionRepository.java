package ir.rabbit.group.questionbank.repository.question;

import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ExplainedQuestionRepository extends JpaRepository<ExplainedQuestion,Long> {


    List<ExplainedQuestion> findAll();

    List<ExplainedQuestion> findByUser_Id(Long userId);

    List<ExplainedQuestion> findByCourseInfo_Id(Long courseID);

    List<ExplainedQuestion> findByIsPublicTrue();

    List<ExplainedQuestion> findByIsPublicTrueAndUser_Id(Long userId);

}
