package ir.rabbit.group.questionbank.repository.favorite;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteExam;
import ir.rabbit.group.questionbank.model.compositeId.UserExamId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface FavoriteExamRepository extends JpaRepository<FavoriteExam, UserExamId> {


    Optional<FavoriteExam> findByUserExamId_UserIdAndUserExamId_ExamId(Long userId, Long examId);

    List<FavoriteExam> findByUserExamId_UserId(Long userId);

    @Query("select f.userExamId.examId from  FavoriteExam f where f.userExamId.userId =:userId")
    List<String> getFavoriteExamsIdOfSpecificUser(@Param("userId") Long userId);

}
