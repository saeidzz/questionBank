package ir.rabbit.group.questionbank.repository.abstraction;

import ir.rabbit.group.questionbank.model.question.TestQuestion;

import java.util.List;

public interface TestQuestionNativeDao {
    List<TestQuestion> getExamTestQuestions(Long examId);

    List<TestQuestion> getFavoriteTQuestionsOfSpecificUser(Long userId);

    List<TestQuestion> getAllUserReportedTestQuestion();

    List<TestQuestion> getTestQuestionForExam(Long userId, Long courseId);
}
