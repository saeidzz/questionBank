package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersTrueFalseQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface UserAnswerTrueFalseQuestionRepository extends JpaRepository<UserAnswersTrueFalseQuestion,Long> {

    List<UserAnswersTrueFalseQuestion> findByUser_IdAndExam_Id(Long userId, Long examId);

}
