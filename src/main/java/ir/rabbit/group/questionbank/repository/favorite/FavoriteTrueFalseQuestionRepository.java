package ir.rabbit.group.questionbank.repository.favorite;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteTrueFalseQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface FavoriteTrueFalseQuestionRepository extends JpaRepository<FavoriteTrueFalseQuestion,Long> {

    Optional<FavoriteTrueFalseQuestion> findByUserTrueFalseQuestionId_UserIdAndUserTrueFalseQuestionId_TrueFalseQuestionId(Long userId, Long TFQuestionId);

    List<FavoriteTrueFalseQuestion> findByUserTrueFalseQuestionId_UserId(Long userId);

    @Query("select f.userTrueFalseQuestionId.trueFalseQuestionId from FavoriteTrueFalseQuestion f where f.userTrueFalseQuestionId.userId=:userId")
    List<String> getFavoriteTFQuestionIdsOfSpecificUser(@Param("userId") Long userId);

}
