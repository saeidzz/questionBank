package ir.rabbit.group.questionbank.repository.abstraction;

import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;

import java.util.List;

public interface TrueFalseQuestionNativeDao {
    List<TrueFalseQuestion> getExamTQuestions(Long examID);

    List<TrueFalseQuestion> getFavoriteTFQuestionsOfSpecificUser(Long id);

    List<TrueFalseQuestion> getAllUserReportedTrueFalseQuestions();

    List<TrueFalseQuestion> getTFQuestionForExam(Long userId, Long courseId);
}
