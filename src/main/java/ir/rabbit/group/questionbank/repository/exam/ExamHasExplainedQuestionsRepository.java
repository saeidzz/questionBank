package ir.rabbit.group.questionbank.repository.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasExplainedQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExamHasExplainedQuestionsRepository extends JpaRepository<ExamHasExplainedQuestion,Long> {

    List<ExamHasExplainedQuestion> findByExam_Id(Long examHasExplainedQID);
}
