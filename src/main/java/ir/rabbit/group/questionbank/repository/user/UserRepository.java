package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    List<User> findAll();

    User findByUserName(String username);

    boolean existsByEmailEquals(String mail);

    boolean existsByUserNameEquals(String username);

}
