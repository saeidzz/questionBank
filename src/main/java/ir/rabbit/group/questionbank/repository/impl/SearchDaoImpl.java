package ir.rabbit.group.questionbank.repository.impl;

import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.exam.ExamFile;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.user.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


@Repository
public class SearchDaoImpl implements ir.rabbit.group.questionbank.repository.abstraction.SearchDao {

    @Autowired
    private EntityManager entityManager;


    @Override
    public List<TestQuestion> searchTestQuestion(String phrase, CourseInfo courseInfo) {
        SessionFactory sessionFactory = entityManager.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(TestQuestion.class);
        Criteria criteria1 = session.createCriteria(TestQuestion.class);

        LogicalExpression andCondition =
                Restrictions.and(
                        Restrictions.and(
                                Restrictions.eq("isPublic", true),
                                Restrictions.or(
                                        Restrictions.like("content", "%" + phrase + "%"),
                                        Restrictions.like("optionA", "%" + phrase + "%"),
                                        Restrictions.like("optionB", "%" + phrase + "%"),
                                        Restrictions.like("optionC", "%" + phrase + "%"),
                                        Restrictions.like("optionD", "%" + phrase + "%"),
                                        Restrictions.like("subjectOrCourseTitle", "%" + phrase + "%"),
                                        Restrictions.like("seasons", "%" + phrase + "%"))),
                        //   Restrictions.like("score", "%" + phrase + "%"))),
                        Restrictions.eq("courseInfo", courseInfo));

        criteria.add(andCondition);

        criteria1.add(andCondition);
        criteria1.setProjection(Projections.rowCount());
        int total = ((Long) criteria1.uniqueResult()).intValue();


        criteria.setFirstResult(total <= 80 ? 0 : total - 80);
        criteria.setMaxResults(80);
        List<TestQuestion> questionList = criteria.list();
        session.flush();


        return questionList;

    }

    @Override
    public List<ExplainedQuestion> searchExplainedQuestion(String phrase, CourseInfo courseInfo) {
        SessionFactory sessionFactory = entityManager.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(ExplainedQuestion.class);
        Criteria criteria1 = session.createCriteria(ExplainedQuestion.class);

        LogicalExpression andCondition =
                Restrictions.and(
                        Restrictions.and(
                                Restrictions.eq("isPublic", true),
                                Restrictions.or(
                                        Restrictions.like("content", "%" + phrase + "%"),
                                        Restrictions.like("answer", "%" + phrase + "%"),
                                        Restrictions.like("subjectOrCourseTitle", "%" + phrase + "%"),
                                        Restrictions.like("seasons", "%" + phrase + "%"))),
                        //    Restrictions.like("score", "%" + phrase + "%"))),
                        Restrictions.eq("courseInfo", courseInfo));

        criteria.add(andCondition);

        criteria1.add(andCondition);
        criteria1.setProjection(Projections.rowCount());
        int total = ((Long) criteria1.uniqueResult()).intValue();


        criteria.setFirstResult(total <= 80 ? 0 : total - 80);
        criteria.setMaxResults(80);

        List<ExplainedQuestion> questionList = criteria.list();
        session.flush();


        return questionList;


    }

    @Override
    public List<TrueFalseQuestion> searchTrueFalseQuestion(String phrase, CourseInfo courseInfo) {
        SessionFactory sessionFactory = entityManager.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(TrueFalseQuestion.class);
        Criteria criteria1 = session.createCriteria(TrueFalseQuestion.class);

        LogicalExpression andCondition = Restrictions.and(Restrictions.eq("isPublic", true),
                Restrictions.eq("courseInfo", courseInfo));

        Criterion trueFalseQuestion = Restrictions.like("content", "%" + phrase + "%");
        Criterion subjectOrCourseTitle = Restrictions.like("subjectOrCourseTitle", "%" + phrase + "%");

        LogicalExpression or1 = Restrictions.or(trueFalseQuestion, subjectOrCourseTitle);

        LogicalExpression andCond = Restrictions.and(or1, andCondition);

        criteria.add(or1);

        criteria1.add(andCond);

        criteria1.setProjection(Projections.rowCount());
        int total = ((Long) criteria1.uniqueResult()).intValue();


        criteria.setFirstResult(total <= 80 ? 0 : total - 80);
        criteria.setMaxResults(80);
        List<TrueFalseQuestion> questionList = criteria.list();
        session.flush();


        return questionList;
    }

    @Override
    public List<MatchingQuestion> searchMatchingQuestion(String phrase, CourseInfo courseInfo) {
        SessionFactory sessionFactory = entityManager.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from MatchingQuestion where courseInfo_id=? and isPublic=?");
        query.setString(0, String.valueOf(courseInfo.getId()));
        query.setBoolean(1, true);
        List<MatchingQuestion> questionList = query.list();
        List<MatchingQuestion> res = new ArrayList<>();

        questionList.stream().parallel().forEach(e -> {
            System.out.println(e.toString());
            if (e.toString().contains(phrase)) {
                res.add(e);
            }
            session.evict(e);
        });

        session.flush();


        return res;

    }

    @Override
    public List<ExamFile> searchExamFiles(String phrase, CourseInfo courseInfo) {
        SessionFactory sessionFactory = entityManager.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(ExamFile.class);
        Criteria criteria1 = session.createCriteria(ExamFile.class);

        LogicalExpression andCondition =
                Restrictions.and(
                        Restrictions.and(
                                Restrictions.eq("isPublic", true),
                                Restrictions.or(
                                        Restrictions.like("title", "%" + phrase + "%"),
                                        Restrictions.like("date", "%" + phrase + "%"),
                                        Restrictions.like("subjectOrCourseTitle", "%" + phrase + "%"),
                                        Restrictions.like("seasons", "%" + phrase + "%"))),
                        Restrictions.eq("courseInfo", courseInfo));

        criteria.add(andCondition);

        criteria1.add(andCondition);
        criteria1.setProjection(Projections.rowCount());
        int total = ((Long) criteria1.uniqueResult()).intValue();


        criteria.setFirstResult(total <= 80 ? 0 : total - 80);
        criteria.setMaxResults(80);

        List<ExamFile> questionList = criteria.list();
        session.flush();


        return questionList;


    }

    @Override
    public List<Exam> searchExams(String phrase, CourseInfo courseInfo) {
        SessionFactory sessionFactory = entityManager.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Exam.class);
        Criteria criteria1 = session.createCriteria(Exam.class);

        LogicalExpression andCondition =
                Restrictions.and(
                        Restrictions.and(
                                Restrictions.eq("isPublic", true),
                                Restrictions.or(
                                        Restrictions.like("title", "%" + phrase + "%"),
                                        Restrictions.like("seasons", "%" + phrase + "%"),
                                        Restrictions.like("date", "%" + phrase + "%"),
                                        Restrictions.like("explanation", "%" + phrase + "%"),
                                        //   Restrictions.like("examLong", "%" + phrase + "%"),
                                        Restrictions.like("subjectOrCourseTitle", "%" + phrase + "%"))),
                        //  Restrictions.like("examScore", "%" + phrase + "%"))),
                        Restrictions.eq("courseInfo", courseInfo));

        criteria.add(andCondition);

        criteria1.add(andCondition);
        criteria1.setProjection(Projections.rowCount());
        int total = ((Long) criteria1.uniqueResult()).intValue();


        criteria.setFirstResult(total <= 80 ? 0 : total - 80);
        criteria.setMaxResults(80);

        List<Exam> questionList = criteria.list();
        session.flush();


        return questionList;
    }

    @Override
    public List<User> searchUsers(String phrase) {
        SessionFactory sessionFactory = entityManager.unwrap(SessionFactory.class);
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(User.class);
        Criteria criteria1 = session.createCriteria(User.class);


        Criterion userName = Restrictions.like("userName", "%" + phrase + "%");
        Criterion name = Restrictions.like("name", "%" + phrase + "%");
        Criterion email = Restrictions.like("email", "%" + phrase + "%");
        Criterion bio = Restrictions.like("bio", "%" + phrase + "%");

        LogicalExpression or1 = Restrictions.or(bio, email);
        LogicalExpression or2 = Restrictions.or(name, userName);
        LogicalExpression totalOr = Restrictions.or(or1, or2);

        criteria.add(totalOr);

        criteria1.add(totalOr);
        criteria1.setProjection(Projections.rowCount());
        int total = ((Long) criteria1.uniqueResult()).intValue();


        criteria.setFirstResult(total <= 80 ? 0 : total - 80);
        criteria.setMaxResults(80);

        List<User> questionList = criteria.list();
        session.flush();


        return questionList;
    }
}
