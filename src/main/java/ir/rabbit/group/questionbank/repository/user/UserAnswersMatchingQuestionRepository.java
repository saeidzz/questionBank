package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersMatchingQuestions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface UserAnswersMatchingQuestionRepository extends JpaRepository<UserAnswersMatchingQuestions,Long> {


    List<UserAnswersMatchingQuestions> findByUser_IdAndExam_Id(Long userId, Long examId);

}
