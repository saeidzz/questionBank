package ir.rabbit.group.questionbank.repository.reported;

import ir.rabbit.group.questionbank.model.compositeId.UserExplainedQuestionId;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedExplainedQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface ReportedExplainedQuestionRepository extends JpaRepository<ReportedExplainedQuestion, UserExplainedQuestionId> {

}
