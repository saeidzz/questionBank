package ir.rabbit.group.questionbank.repository.question;

import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TrueFalseQuestionRepository extends JpaRepository<TrueFalseQuestion, Long> {
    List<TrueFalseQuestion> findAll();

    List<TrueFalseQuestion> findByUser_Id(Long userId);

    List<TrueFalseQuestion> findByCourseInfo_Id(Long courseID);

    List<TrueFalseQuestion> findByIsPublicTrue();


    List<TrueFalseQuestion> findByIsPublicTrueAndUser_Id(Long userId);
}
