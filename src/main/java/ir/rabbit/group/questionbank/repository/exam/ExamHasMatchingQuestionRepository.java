package ir.rabbit.group.questionbank.repository.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasMatchingQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExamHasMatchingQuestionRepository extends JpaRepository<ExamHasMatchingQuestion,Long> {
    List<ExamHasMatchingQuestion> findByExam_Id(Long examID);
}
