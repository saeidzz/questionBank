package ir.rabbit.group.questionbank.repository.courseinfo;

import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface CourseInfoRepository extends JpaRepository<CourseInfo,Long> {


    List<CourseInfo> findAll();

    CourseInfo findByTitleEquals(String title);
}
