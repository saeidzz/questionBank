package ir.rabbit.group.questionbank.repository.impl;

import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TrueFalseQuestionNativeDaoImpl implements ir.rabbit.group.questionbank.repository.abstraction.TrueFalseQuestionNativeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<TrueFalseQuestion> getExamTQuestions(Long examID) {
        Query query = entityManager.createQuery("from TrueFalseQuestion tfq where tfq.id in (select ehtfq.trueFalseQuestion.id from ExamHasTrueFalseQuestion ehtfq where ehtfq.exam.id =?)");
        query.setParameter(0, examID);
        return query.getResultList();
    }

    @Override
    public List<TrueFalseQuestion> getFavoriteTFQuestionsOfSpecificUser(Long id) {
        Query query = entityManager.createQuery("from TrueFalseQuestion tfq where tfq.id in (select questionId from FavoriteTrueFalseQuestion ftfq where ftfq.userId = ?)");
        query.setParameter(0, id);
        return query.getResultList();
    }

    @Override
    public List<TrueFalseQuestion> getAllUserReportedTrueFalseQuestions() {
        Query query = entityManager.createQuery("from TrueFalseQuestion tfq where tfq.id in(select questionId from ReportedTrueFalseQuestion)");
        return query.getResultList();
    }

    @Override
    public List<TrueFalseQuestion> getTFQuestionForExam(Long userId, Long courseId) {

        Query query = entityManager.createQuery("from TrueFalseQuestion tfq where ( tfq.user.id = ? or tfq.id in (select from FavoriteTrueFalseQuestion ftfq where ftfq.userId = ?)) and tfq.courseInfo.id = ?");
        query.setParameter(0, userId);
        query.setParameter(1, userId);
        query.setParameter(2, courseId);

        return query.getResultList();
    }

}
