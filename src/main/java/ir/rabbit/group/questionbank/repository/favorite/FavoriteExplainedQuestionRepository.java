package ir.rabbit.group.questionbank.repository.favorite;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteExplainedQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface FavoriteExplainedQuestionRepository extends JpaRepository<FavoriteExplainedQuestion, Long> {

    Optional<FavoriteExplainedQuestion> findByUserExplainedQuestionId_UserIdAndUserExplainedQuestionId_ExplainedQuestionId(Long userId, Long EQuestionId);

    List<FavoriteExplainedQuestion> findByUserExplainedQuestionId_UserId(Long userId);

    @Query("select f.userExplainedQuestionId.explainedQuestionId from FavoriteExplainedQuestion f where f.userExplainedQuestionId.userId=:userId")
    List<String> getFavoriteEQuestionIdsOfSpecificUser(@Param("userId") Long userId);

}
