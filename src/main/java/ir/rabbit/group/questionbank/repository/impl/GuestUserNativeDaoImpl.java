package ir.rabbit.group.questionbank.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class GuestUserNativeDaoImpl implements ir.rabbit.group.questionbank.repository.abstraction.GuestUserNativeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<String> getAllGuestUsersMails(Long examId) {
        Query query = entityManager.createQuery("select email from GuestUser where exam.id =?");
        query.setParameter(0, examId);
        return query.getResultList();
    }

}
