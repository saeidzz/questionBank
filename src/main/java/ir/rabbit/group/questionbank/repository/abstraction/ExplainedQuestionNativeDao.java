package ir.rabbit.group.questionbank.repository.abstraction;

import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;

import java.util.List;

public interface ExplainedQuestionNativeDao {
    List<ExplainedQuestion> getExamExplainedQuestions(Long examId);

    List<ExplainedQuestion> getAllUserReportedExplainedQuestions();

    List<ExplainedQuestion> getExplainedQuestionForExam(Long userId, Long courseId);

    List<ExplainedQuestion> getFavoriteEQuestionsOfSpecificUser(Long id);
}
