package ir.rabbit.group.questionbank.repository.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasTrueFalseQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExamHasTrueFalseQuestionRepository extends JpaRepository<ExamHasTrueFalseQuestion,Long>{
    List<ExamHasTrueFalseQuestion> findByExam_Id(Long examID);
}
