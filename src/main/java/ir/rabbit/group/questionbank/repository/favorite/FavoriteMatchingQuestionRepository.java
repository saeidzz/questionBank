package ir.rabbit.group.questionbank.repository.favorite;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteMatchingQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface FavoriteMatchingQuestionRepository extends JpaRepository<FavoriteMatchingQuestion,Long> {

    Optional<FavoriteMatchingQuestion> findByUserMatchingQuestionId_UserIdAndUserMatchingQuestionId_MatchingQuestionId(Long userId, Long MQuestionId);

    List<FavoriteMatchingQuestion> findByUserMatchingQuestionId_UserId(Long userId);
    @Query("select f.userMatchingQuestionId.matchingQuestionId from FavoriteMatchingQuestion f where f.userMatchingQuestionId.userId=:userId")
    List<String> getFavoriteMQuestionIdsOfSpecificUser(@Param("userId") Long userId);

}
