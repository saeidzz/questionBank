package ir.rabbit.group.questionbank.repository.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasTestQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExamHasTestQuestionRepository extends JpaRepository<ExamHasTestQuestion,Long> {
    List<ExamHasTestQuestion> findByExam_Id(Long examID);
}
