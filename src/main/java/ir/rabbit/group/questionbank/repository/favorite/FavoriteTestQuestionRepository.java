package ir.rabbit.group.questionbank.repository.favorite;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteTestQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface FavoriteTestQuestionRepository extends JpaRepository<FavoriteTestQuestion,Long> {

    Optional<FavoriteTestQuestion> findByUserTestQuestionId_UserIdAndUserTestQuestionId_TestQuestionId(Long userId, Long tQuestionId);

    List<FavoriteTestQuestion> findByUserTestQuestionId_UserId(Long userId);

    @Query("select f.userTestQuestionId.testQuestionId from FavoriteTestQuestion f where f.userTestQuestionId.userId=:userId")
    List<String> getFavoriteTQIdOfSpecificUser(@Param("userId") Long userId);

}
