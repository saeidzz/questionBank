package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersExplainedQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface UserAnswersExplainedQuestionRepository extends JpaRepository<UserAnswersExplainedQuestion,Long> {
    List<UserAnswersExplainedQuestion> findByUser_IdAndExam_Id(Long userId, Long examId);

}
