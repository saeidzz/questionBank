package ir.rabbit.group.questionbank.repository.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExamHasUserRepository extends JpaRepository<ExamHasUser,Long> {
    List<ExamHasUser> findByExam_Id(Long examID);

    List<ExamHasUser> findByUser_Id(Long userID);

    List<ExamHasUser> findByScoreGreaterThan(double score);

    List<ExamHasUser> findByDoneTrue();

    ExamHasUser findByUser_IdAndExam_Id(Long userId, Long examId);

}
