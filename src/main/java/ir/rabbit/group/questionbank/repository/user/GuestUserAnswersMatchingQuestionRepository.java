package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersMatchingQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface GuestUserAnswersMatchingQuestionRepository extends JpaRepository<GuestUserAnswersMatchingQuestion,Long> {

    List<GuestUserAnswersMatchingQuestion> findAll();

    List<GuestUserAnswersMatchingQuestion> findByGuestUser_Id(Long userId);

}
