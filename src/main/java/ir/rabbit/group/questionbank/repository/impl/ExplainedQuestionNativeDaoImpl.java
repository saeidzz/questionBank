package ir.rabbit.group.questionbank.repository.impl;

import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ExplainedQuestionNativeDaoImpl implements ir.rabbit.group.questionbank.repository.abstraction.ExplainedQuestionNativeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<ExplainedQuestion> getExamExplainedQuestions(Long examId) {
        Query query = entityManager.createQuery("from ExplainedQuestion q where q.id in(select eheq.explainedQuestion.id from ExamHasExplainedQuestion eheq where eheq.exam.id=?)");
        query.setParameter(0, examId);
        return query.getResultList();
    }

    @Override
    public List<ExplainedQuestion> getAllUserReportedExplainedQuestions() {
        Query query = entityManager.createQuery("from ExplainedQuestion eq WHERE eq.id in(SELECT req.id from ReportedExplainedQuestion req)");
        return query.getResultList();
    }

    @Override
    public List<ExplainedQuestion> getExplainedQuestionForExam(Long userId, Long courseId) {
        Query query = entityManager.createQuery("from ExplainedQuestion q  where (q.user.id=? or q.id in (select feq.questionId from FavoriteExplainedQuestion feq where feq.userId=?)) and q.courseInfo.id=? ");

        query.setParameter(0, userId);
        query.setParameter(1, userId);
        query.setParameter(2, courseId);
        return query.getResultList();
    }

    @Override
    public List<ExplainedQuestion> getFavoriteEQuestionsOfSpecificUser(Long id) {
        Query query = entityManager.createQuery("from ExplainedQuestion q  where q.id in (select feq.questionId from FavoriteExplainedQuestion feq where feq.userId=?)");
        query.setParameter(0, id);
        return query.getResultList();
    }
}
