package ir.rabbit.group.questionbank.repository.impl;

import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.repository.abstraction.ExamNativeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ExamNativeDaoImpl implements ExamNativeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Exam> getFavoriteExamsOfSpecificUser(Long userId) {
        Query query = entityManager.createQuery("from Exam e where e.id in(select examId FROM FavoriteExam where userId=?)");
        query.setParameter(0, userId);
        return query.getResultList();
    }

    @Override
    public List<Exam> getAllUserReportedExams() {
        Query query = entityManager.createQuery("from Exam e WHERE e.id in(SELECT re.examId from ReportedExam re )");
        return query.getResultList();
    }


}
