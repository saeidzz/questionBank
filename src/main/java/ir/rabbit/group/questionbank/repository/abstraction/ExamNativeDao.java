package ir.rabbit.group.questionbank.repository.abstraction;

import ir.rabbit.group.questionbank.model.exam.Exam;

import java.util.List;

public interface ExamNativeDao {
    List<Exam> getFavoriteExamsOfSpecificUser(Long userId);

    List<Exam> getAllUserReportedExams();
}
