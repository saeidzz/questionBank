package ir.rabbit.group.questionbank.repository.question;

import ir.rabbit.group.questionbank.model.question.MatchingNode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface MatchingNodeRepository extends JpaRepository<MatchingNode,Long> {


    List<MatchingNode> findAll();

    List<MatchingNode> findByMatchingQuestion_Id(Long matchingQID);


}
