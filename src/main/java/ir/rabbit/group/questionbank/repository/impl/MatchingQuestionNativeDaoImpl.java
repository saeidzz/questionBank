package ir.rabbit.group.questionbank.repository.impl;

import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class MatchingQuestionNativeDaoImpl implements ir.rabbit.group.questionbank.repository.abstraction.MatchingQuestionNativeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<MatchingQuestion> getExamMatchingQuestions(Long examID) {
        Query query = entityManager.createQuery("from MatchingQuestion m where m.id in (select ehmq.matchingQuestion.id from ExamHasMatchingQuestion ehmq where ehmq.exam.id=?)");
        query.setParameter(0, examID);
        return query.getResultList();
    }

    @Override
    public List<MatchingQuestion> getFavoriteMQuestionsOfSpecificUser(Long userId) {
        Query query = entityManager.createQuery("from MatchingQuestion m where m.id in(select questionId FROM FavoriteMatchingQuestion where userId=?)");
        query.setParameter(0, userId);

        return query.getResultList();
    }

    @Override
    public List<MatchingQuestion> getAllUserReportedMatchingQuestions() {
        Query query = entityManager.createQuery("from MatchingQuestion mq WHERE mq.id in(SELECT rmq.questionId from ReportedMatchingQuestion rmq)");
        return query.getResultList();
    }

    @Override
    public List<MatchingQuestion> getMQuestionForExam(Long userId, Long courseId) {
        Query query = entityManager.createQuery("from MatchingQuestion q  where (q.user.id=? or q.id in(select fmq.questionId from FavoriteMatchingQuestion fmq where fmq.userId=?)) and q.courseInfo.id=? ");
        query.setParameter(0, userId);
        query.setParameter(1, userId);
        query.setParameter(2, courseId);
        return query.getResultList();
    }


}
