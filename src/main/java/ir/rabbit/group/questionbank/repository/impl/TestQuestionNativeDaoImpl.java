package ir.rabbit.group.questionbank.repository.impl;

import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TestQuestionNativeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TestQuestionNativeDaoImpl implements TestQuestionNativeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<TestQuestion> getExamTestQuestions(Long examId) {
        Query query = entityManager.createQuery("from TestQuestion q where q.id in (select ehtq.testQuestion.id from ExamHasTestQuestion ehtq where ehtq.exam.id=?)");
        query.setParameter(0, examId);
        return query.getResultList();

    }

    @Override
    public List<TestQuestion> getFavoriteTQuestionsOfSpecificUser(Long userId) {
        Query query = entityManager.createQuery("from TestQuestion q where q.id in(select questionId FROM FavoriteTestQuestion where userId=?)");
        query.setParameter(0, userId);
        return query.getResultList();
    }

    @Override
    public List<TestQuestion> getAllUserReportedTestQuestion() {
        Query query = entityManager.createQuery("from TestQuestion q where q.id in(select rtq.questionId from ReportedTestQuestion rtq )");
        return query.getResultList();
    }


    @Override
    public List<TestQuestion> getTestQuestionForExam(Long userId, Long courseId) {
        Query query = entityManager.createQuery("from TestQuestion q where (q.user.id=? or q.id in (select ftq.questionId from FavoriteTestQuestion ftq where ftq.userId=? )) and q.courseInfo.id = ?");
        query.setParameter(0, userId);
        query.setParameter(1, userId);
        query.setParameter(2, courseId);
        return query.getResultList();
    }

}
