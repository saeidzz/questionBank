package ir.rabbit.group.questionbank.repository.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersExplainedQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface GuestUserAnswersExplainedQuestionRepository extends JpaRepository<GuestUserAnswersExplainedQuestion,Long> {

    List<GuestUserAnswersExplainedQuestion> findAll();

    List<GuestUserAnswersExplainedQuestion> findByGuestUser_Id(Long userId);

}
