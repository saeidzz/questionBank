package ir.rabbit.group.questionbank.repository.reported;

import ir.rabbit.group.questionbank.model.compositeId.UserMatchingQuestionId;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedMatchingQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;import org.springframework.stereotype.Repository;


@Repository
public interface ReportedMatchingQuestionRepository extends JpaRepository<ReportedMatchingQuestion, UserMatchingQuestionId> {


}
