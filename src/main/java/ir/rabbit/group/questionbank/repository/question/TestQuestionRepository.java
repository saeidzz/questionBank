package ir.rabbit.group.questionbank.repository.question;

import ir.rabbit.group.questionbank.model.question.TestQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by saeid on 8/10/17.
 */import org.springframework.stereotype.Repository;


@Repository
public interface TestQuestionRepository extends JpaRepository<TestQuestion,Long> {

    List<TestQuestion> findAll();


    List<TestQuestion> findByUser_Id(Long userId);

    List<TestQuestion> findByCourseInfo_Id(Long courseID);

    List<TestQuestion> findByIsPublicTrue();

    List<TestQuestion> findByIsPublicTrueAndUser_Id(Long userId);

}
