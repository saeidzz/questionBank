package ir.rabbit.group.questionbank.repository.abstraction;

import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.exam.ExamFile;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.user.User;

import java.util.List;

public interface SearchDao {
    List<TestQuestion> searchTestQuestion(String phrase, CourseInfo courseInfo);

    List<ExplainedQuestion> searchExplainedQuestion(String phrase, CourseInfo courseInfo);

    List<TrueFalseQuestion> searchTrueFalseQuestion(String phrase, CourseInfo courseInfo);

    List<MatchingQuestion> searchMatchingQuestion(String phrase, CourseInfo courseInfo);

    List<ExamFile> searchExamFiles(String phrase, CourseInfo courseInfo);

    List<Exam> searchExams(String phrase, CourseInfo courseInfo);

    List<User> searchUsers(String phrase);
}
