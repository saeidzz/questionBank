package ir.rabbit.group.questionbank.model.question;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.user.User;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table
@SequenceGenerator(name = "EQ_SEQ", sequenceName = "EQ", allocationSize = 10, initialValue = 1)
public class ExplainedQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EQ_SEQ")
    private Long id;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    @NotEmpty(message = "محتوی سوال نباید خالی باشد !")
    private String content;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    @NotEmpty(message = "محتوی پاسخ نباید خالی باشد !")
    private String answer;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String seasons;
    private int level;
    private boolean isPublic;
    private boolean checkedAndHasNoProblem;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String subjectOrCourseTitle;

    private float score;


    @ManyToOne
    private User user;

    @ManyToOne
    private CourseInfo courseInfo;


    @Transient
    private MultipartFile questionImage;

    @Transient
    private MultipartFile asnswerImage;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSeasons() {
        return seasons;
    }

    public void setSeasons(String seasons) {
        this.seasons = seasons;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public CourseInfo getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(CourseInfo courseInfo) {
        this.courseInfo = courseInfo;
    }


    public MultipartFile getQuestionImage() {
        return questionImage;
    }

    public void setQuestionImage(MultipartFile questionImage) {
        this.questionImage = questionImage;
    }

    public MultipartFile getAsnswerImage() {
        return asnswerImage;
    }

    public void setAsnswerImage(MultipartFile asnswerImage) {
        this.asnswerImage = asnswerImage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isCheckedAndHasNoProblem() {
        return checkedAndHasNoProblem;
    }

    public void setCheckedAndHasNoProblem(boolean checkedAndHasNoProblem) {
        this.checkedAndHasNoProblem = checkedAndHasNoProblem;
    }

    public boolean getPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getSubjectOrCourseTitle() {
        return subjectOrCourseTitle;
    }

    public void setSubjectOrCourseTitle(String subjectOrCourseTitle) {
        this.subjectOrCourseTitle = subjectOrCourseTitle;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(content).append('\'');
        sb.append(answer).append('\'');
        sb.append(seasons).append('\'');
        sb.append(level == 1 ? "ساده" : level == 2 ? "متوسط" : "سخت");
        sb.append(subjectOrCourseTitle).append('\'');
        sb.append(score);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExplainedQuestion)) return false;

        ExplainedQuestion that = (ExplainedQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}


