package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "GUAMQ_SEQ", sequenceName = "GUAMQ_SEQ", allocationSize = 10, initialValue = 1)
public class GuestUserAnswersMatchingQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GUAMQ_SEQ")
    private Long id;


    private String userAnswer;

    @ManyToOne
    private MatchingQuestion matchingQuestion;

    @ManyToOne
    private GuestUser guestUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public MatchingQuestion getMatchingQuestion() {
        return matchingQuestion;
    }

    public void setMatchingQuestion(MatchingQuestion matchingQuestion) {
        this.matchingQuestion = matchingQuestion;
    }

    public GuestUser getGuestUser() {
        return guestUser;
    }

    public void setGuestUser(GuestUser guestUser) {
        this.guestUser = guestUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuestUserAnswersMatchingQuestion)) return false;

        GuestUserAnswersMatchingQuestion that = (GuestUserAnswersMatchingQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
