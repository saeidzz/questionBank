package ir.rabbit.group.questionbank.model.exam;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.user.User;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table
@SequenceGenerator(name = "ExamFile_SEQ", sequenceName = "ExamFile_SEQ", allocationSize = 10, initialValue = 1)

public class ExamFile extends AbstractAuditingEntity {


    public boolean isPublic;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ExamFile_SEQ")
    private Long id;
    @NotEmpty(message = "عنوان فایل باید وارد شود.")
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String title;
    private int level;
    @NotEmpty(message = "تاریخ آزمون باید مشخص شود.")
    private String date;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String seasons;
    private String questionFileExt;
    private String answerFileExt;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String subjectOrCourseTitle;
    @ManyToOne

    @JsonIgnore
    private User user;


    @ManyToOne
    @JsonIgnore
    private CourseInfo courseInfo;


    @Transient
    private MultipartFile questionsFile;
    @Transient
    private MultipartFile answersFile;

    private boolean checkedAndHasNoProblem;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public CourseInfo getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(CourseInfo courseInfo) {
        this.courseInfo = courseInfo;
    }


    public MultipartFile getQuestionsFile() {
        return questionsFile;
    }

    public void setQuestionsFile(MultipartFile questionsFile) {
        this.questionsFile = questionsFile;
    }

    public MultipartFile getAnswersFile() {
        return answersFile;
    }

    public void setAnswersFile(MultipartFile answersFile) {
        this.answersFile = answersFile;
    }

    public String getSeasons() {
        return seasons;
    }

    public void setSeasons(String seasons) {
        this.seasons = seasons;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isCheckedAndHasNoProblem() {
        return checkedAndHasNoProblem;
    }

    public void setCheckedAndHasNoProblem(boolean checkedAndHasNoProblem) {
        this.checkedAndHasNoProblem = checkedAndHasNoProblem;
    }

    public String getQuestionFileExt() {
        return questionFileExt;
    }

    public void setQuestionFileExt(String questionFileExt) {
        this.questionFileExt = questionFileExt;
    }

    public String getAnswerFileExt() {
        return answerFileExt;
    }

    public void setAnswerFileExt(String answerFileExt) {
        this.answerFileExt = answerFileExt;
    }

    public String getSubjectOrCourseTitle() {
        return subjectOrCourseTitle;
    }

    public void setSubjectOrCourseTitle(String subjectOrCourseTitle) {
        this.subjectOrCourseTitle = subjectOrCourseTitle;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(title).append('\'');
        sb.append(level);
        sb.append(date).append('\'');
        sb.append(seasons).append('\'');
        sb.append(subjectOrCourseTitle).append('\'');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamFile)) return false;

        ExamFile examFile = (ExamFile) o;

        return getId().equals(examFile.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}

