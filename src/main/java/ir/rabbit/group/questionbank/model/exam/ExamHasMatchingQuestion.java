package ir.rabbit.group.questionbank.model.exam;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "EHMQ_SEQ", sequenceName = "EHMQ_SEQ", allocationSize = 10, initialValue = 1)
public class ExamHasMatchingQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EHMQ_SEQ")
    private Long id;


    @ManyToOne
    private MatchingQuestion matchingQuestion;


    @ManyToOne
    private Exam exam;

    private boolean done;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MatchingQuestion getMatchingQuestion() {
        return matchingQuestion;
    }

    public void setMatchingQuestion(MatchingQuestion matchingQuestion) {
        this.matchingQuestion = matchingQuestion;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamHasMatchingQuestion)) return false;

        ExamHasMatchingQuestion that = (ExamHasMatchingQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
