package ir.rabbit.group.questionbank.model.reported;


import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.compositeId.UserExplainedQuestionId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table
public class ReportedExplainedQuestion extends AbstractAuditingEntity {

    @EmbeddedId
    private UserExplainedQuestionId userExplainedQuestionId;

    public UserExplainedQuestionId getUserExplainedQuestionId() {
        return userExplainedQuestionId;
    }

    public void setUserExplainedQuestionId(UserExplainedQuestionId userExplainedQuestionId) {
        this.userExplainedQuestionId = userExplainedQuestionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportedExplainedQuestion that = (ReportedExplainedQuestion) o;
        return Objects.equals(userExplainedQuestionId, that.userExplainedQuestionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userExplainedQuestionId);
    }
}
