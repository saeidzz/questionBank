package ir.rabbit.group.questionbank.model.Favorit;


import ir.rabbit.group.questionbank.model.compositeId.UserTrueFalseQuestionId;
import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class FavoriteTrueFalseQuestion extends AbstractAuditingEntity {

    @EmbeddedId
    private UserTrueFalseQuestionId userTrueFalseQuestionId;

    public UserTrueFalseQuestionId getUserTrueFalseQuestionId() {
        return userTrueFalseQuestionId;
    }

    public void setUserTrueFalseQuestionId(UserTrueFalseQuestionId userTrueFalseQuestionId) {
        this.userTrueFalseQuestionId = userTrueFalseQuestionId;
    }
}
