package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
/*import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;*/
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;


@Entity
@SequenceGenerator(name = "USER_SEQ",
        sequenceName = "USER_SEQ", allocationSize = 10, initialValue = 1)
@Table(name = "USER", uniqueConstraints =
@UniqueConstraint(columnNames = {"USERNAME", "EMAIL"}))
public class User extends AbstractAuditingEntity {//implements UserDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ")
    private Long id;
    @NotEmpty(message = "نام کاربری نباید خالی باشد")
    @Column(unique = true)
    @NotNull
    @Size(max = 20, min = 6, message = "نام کاربری باید بین 6 تا 20 حرف باشد.")
    private String userName;
    @NotEmpty(message = "رمز عبور نمیتواند خالی باشد")
    @Size(min = 8, message = "رمز عبور باید حداقل 8 کاراکتر باشد .")
    private String password;
    private boolean enabled;
    @NotEmpty(message = "نام نباید خالی باشد")
    private String name;
    @NotEmpty(message = "لطفا یک پست الکترونیکی معتبر وارد نمایید")
    @Email(message = "لطفا یک آدرس ایمیل معتبر وارد نمایید .")
    @Column(unique = true)
    @NotNull
    private String email;
    private int credit;
    private long uploadedSize;

    @Size(min = 0, max = 255, message = "حداقل 20 حرف درمورد خودتان بنویسید ")
    private String bio;

    @ManyToOne
    private CourseInfo courseInfo;


    @Transient
    private MultipartFile userPic;

    public User(@NotEmpty(message = "نام کاربری نباید خالی باشد") @NotNull @Size(max = 20, min = 6, message = "نام کاربری باید بین 6 تا 20 حرف باشد.") String userName, @NotEmpty(message = "رمز عبور نمیتواند خالی باشد") @Size(min = 8, message = "رمز عبور باید حداقل 8 کاراکتر باشد .") String password, boolean enabled, @NotEmpty(message = "نام نباید خالی باشد") String name, @NotEmpty(message = "لطفا یک پست الکترونیکی معتبر وارد نمایید") @Email(message = "لطفا یک آدرس ایمیل معتبر وارد نمایید .") @NotNull String email, int credit, long uploadedSize, @Size(min = 20, max = 255, message = "حداقل 20 حرف درمورد خودتان بنویسید ") String bio, CourseInfo courseInfo) {
        this.userName = userName;
        this.password = password;
        this.enabled = enabled;
        this.name = name;
        this.email = email;
        this.credit = credit;
        this.uploadedSize = uploadedSize;
        this.bio = bio;
        this.courseInfo = courseInfo;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

 /*   @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }
*/
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   /* @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
*/
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MultipartFile getUserPic() {
        return userPic;
    }

    public void setUserPic(MultipartFile userPic) {
        this.userPic = userPic;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public CourseInfo getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(CourseInfo courseInfo) {
        this.courseInfo = courseInfo;
    }

    public long getUploadedSize() {
        return uploadedSize;
    }

    public void setUploadedSize(long uploadedSize) {
        this.uploadedSize = uploadedSize;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(userName).append('\'');
        sb.append(name).append('\'');
        sb.append(email).append('\'');
        sb.append(bio).append('\'');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User users = (User) o;

        return getId().equals(users.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}

