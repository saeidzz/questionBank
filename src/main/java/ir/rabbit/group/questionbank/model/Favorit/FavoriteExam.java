package ir.rabbit.group.questionbank.model.Favorit;


import ir.rabbit.group.questionbank.model.compositeId.UserExamId;
import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class FavoriteExam extends AbstractAuditingEntity {

    @EmbeddedId
    private UserExamId userExamId;


    public UserExamId getUserExamId() {
        return userExamId;
    }

    public void setUserExamId(UserExamId userExamId) {
        this.userExamId = userExamId;
    }


}
