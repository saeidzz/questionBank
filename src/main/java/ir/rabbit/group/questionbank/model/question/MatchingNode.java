package ir.rabbit.group.questionbank.model.question;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "MN_SEQ", sequenceName = "MN_SEQ", allocationSize = 10, initialValue = 1)
public class MatchingNode extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MN_SEQ")
    private Long id;
    private String clue;
    private String matchString;

    @ManyToOne
    private MatchingQuestion matchingQuestion;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClue() {
        return clue;
    }

    public void setClue(String clue) {
        this.clue = clue;
    }

    public String getMatchString() {
        return matchString;
    }

    public void setMatchString(String matchString) {
        this.matchString = matchString;
    }

    public MatchingQuestion getMatchingQuestion() {
        return matchingQuestion;
    }

    public void setMatchingQuestion(MatchingQuestion matchingQuestion) {
        this.matchingQuestion = matchingQuestion;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MatchingNode{");
        sb.append("id=").append(id);
        sb.append(", clue='").append(clue).append('\'');
        sb.append(", matchString='").append(matchString).append('\'');
        sb.append('}');
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatchingNode)) return false;

        MatchingNode that = (MatchingNode) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
