package ir.rabbit.group.questionbank.model.question;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.user.User;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * Created by saeid on 8/10/17.
 */
@Entity
@Table
@SequenceGenerator(name = "TQ_SEQ", sequenceName = "TQ_SEQ", allocationSize = 10, initialValue = 1)
public class TestQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TQ_SEQ")
    private Long id;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    @NotEmpty(message = "محتوی سوال نباید خالی باشد !")
    private String content;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    @NotEmpty(message = "گزینه اول نمیتواند خالی باشد!")
    private String optionA;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    @NotEmpty(message = "گزینه دوم نمیتواند خالی باشد !")
    private String optionB;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String optionC;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String optionD;
    private int level;
    @NotEmpty(message = "پاسخ صحیح را وارد کنید")
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String rightOption;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String seasons;
    private String subjectOrCourseTitle;


    private boolean isPublic;

    private float score;


    @ManyToOne
    private User user;

    @ManyToOne
    private CourseInfo courseInfo;


    @Transient
    private MultipartFile questionImage;
    @Transient
    private MultipartFile asnswerImage;

    private boolean checkedAndHasNoProblem;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getRightOption() {
        return rightOption;
    }

    public void setRightOption(String rightOption) {
        this.rightOption = rightOption;
    }

    public String getSeasons() {
        return seasons;
    }

    public void setSeasons(String seasons) {
        this.seasons = seasons;
    }


    public CourseInfo getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(CourseInfo courseInfo) {
        this.courseInfo = courseInfo;
    }

    public MultipartFile getQuestionImage() {
        return questionImage;
    }

    public void setQuestionImage(MultipartFile questionImage) {
        this.questionImage = questionImage;
    }

    public MultipartFile getAsnswerImage() {
        return asnswerImage;
    }

    public void setAsnswerImage(MultipartFile asnswerImage) {
        this.asnswerImage = asnswerImage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public boolean isCheckedAndHasNoProblem() {
        return checkedAndHasNoProblem;
    }

    public void setCheckedAndHasNoProblem(boolean checkedAndHasNoProblem) {
        this.checkedAndHasNoProblem = checkedAndHasNoProblem;
    }

    public boolean getPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }


    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getSubjectOrCourseTitle() {
        return subjectOrCourseTitle;
    }

    public void setSubjectOrCourseTitle(String subjectOrCourseTitle) {
        this.subjectOrCourseTitle = subjectOrCourseTitle;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(content).append('\'');
        sb.append(optionA).append('\'');
        sb.append(optionB).append('\'');
        sb.append(optionC).append('\'');
        sb.append(optionD).append('\'');
        sb.append(level == 1 ? "ساده" : level == 2 ? "متوسط" : "سخت");
        sb.append(rightOption).append('\'');
        sb.append(seasons).append('\'');
        sb.append(subjectOrCourseTitle).append('\'');
        sb.append(score);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestQuestion)) return false;

        TestQuestion that = (TestQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}


