package ir.rabbit.group.questionbank.model.course_and_field;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(
        name = "CourseInfo_SEQ",
        sequenceName = "CourseInfo_SEQ",
        allocationSize = 10,
        initialValue = 1
)
public class CourseInfo extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CourseInfo_SEQ")
    private Long id;
    private String title;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CourseInfo{");
        sb.append("title='").append(title).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CourseInfo)) return false;

        CourseInfo that = (CourseInfo) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}

