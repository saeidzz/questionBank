package ir.rabbit.group.questionbank.model.Favorit;

import ir.rabbit.group.questionbank.model.compositeId.UserTestQuestionId;
import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;

import javax.persistence.*;

@Entity
@Table
public class FavoriteTestQuestion extends AbstractAuditingEntity {

    @EmbeddedId
    private UserTestQuestionId userTestQuestionId;

    public UserTestQuestionId getUserTestQuestionId() {
        return userTestQuestionId;
    }

    public void setUserTestQuestionId(UserTestQuestionId userTestQuestionId) {
        this.userTestQuestionId = userTestQuestionId;
    }
}
