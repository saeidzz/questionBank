package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.exam.Exam;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "GUPIE_SEQ", sequenceName = "GUPIE_SEQ", allocationSize = 10, initialValue = 1)
public class GuestUser extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GUPIE_SEQ")
    private Long id;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String name;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String message;
    private double score;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String email;
    @Length(max = 11, message = "حداکثر حروف مجاز 11 می باشد")
    private String phoneNumber;


    @ManyToOne
    private Exam exam;

    private boolean done;
    private boolean examSeen;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Long getUserId() {
        return exam.getUser().getId();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isExamSeen() {
        return examSeen;
    }

    public void setExamSeen(boolean examSeen) {
        this.examSeen = examSeen;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuestUser)) return false;

        GuestUser that = (GuestUser) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GuestUser{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", score=").append(score);
        sb.append(", email='").append(email).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", exam=").append(exam);
        sb.append(", done=").append(done);
        sb.append(", examSeen=").append(examSeen);
        sb.append('}');
        return sb.toString();
    }
}
