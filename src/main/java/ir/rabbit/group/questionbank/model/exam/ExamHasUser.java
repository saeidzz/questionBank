package ir.rabbit.group.questionbank.model.exam;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.user.User;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "EHU_SEQ", sequenceName = "EHU_SEQ", allocationSize = 10, initialValue = 1)
public class ExamHasUser extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EHU_SEQ")
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Exam exam;

    private double score;
    private boolean done;
    private boolean examSeen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isExamSeen() {
        return examSeen;
    }

    public void setExamSeen(boolean examSeen) {
        this.examSeen = examSeen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamHasUser)) return false;

        ExamHasUser that = (ExamHasUser) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
