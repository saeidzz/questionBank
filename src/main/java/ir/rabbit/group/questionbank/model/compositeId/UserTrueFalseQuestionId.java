package ir.rabbit.group.questionbank.model.compositeId;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserTrueFalseQuestionId implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long trueFalseQuestionId;

    private Long userId;

    public static UserTrueFalseQuestionId of(Long trueFalseQuestionId, Long userId) {
        UserTrueFalseQuestionId id = new UserTrueFalseQuestionId();
        id.setUserId(userId);
        id.setTrueFalseQuestionId(trueFalseQuestionId);

        return id;
    }

    public Long getTrueFalseQuestionId() {
        return trueFalseQuestionId;
    }

    public void setTrueFalseQuestionId(Long trueFalseQuestionId) {
        this.trueFalseQuestionId = trueFalseQuestionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserExamId{" +
                "trueFalseQuestionId=" + trueFalseQuestionId +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserTrueFalseQuestionId that = (UserTrueFalseQuestionId) o;
        return Objects.equals(trueFalseQuestionId, that.trueFalseQuestionId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trueFalseQuestionId, userId);
    }
}
