package ir.rabbit.group.questionbank.model.compositeId;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserExplainedQuestionId implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long explainedQuestionId;

    private Long userId;

    public static UserExplainedQuestionId of(Long explainedQuestionId, Long userId) {
        UserExplainedQuestionId id = new UserExplainedQuestionId();
        id.setUserId(userId);
        id.setExplainedQuestionId(explainedQuestionId);

        return id;
    }

    public Long getExplainedQuestionId() {
        return explainedQuestionId;
    }

    public void setExplainedQuestionId(Long explainedQuestionId) {
        this.explainedQuestionId = explainedQuestionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserExplainedQuestionId that = (UserExplainedQuestionId) o;
        return Objects.equals(explainedQuestionId, that.explainedQuestionId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(explainedQuestionId, userId);
    }
}
