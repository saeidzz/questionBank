package ir.rabbit.group.questionbank.model.exam;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.user.User;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
@Table
@SequenceGenerator(
        name = "Exam_SEQ",
        sequenceName = "Exam_SEQ",
        allocationSize = 10,
        initialValue = 1
)
public class Exam extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Exam_SEQ")
    private Long id;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    @NotEmpty(message = "عنوان آزمون نباید خالی باشد !")
    private String title;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String seasons;
    private int level;
    @NotEmpty(message = "تاریخ آزمون را مشخص کنید .")
    private String date;
    private boolean isPublic;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String explanation;
    private long longTime;
    private Date startTime;
    private Date endTime;
    private boolean started;
    private boolean finished;
    private boolean checkedAndHasNoProblem;
    private boolean exported;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String password;
    private boolean timeLimited;
    private float score;
    @Length(max = 255, message = "حداکثر حروف مجاز ۲۵۵ می باشد")
    private String subjectOrCourseTitle;
    private String link;
    @ManyToOne
    private User user;
    @ManyToOne
    private CourseInfo courseInfo;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeasons() {
        return seasons;
    }

    public void setSeasons(String seasons) {
        this.seasons = seasons;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public CourseInfo getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(CourseInfo courseInfo) {
        this.courseInfo = courseInfo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }


    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }


    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public long getLongTime() {
        return longTime;
    }

    public void setLongTime(long longTime) {
        this.longTime = longTime;
    }


    public boolean isCheckedAndHasNoProblem() {
        return checkedAndHasNoProblem;
    }

    public void setCheckedAndHasNoProblem(boolean checkedAndHasNoProblem) {
        this.checkedAndHasNoProblem = checkedAndHasNoProblem;
    }

    public boolean isExported() {
        return exported;
    }

    public void setExported(boolean exported) {
        this.exported = exported;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isTimeLimited() {
        return timeLimited;
    }

    public void setTimeLimited(boolean timeLimited) {
        this.timeLimited = timeLimited;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getSubjectOrCourseTitle() {
        return subjectOrCourseTitle;
    }

    public void setSubjectOrCourseTitle(String subjectOrCourseTitle) {
        this.subjectOrCourseTitle = subjectOrCourseTitle;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(title).append('\'');
        sb.append(seasons).append('\'');
        sb.append(level);
        sb.append(date).append('\'');
        sb.append(explanation).append('\'');
        sb.append(longTime);
        sb.append(score);
        sb.append(subjectOrCourseTitle).append('\'');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Exam)) return false;

        Exam exam = (Exam) o;

        return getId().equals(exam.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
