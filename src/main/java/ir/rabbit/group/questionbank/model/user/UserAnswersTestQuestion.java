package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.question.TestQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "UATQ_SEQ", sequenceName = "UATQ_SEQ", allocationSize = 10, initialValue = 1)
public class UserAnswersTestQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UATQ_SEQ")
    private Long id;


    private String userAnswer;

    @ManyToOne
    private TestQuestion testQuestion;

    @ManyToOne
    private User user;

    @ManyToOne
    private Exam exam;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public TestQuestion getTestQuestion() {
        return testQuestion;
    }

    public void setTestQuestion(TestQuestion testQuestion) {
        this.testQuestion = testQuestion;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAnswersTestQuestion)) return false;

        UserAnswersTestQuestion that = (UserAnswersTestQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
