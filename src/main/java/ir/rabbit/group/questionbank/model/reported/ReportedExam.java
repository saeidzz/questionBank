package ir.rabbit.group.questionbank.model.reported;


import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.compositeId.UserExamId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table
public class ReportedExam extends AbstractAuditingEntity {


    @EmbeddedId
    private UserExamId userExamId;

    public UserExamId getUserExamId() {
        return userExamId;
    }

    public void setUserExamId(UserExamId userExamId) {
        this.userExamId = userExamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportedExam that = (ReportedExam) o;
        return Objects.equals(userExamId, that.userExamId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userExamId);
    }
}
