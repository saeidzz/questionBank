package ir.rabbit.group.questionbank.model.reported;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.compositeId.UserMatchingQuestionId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ReportedMatchingQuestion extends AbstractAuditingEntity {
    @EmbeddedId
    private UserMatchingQuestionId userMatchingQuestionId;


    public UserMatchingQuestionId getUserMatchingQuestionId() {
        return userMatchingQuestionId;
    }

    public void setUserMatchingQuestionId(UserMatchingQuestionId userMatchingQuestionId) {
        this.userMatchingQuestionId = userMatchingQuestionId;
    }


}
