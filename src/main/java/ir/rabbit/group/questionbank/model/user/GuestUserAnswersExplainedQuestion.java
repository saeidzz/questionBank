package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "GUAEQ_SEQ", sequenceName = "GUAEQ_SEQ", allocationSize = 10, initialValue = 1)
public class GuestUserAnswersExplainedQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GUAEQ_SEQ")
    private Long id;


    private String userAnswer;

    @ManyToOne
    private ExplainedQuestion explainedQuestion;

    @ManyToOne
    private GuestUser guestUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }


    public GuestUser getGuestUser() {
        return guestUser;
    }

    public void setGuestUser(GuestUser guestUser) {
        this.guestUser = guestUser;
    }

    public ExplainedQuestion getExplainedQuestion() {
        return explainedQuestion;
    }

    public void setExplainedQuestion(ExplainedQuestion explainedQuestion) {
        this.explainedQuestion = explainedQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuestUserAnswersExplainedQuestion)) return false;

        GuestUserAnswersExplainedQuestion that = (GuestUserAnswersExplainedQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
