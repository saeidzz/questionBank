package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "UATFQ_SEQ", sequenceName = "UATFQ_SEQ", allocationSize = 10, initialValue = 1)
public class UserAnswersTrueFalseQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UATFQ_SEQ")
    private Long id;


    private String userAnswer;

    @ManyToOne
    private TrueFalseQuestion trueFalseQuestion;

    @ManyToOne
    private User user;

    @ManyToOne
    private Exam exam;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public TrueFalseQuestion getTrueFalseQuestion() {
        return trueFalseQuestion;
    }

    public void setTrueFalseQuestion(TrueFalseQuestion trueFalseQuestion) {
        this.trueFalseQuestion = trueFalseQuestion;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAnswersTrueFalseQuestion)) return false;

        UserAnswersTrueFalseQuestion that = (UserAnswersTrueFalseQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
