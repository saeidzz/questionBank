package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "GUATFQ_SEQ", sequenceName = "GUATFQ_SEQ", allocationSize = 10, initialValue = 1)
public class GuestUserAnswersTrueFalseQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GUATFQ_SEQ")
    private Long id;


    private String userAnswer;

    @ManyToOne
    private TrueFalseQuestion trueFalseQuestion;

    @ManyToOne
    private GuestUser guestUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public TrueFalseQuestion getTrueFalseQuestion() {
        return trueFalseQuestion;
    }

    public void setTrueFalseQuestion(TrueFalseQuestion trueFalseQuestion) {
        this.trueFalseQuestion = trueFalseQuestion;
    }

    public GuestUser getGuestUser() {
        return guestUser;
    }

    public void setGuestUser(GuestUser guestUser) {
        this.guestUser = guestUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuestUserAnswersTrueFalseQuestion)) return false;

        GuestUserAnswersTrueFalseQuestion that = (GuestUserAnswersTrueFalseQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
