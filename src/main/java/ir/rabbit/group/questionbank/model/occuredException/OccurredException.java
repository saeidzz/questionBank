package ir.rabbit.group.questionbank.model.occuredException;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@SequenceGenerator(name = "OE_SEQ", sequenceName = "OE_SEQ", allocationSize = 10, initialValue = 1)

public class OccurredException extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OE_SEQ")
    private long id;
    private Date date;
    @Column(columnDefinition = "TEXT")
    private String exception;
    private String url;
    private String remoteUser;
    private String ip;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRemoteUser() {
        return remoteUser;
    }

    public void setRemoteUser(String remoteUser) {
        this.remoteUser = remoteUser;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OccurredException)) return false;

        OccurredException that = (OccurredException) o;

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }
}
