package ir.rabbit.group.questionbank.model.compositeId;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserTestQuestionId implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long testQuestionId;

    private Long userId;

    public static UserTestQuestionId of(Long testQuestionId, Long userId) {
        UserTestQuestionId id = new UserTestQuestionId();
        id.setUserId(userId);
        id.setTestQuestionId(testQuestionId);

        return id;
    }

    public Long getTestQuestionId() {
        return testQuestionId;
    }

    public void setTestQuestionId(Long testQuestionId) {
        this.testQuestionId = testQuestionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserExamId{" +
                "testQuestionId=" + testQuestionId +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserTestQuestionId that = (UserTestQuestionId) o;
        return Objects.equals(testQuestionId, that.testQuestionId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testQuestionId, userId);
    }
}
