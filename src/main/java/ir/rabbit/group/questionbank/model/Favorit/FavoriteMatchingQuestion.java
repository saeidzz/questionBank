package ir.rabbit.group.questionbank.model.Favorit;

import ir.rabbit.group.questionbank.model.compositeId.UserMatchingQuestionId;
import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class FavoriteMatchingQuestion extends AbstractAuditingEntity {

    @EmbeddedId
    private UserMatchingQuestionId userMatchingQuestionId;

    public UserMatchingQuestionId getUserMatchingQuestionId() {
        return userMatchingQuestionId;
    }

    public void setUserMatchingQuestionId(UserMatchingQuestionId userMatchingQuestionId) {
        this.userMatchingQuestionId = userMatchingQuestionId;
    }
}
