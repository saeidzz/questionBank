package ir.rabbit.group.questionbank.model.connection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.user.User;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(
        name = "UHC_SEQ",
        sequenceName = "UHC_SEQ",
        allocationSize = 10,
        initialValue = 1
)
public class UserHasConnections extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UHC_SEQ")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "connectedToThisId")
    @JsonIgnore
    private User connectedToThis;


    @ManyToOne
    @JoinColumn(name = "thisConnectedToId")
    @JsonIgnore
    private User thisConnectedTo;

    private boolean accepted;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getConnectedToThis() {
        return connectedToThis;
    }

    public void setConnectedToThis(User connectedToThis) {
        this.connectedToThis = connectedToThis;
    }

    public User getThisConnectedTo() {
        return thisConnectedTo;
    }

    public void setThisConnectedTo(User thisConnectedTo) {
        this.thisConnectedTo = thisConnectedTo;
    }

    public boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserHasConnections)) return false;

        UserHasConnections that = (UserHasConnections) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
