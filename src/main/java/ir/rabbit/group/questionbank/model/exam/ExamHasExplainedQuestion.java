package ir.rabbit.group.questionbank.model.exam;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "EHEQ_SEQ", sequenceName = "EHEQ_SEQ", allocationSize = 10, initialValue = 1)
public class ExamHasExplainedQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EHEQ_SEQ")
    private Long id;


    @ManyToOne
    private ExplainedQuestion explainedQuestion;


    @ManyToOne
    private Exam exam;


    private boolean done;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ExplainedQuestion getExplainedQuestion() {
        return explainedQuestion;
    }

    public void setExplainedQuestion(ExplainedQuestion explainedQuestion) {
        this.explainedQuestion = explainedQuestion;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamHasExplainedQuestion)) return false;

        ExamHasExplainedQuestion that = (ExamHasExplainedQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
