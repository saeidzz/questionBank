package ir.rabbit.group.questionbank.model.reported;


import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.compositeId.UserTrueFalseQuestionId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table
public class ReportedTrueFalseQuestion extends AbstractAuditingEntity {
    @EmbeddedId
    private UserTrueFalseQuestionId userTrueFalseQuestionId;

    public UserTrueFalseQuestionId getUserTrueFalseQuestionId() {
        return userTrueFalseQuestionId;
    }

    public void setUserTrueFalseQuestionId(UserTrueFalseQuestionId userTrueFalseQuestionId) {
        this.userTrueFalseQuestionId = userTrueFalseQuestionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportedTrueFalseQuestion that = (ReportedTrueFalseQuestion) o;
        return Objects.equals(userTrueFalseQuestionId, that.userTrueFalseQuestionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userTrueFalseQuestionId);
    }
}
