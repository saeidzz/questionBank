package ir.rabbit.group.questionbank.model.compositeId;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class
UserExamId  implements Serializable {
    private static final long serialVersionUID = 1L;


    private Long examId;

    private Long userId;

    public static UserExamId of(Long examId, Long userId) {
        UserExamId userExamId = new UserExamId();
        userExamId.setUserId(userId);
        userExamId.setExamId(examId);

        return userExamId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "UserExamId{" +
                "examId=" + examId +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserExamId that = (UserExamId) o;
        return Objects.equals(examId, that.examId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(examId, userId);
    }
}
