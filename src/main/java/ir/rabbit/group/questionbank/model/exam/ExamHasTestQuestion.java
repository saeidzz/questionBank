package ir.rabbit.group.questionbank.model.exam;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.TestQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "EHTQ_SEQ", sequenceName = "EHTQ_SEQ", allocationSize = 10, initialValue = 1)
public class ExamHasTestQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EHTQ_SEQ")
    private Long id;


    @ManyToOne
    private TestQuestion testQuestion;


    @ManyToOne
    private Exam exam;


    private boolean done;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestQuestion getTestQuestion() {
        return testQuestion;
    }

    public void setTestQuestion(TestQuestion testQuestion) {
        this.testQuestion = testQuestion;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamHasTestQuestion)) return false;

        ExamHasTestQuestion that = (ExamHasTestQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
