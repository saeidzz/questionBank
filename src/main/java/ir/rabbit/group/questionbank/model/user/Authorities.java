package ir.rabbit.group.questionbank.model.user;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@SequenceGenerator(name = "Authorities_SEQ", sequenceName = "Authorities_SEQ", allocationSize = 10, initialValue = 1)
public class Authorities implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Authorities_SEQ")
    private long authoritiesId;


    private String userName;
    private String authority;


    public long getAuthoritiesId() {
        return authoritiesId;
    }

    public void setAuthoritiesId(long authoritiesId) {
        this.authoritiesId = authoritiesId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Authorities)) return false;

        Authorities that = (Authorities) o;

        return getAuthoritiesId() == that.getAuthoritiesId();
    }

    @Override
    public int hashCode() {
        return (int) (getAuthoritiesId() ^ (getAuthoritiesId() >>> 32));
    }
}
