package ir.rabbit.group.questionbank.model.reported;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.compositeId.UserTestQuestionId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table
public class ReportedTestQuestion extends AbstractAuditingEntity {
    @EmbeddedId
    private UserTestQuestionId userTestQuestionId;

    public UserTestQuestionId getUserTestQuestionId() {
        return userTestQuestionId;
    }

    public void setUserTestQuestionId(UserTestQuestionId userTestQuestionId) {
        this.userTestQuestionId = userTestQuestionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportedTestQuestion that = (ReportedTestQuestion) o;
        return Objects.equals(userTestQuestionId, that.userTestQuestionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userTestQuestionId);
    }
}
