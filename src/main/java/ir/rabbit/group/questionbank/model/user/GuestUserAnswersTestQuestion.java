package ir.rabbit.group.questionbank.model.user;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.TestQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "GUATQ_SEQ", sequenceName = "GUATQ_SEQ", allocationSize = 10, initialValue = 1)
public class GuestUserAnswersTestQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GUATQ_SEQ")
    private Long id;


    private String userAnswer;

    @ManyToOne
    private TestQuestion testQuestion;

    @ManyToOne
    private GuestUser guestUser;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public TestQuestion getTestQuestion() {
        return testQuestion;
    }

    public void setTestQuestion(TestQuestion testQuestion) {
        this.testQuestion = testQuestion;
    }

    public GuestUser getGuestUser() {
        return guestUser;
    }

    public void setGuestUser(GuestUser guestUser) {
        this.guestUser = guestUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuestUserAnswersTestQuestion)) return false;

        GuestUserAnswersTestQuestion that = (GuestUserAnswersTestQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
