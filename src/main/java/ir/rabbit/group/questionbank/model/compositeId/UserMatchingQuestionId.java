package ir.rabbit.group.questionbank.model.compositeId;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserMatchingQuestionId implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long matchingQuestionId;

    private Long userId;
    public static UserMatchingQuestionId of(Long matchingQuestionId, Long userId) {
        UserMatchingQuestionId id = new UserMatchingQuestionId();
        id.setUserId(userId);
        id.setMatchingQuestionId(matchingQuestionId);

        return id;
    }
    public Long getMatchingQuestionId() {
        return matchingQuestionId;
    }

    public void setMatchingQuestionId(Long matchingQuestionId) {
        this.matchingQuestionId = matchingQuestionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMatchingQuestionId that = (UserMatchingQuestionId) o;
        return Objects.equals(matchingQuestionId, that.matchingQuestionId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(matchingQuestionId, userId);
    }
}
