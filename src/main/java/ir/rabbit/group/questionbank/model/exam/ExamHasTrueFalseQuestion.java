package ir.rabbit.group.questionbank.model.exam;

import ir.rabbit.group.questionbank.model.auditing.AbstractAuditingEntity;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;

import javax.persistence.*;

@Entity
@Table
@SequenceGenerator(name = "EHTFQ_SEQ", sequenceName = "EHTFQ_SEQ", allocationSize = 10, initialValue = 1)
public class ExamHasTrueFalseQuestion extends AbstractAuditingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EHTFQ_SEQ")
    private Long id;


    @ManyToOne
    private TrueFalseQuestion trueFalseQuestion;


    @ManyToOne
    private Exam exam;

    private boolean done;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TrueFalseQuestion getTrueFalseQuestion() {
        return trueFalseQuestion;
    }

    public void setTrueFalseQuestion(TrueFalseQuestion trueFalseQuestion) {
        this.trueFalseQuestion = trueFalseQuestion;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamHasTrueFalseQuestion)) return false;

        ExamHasTrueFalseQuestion that = (ExamHasTrueFalseQuestion) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
