package ir.rabbit.group.questionbank.model.auditing;


import ir.rabbit.group.questionbank.model.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;


@Component
public class AbstractAuditingEntityListener {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    Environment env;

    @PrePersist
    public void prePersist(AbstractAuditingEntity target) {
        LocalDateTime now = LocalDateTime.now();
        target.setCreatedDate(now);
        target.setLastModifiedDate(now);
        target.setLastModifiedBy(getCurrentUserId());
        target.setCreatedBy(getCurrentUserId());
//        super.touchForCreate(target);
    }

    @PreUpdate
    public void preUpdate(AbstractAuditingEntity target) {
        LocalDateTime now = LocalDateTime.now();
        target.setLastModifiedDate(now);
        target.setLastModifiedBy(getCurrentUserId());
//        super.touchForUpdate(target);
    }

    @PreRemove
    public void preRemove(AbstractAuditingEntity target) {

    }


    private void auditLog(AbstractAuditingEntity target) {
    }

    private Long getCurrentUserId() {
        User user = null;
      /*  try {
            if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != "anonymousUser") {
                user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            }
        } catch (NullPointerException e) {
            logger.error(String.format("CanNot get user from security context %s", e.getMessage()));
        }*/
        return user == null ? -1l : user.getId();
    }
}
