package ir.rabbit.group.questionbank.exception;

import org.springframework.core.SpringProperties;

public class CanNotCreateExamFileException extends QuestionBankException {

    public CanNotCreateExamFileException(String cause) {
        super(SpringProperties.getProperty("CanNotCreateExamFileException"), cause);
    }
}
