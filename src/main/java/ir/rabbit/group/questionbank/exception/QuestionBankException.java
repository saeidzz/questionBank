package ir.rabbit.group.questionbank.exception;

public class QuestionBankException extends RuntimeException {

    private String message;
    private String cause;

    public QuestionBankException(String message, String cause) {
        this.message = message;
        this.cause = cause;
    }

    public QuestionBankException(String message, String message1, String cause) {
        super(message);
        this.message = message1;
        this.cause = cause;
    }

    public QuestionBankException(String message, Throwable cause, String message1, String cause1) {
        super(message, cause);
        this.message = message1;
        this.cause = cause1;
    }

    public QuestionBankException(Throwable cause, String message, String cause1) {
        super(cause);
        this.message = message;
        this.cause = cause1;
    }

    public QuestionBankException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String message1, String cause1) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.message = message1;
        this.cause = cause1;
    }
}
