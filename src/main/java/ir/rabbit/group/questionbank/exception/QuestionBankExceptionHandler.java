package ir.rabbit.group.questionbank.exception;



import ir.rabbit.group.questionbank.model.occuredException.OccurredException;
import ir.rabbit.group.questionbank.service.abstraction.occuredException.OccuredExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;


@ControllerAdvice
public class QuestionBankExceptionHandler {

    @Autowired
    OccuredExceptionService occuredExceptionService;

    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e) {


        ModelAndView model = new ModelAndView("error");

        Date d = new Date();
        String url = request.getRequestURI();


        OccurredException occuredException = new OccurredException();
        occuredException.setDate(d);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        occuredException.setException(sw.toString());
        occuredException.setUrl(url);
        occuredException.setRemoteUser(request.getRemoteUser());

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        occuredException.setIp(ipAddress);
        occuredExceptionService.addOccuredException(occuredException);
        return model;
    }

}
