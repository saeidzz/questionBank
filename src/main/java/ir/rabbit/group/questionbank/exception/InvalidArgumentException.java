package ir.rabbit.group.questionbank.exception;

public class InvalidArgumentException extends QuestionBankException {
    public InvalidArgumentException(String message, String cause) {
        super(message, cause);
    }
}
