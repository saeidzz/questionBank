package ir.rabbit.group.questionbank.exception;

import org.springframework.core.SpringProperties;

public class FileSizLimitExceededException extends QuestionBankException {
    public FileSizLimitExceededException(String cause) {
        super(SpringProperties.getProperty("FileSizLimitExceededException"), cause);
    }
}
