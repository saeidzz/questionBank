package ir.rabbit.group.questionbank.exception;

import org.springframework.core.SpringProperties;

public class CanNotBuildCaptchaException extends QuestionBankException {

    public CanNotBuildCaptchaException(String cause) {
        super(SpringProperties.getProperty("CanNotBuildCaptcha"), cause);
    }
}
