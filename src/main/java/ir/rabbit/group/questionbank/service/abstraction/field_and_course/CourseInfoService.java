package ir.rabbit.group.questionbank.service.abstraction.field_and_course;

import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;

import java.util.List;

public interface CourseInfoService {
    void addCourseInfo(CourseInfo courseInfo);

    CourseInfo getCourseInfoById(Long id);

    List<CourseInfo> getAllCourseInfos();

    void deleteCourseInfo(CourseInfo courseInfo);

    Long ExistCourse(CourseInfo courseInfo);
}
