package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersMatchingQuestions;

import java.util.List;

public interface UserAnswersMatchingQuestionService {
    void addUserAnswersMatchingQuestions(UserAnswersMatchingQuestions userAnswersMatchingQuestions);

    List<UserAnswersMatchingQuestions> getUserAnswerMatchingQuestionByExamAndUserId(Long userId, Long examID);

}
