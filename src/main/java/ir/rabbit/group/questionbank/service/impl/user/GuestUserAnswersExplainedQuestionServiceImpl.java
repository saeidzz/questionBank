package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersExplainedQuestion;
import ir.rabbit.group.questionbank.repository.user.GuestUserAnswersExplainedQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.GuestUserAnswersExplainedQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestUserAnswersExplainedQuestionServiceImpl implements GuestUserAnswersExplainedQuestionService {

    @Autowired
    GuestUserAnswersExplainedQuestionRepository guestUserAnswersExplainedQuestionDao;

    @Override
    public void addGuestUserAnswersEQuestion(GuestUserAnswersExplainedQuestion guestUserAnswersExplainedQuestion) {
        guestUserAnswersExplainedQuestionDao.save(guestUserAnswersExplainedQuestion);
    }

    @Override
    public void deleteGuestUserAnswersEQuestion(GuestUserAnswersExplainedQuestion guestUserAnswersExplainedQuestion) {
        guestUserAnswersExplainedQuestionDao.delete(guestUserAnswersExplainedQuestion);
    }

    @Override
    public List<GuestUserAnswersExplainedQuestion> getAllguestUserAnswersExplainedxplainedQuestion() {
        return guestUserAnswersExplainedQuestionDao.findAll();
    }

    @Override
    public List<GuestUserAnswersExplainedQuestion> getAllGuestUsersAnswersExplainedQuestionsByGPIEId(Long id) {
        return guestUserAnswersExplainedQuestionDao.findByGuestUser_Id(id);
    }
}
