package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersTestQuestion;
import ir.rabbit.group.questionbank.repository.user.GuestUserAnswersTestQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.GuestUserAnswersTestQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestUserAnswersTestQuestionServiceImpl implements GuestUserAnswersTestQuestionService {

    @Autowired
    GuestUserAnswersTestQuestionRepository guestUserAnswersTestQuestionDao;

    public void addGuestUserAnswersQuestion(GuestUserAnswersTestQuestion guestUserAnswersTestQuestion) {
        guestUserAnswersTestQuestionDao.save(guestUserAnswersTestQuestion);
    }

    public void deleteGuestUserAnswersQuestion(GuestUserAnswersTestQuestion guestUserAnswersTestQuestion) {
        guestUserAnswersTestQuestionDao.delete(guestUserAnswersTestQuestion);
    }

    public List<GuestUserAnswersTestQuestion> getAllGuestUsersAnswersTestQuestions() {
        return guestUserAnswersTestQuestionDao.findAll();
    }

    public List<GuestUserAnswersTestQuestion> getAllGuestUsersAnswersTestQuestionsByGPIEId(Long id) {
        return guestUserAnswersTestQuestionDao.findByGuestUser_Id(id);
    }
}
