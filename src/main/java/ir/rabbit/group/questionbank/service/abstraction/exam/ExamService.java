package ir.rabbit.group.questionbank.service.abstraction.exam;

import ir.rabbit.group.questionbank.model.exam.Exam;

import java.util.List;

public interface ExamService {

    void addExam(Exam exam);

    Exam getExamById(Long id);

    List<Exam> getAllExams();

    void deleteExam(Exam exam);

    void editExam(Exam exam);

    List<Exam> getExamByCreatorUser(Long userId);

    List<Exam> getPublicExams();

    List<Exam> getSpecificUserPublicExam(Long userId);

}
