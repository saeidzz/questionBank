package ir.rabbit.group.questionbank.service.abstraction.question;

import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;

import java.util.List;

public interface ExplainedQuestionService {
    void addQuestion(ExplainedQuestion question);

    ExplainedQuestion getQuestionById(Long id);

    List<ExplainedQuestion> getAllQuestions();

    void deleteQuestion(ExplainedQuestion explainedQuestion);

    void editQuestion(ExplainedQuestion question);

    List<ExplainedQuestion> getByOwnerUser(Long userId);

    List<ExplainedQuestion> getQuestionByCourseInfo(Long courseId);

    List<ExplainedQuestion> getPublicExplainedQuestions();

    List<ExplainedQuestion> getEQuestionForExam(Long userId, Long courseId);

    List<ExplainedQuestion> getSpecificUserPublicExplainedQuestions(Long userId);
}
