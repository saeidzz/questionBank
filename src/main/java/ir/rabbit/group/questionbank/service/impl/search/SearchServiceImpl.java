package ir.rabbit.group.questionbank.service.impl.search;

import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.exam.ExamFile;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.repository.abstraction.SearchDao;
import ir.rabbit.group.questionbank.service.abstraction.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {


    @Autowired
    SearchDao searchDao;

    @Override
    public List<TestQuestion> searchTestQuestion(String phrase, CourseInfo courseInfo) {
        return searchDao.searchTestQuestion(phrase, courseInfo);
    }

    @Override
    public List<ExplainedQuestion> searchExplainedQuestion(String phrase, CourseInfo courseInfo) {
        return searchDao.searchExplainedQuestion(phrase, courseInfo);
    }

    @Override
    public List<TrueFalseQuestion> searchTrueFalseQuestion(String phrase, CourseInfo courseInfo) {
        return searchDao.searchTrueFalseQuestion(phrase, courseInfo);
    }

    @Override
    public List<MatchingQuestion> searchMatchingQuestion(String phrase, CourseInfo courseInfo) {
        return searchDao.searchMatchingQuestion(phrase, courseInfo);
    }

    @Override
    public List<ExamFile> searchExamFiles(String phrase, CourseInfo courseInfo) {
        return searchDao.searchExamFiles(phrase, courseInfo);
    }

    @Override
    public List<Exam> searchExams(String phrase, CourseInfo courseInfo) {
        return searchDao.searchExams(phrase, courseInfo);
    }

    @Override
    public List<User> searchUsers(String phrase) {
        return searchDao.searchUsers(phrase);
    }
}
