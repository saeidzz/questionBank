package ir.rabbit.group.questionbank.service.abstraction.exam;

import ir.rabbit.group.questionbank.model.exam.ExamFile;

import java.util.List;


public interface ExamFileService {
    void addExamFile(ExamFile examFile);

    ExamFile getExamFileById(Long id);

    List<ExamFile> getAllExamFiles();

    void deleteExamFile(ExamFile examFile);

    void editExamFile(ExamFile examFile);

    List<ExamFile> getExamFileByOwner(Long userId);

    List<ExamFile> getPublicExamFiles();

    List<ExamFile> getExamFilesByOwnerUser(Long userId);
}
