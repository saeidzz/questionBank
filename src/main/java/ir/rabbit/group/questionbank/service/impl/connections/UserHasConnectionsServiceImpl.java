package ir.rabbit.group.questionbank.service.impl.connections;

import ir.rabbit.group.questionbank.model.connection.UserHasConnections;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.repository.abstraction.UserNativeDao;
import ir.rabbit.group.questionbank.repository.connections.UserHasConnectionsRepository;
import ir.rabbit.group.questionbank.service.abstraction.connections.UserHasConnectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserHasConnectionsServiceImpl implements UserHasConnectionsService {

    @Autowired
    UserHasConnectionsRepository userHasConnectionsDao;

    @Autowired
    UserNativeDao userNativeDao;

    @Override
    public void addUserHasConnections(UserHasConnections userHasConnections) {
        userHasConnectionsDao.save(userHasConnections);
    }

    @Override
    public List<UserHasConnections> getAllUserHasConnections() {

        return userHasConnectionsDao.findAll();
    }

    @Override
    public List<User> getDoesNotAcceptedFollowers(Long userId) {
        return userNativeDao.getDoesNotAcceptedFollowers(userId);
    }

    @Override
    public List<User> getAcceptedFollowers(Long userId) {
        return userNativeDao.getAcceptedFollowers(userId);
    }

    @Override
    public List<User> getDoesNotAcceptedFollowings(Long userId) {
        return userNativeDao.getDoesNotAcceptedFollowings(userId);
    }

    @Override
    public List<User> getAcceptedFollowings(Long userId) {
        return userNativeDao.getAcceptedFollowings(userId);
    }

    @Override
    public void removeUserHasFollowers(UserHasConnections userHasConnections) {
        userHasConnectionsDao.deleteById(userHasConnections.getId());
    }

    @Override
    public int countFollowers(Long userId) {
        return userHasConnectionsDao.countUserHasConnectionsByAcceptedTrueAndConnectedToThis_Id(userId);
    }

    @Override
    public int countFollowings(Long userId) {
        return userHasConnectionsDao.countUserHasConnectionsByAcceptedTrueAndThisConnectedTo_Id(userId);
    }

    @Override
    public UserHasConnections getUserHasConnectionByConnectorAndConnected(Long connectToThisId, Long thisWanaConnectToID) {
        return userHasConnectionsDao.findByConnectedToThis_IdAndThisConnectedTo_Id(connectToThisId, thisWanaConnectToID);
    }

    @Override
    public void deleteUserHasConnectionByConnectorAndConnected(Long connectToThisId, Long thisWanaConnectToID) {
        userHasConnectionsDao.deleteByConnectedToThis_IdAndThisConnectedTo_Id(connectToThisId, thisWanaConnectToID);
    }

    @Override
    public int countFollowRequests(Long userId) {
        return userHasConnectionsDao.countUserHasConnectionsByAcceptedFalseAndConnectedToThis_Id(userId);
    }

    @Override
    public void updateUserHasConnections(UserHasConnections userHasConnections) {
        UserHasConnections connections = userHasConnectionsDao.findByConnectedToThis_IdAndThisConnectedTo_Id(userHasConnections.getConnectedToThis().getId(), userHasConnections.getThisConnectedTo().getId());
       connections.setAccepted(userHasConnections.getAccepted());
       connections.setConnectedToThis(userHasConnections.getConnectedToThis());
       connections.setThisConnectedTo(userHasConnections.getThisConnectedTo());
        userHasConnectionsDao.save(connections);
    }

}
