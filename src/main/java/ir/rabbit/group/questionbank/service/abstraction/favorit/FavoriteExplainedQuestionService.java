package ir.rabbit.group.questionbank.service.abstraction.favorit;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;

import java.util.List;

public interface FavoriteExplainedQuestionService {
    void addToMyFavoriteExplainedQuestions(FavoriteExplainedQuestion favoriteExplainedQuestion);

    void deleteFromMyFavoriteExplainedQuestions(FavoriteExplainedQuestion favoriteExplainedQuestion);

    FavoriteExplainedQuestion getMyFavoriteExplainedQuestionsById(Long userId, Long EQuestionId);

    List<FavoriteExplainedQuestion> getAllUserFavoriteExplainedQuestions(Long userId);

    List<String> getFavoriteEQuestionIdsOfSpecificUser(Long userId);

    List<ExplainedQuestion> getFavoriteEQuestionsOfSpecificUser(Long userId);
}
