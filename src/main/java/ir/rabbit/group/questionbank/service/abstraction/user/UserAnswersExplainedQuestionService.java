package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersExplainedQuestion;

import java.util.List;

public interface UserAnswersExplainedQuestionService {
    void addUserAnswersExplainedQuestions(UserAnswersExplainedQuestion userAnswersExplainedQuestion);

    List<UserAnswersExplainedQuestion> getUserAnswersExplainedQuestions(Long userId, Long examID);

}
