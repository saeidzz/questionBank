package ir.rabbit.group.questionbank.service.abstraction.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasUser;
import ir.rabbit.group.questionbank.model.user.User;

import java.util.List;

public interface ExamHasUserService {
    void addExamHasUser(ExamHasUser examHasUser);

    List<ExamHasUser> getAllExamHasUsers();

    List<ExamHasUser> getExamHasUsersByExamID(Long examID);

    List<ExamHasUser> getExamHasUserByUserID(Long userID);

    List<ExamHasUser> getExamHasUserByUserScore(double score);

    List<ExamHasUser> getExamHasUserByExamDone();

    ExamHasUser getExamHasUserById(Long id);

    void deleteExamHasUser(Long examHasUserId);

    ExamHasUser getExamHasUserByUserAndExamID(Long userId, Long examId);

    List<User> getExamAddedUsers(Long examID);
}
