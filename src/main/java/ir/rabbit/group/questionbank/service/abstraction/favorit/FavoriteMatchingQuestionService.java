package ir.rabbit.group.questionbank.service.abstraction.favorit;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteMatchingQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;

import java.util.List;

public interface FavoriteMatchingQuestionService {
    void addToMyFavoriteMatchingQuestion(FavoriteMatchingQuestion favoriteMatchingQuestion);

    void deleteFromMyFavoriteMatchingQuestion(FavoriteMatchingQuestion favoriteMatchingQuestion);

    FavoriteMatchingQuestion getMyFavoriteMatchingQuestionById(Long userId, Long MQuestionId);

    List<FavoriteMatchingQuestion> getAllUserFavoriteMatchingQuestions(Long userId);

    List<String> getFavoriteMQuestionIdsOfSpecificUser(Long userId);

    List<MatchingQuestion> getFavoriteMQuestionsOfSpecificUser(Long userId);
}
