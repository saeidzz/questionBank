package ir.rabbit.group.questionbank.service.impl.exam;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.exam.ExamFile;
import ir.rabbit.group.questionbank.repository.exam.ExamFileRepository;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamFileServiceImpl implements ExamFileService {
    @Autowired
    ExamFileRepository examFileDao;


    public void addExamFile(ExamFile examFile) {
        examFileDao.save(examFile);
    }

    public ExamFile getExamFileById(Long id) {
        return examFileDao.findById(id).get();
    }

    public List<ExamFile> getAllExamFiles() {
        return examFileDao.findAll();
    }

    public void deleteExamFile(ExamFile examFile) {
        examFileDao.deleteById(examFile.getId());
    }

    public void editExamFile(ExamFile examFile) {
        ExamFile file = examFileDao.findById(examFile.getId()).orElseThrow(NotFoundException::new);
        examFile.setId(file.getId());
        examFileDao.save(examFile);
    }

    @Override
    public List<ExamFile> getExamFileByOwner(Long userId) {
        return examFileDao.findByUser_Id(userId);
    }

    @Override
    public List<ExamFile> getPublicExamFiles() {
        return examFileDao.findByIsPublicTrue();
    }

    @Override
    public List<ExamFile> getExamFilesByOwnerUser(Long userId) {
        return examFileDao.findByUser_Id(userId);
    }

}
