package ir.rabbit.group.questionbank.service.abstraction.favorit;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteTestQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;

import java.util.List;

public interface FavoriteTestQuestionService {
    void addToMyFavoriteTestQuestions(FavoriteTestQuestion favoriteTestQuestion);

    void deleteFromMyFavoriteTestQuestions(FavoriteTestQuestion favoriteTestQuestion);

    FavoriteTestQuestion getMyFavoriteTestQuestionsById(Long userId, Long TQuestionId);

    List<FavoriteTestQuestion> getAllUserFavoriteTestQuestions(Long userId);

    List<String> getFavoriteTQIdOfSpecificUser(Long userId);

    List<TestQuestion> getFavoriteTQuestionsOfSpecificUser(Long userId);
}
