package ir.rabbit.group.questionbank.service.impl.exam;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.exam.ExamHasTestQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TestQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.exam.ExamHasTestQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamHasTestQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamHasTestQuestionServiceImpl implements ExamHasTestQuestionService {

    @Autowired
    ExamHasTestQuestionRepository examHasTestQuestionDao;
    @Autowired
    TestQuestionNativeDao testQuestionNativeDao;

    public void addExamHasTestQuestion(ExamHasTestQuestion examHasTestQuestion) {
        examHasTestQuestionDao.save(examHasTestQuestion);
    }

    public List<ExamHasTestQuestion> getExamHasTestQuestionByExamID(Long examID) {
        return examHasTestQuestionDao.findByExam_Id(examID);
    }

    public ExamHasTestQuestion getExamHasTestQuestionByID(Long id) {
        return examHasTestQuestionDao.findById(id).orElseThrow(NotFoundException::new);
    }

    public void deleteExamHasTest(Long examHasTestqID) {
        examHasTestQuestionDao.deleteById(examHasTestqID);
    }

    @Override
    public List<TestQuestion> getExamTestQuestions(Long ExamId) {
        return testQuestionNativeDao.getExamTestQuestions(ExamId);
    }



}
