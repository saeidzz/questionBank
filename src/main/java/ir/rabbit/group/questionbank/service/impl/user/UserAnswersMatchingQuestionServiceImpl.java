package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersMatchingQuestions;
import ir.rabbit.group.questionbank.repository.user.UserAnswersMatchingQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.UserAnswersMatchingQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAnswersMatchingQuestionServiceImpl implements UserAnswersMatchingQuestionService {
    @Autowired
    UserAnswersMatchingQuestionRepository userAnswersMatchingQuestionDao;

    @Override
    public void addUserAnswersMatchingQuestions(UserAnswersMatchingQuestions userAnswersMatchingQuestions) {
        userAnswersMatchingQuestionDao.save(userAnswersMatchingQuestions);
    }

    @Override
    public List<UserAnswersMatchingQuestions> getUserAnswerMatchingQuestionByExamAndUserId(Long id, Long examID) {
        return userAnswersMatchingQuestionDao.findByUser_IdAndExam_Id(id, examID);
    }
}
