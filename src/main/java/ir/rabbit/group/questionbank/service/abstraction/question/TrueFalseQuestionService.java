package ir.rabbit.group.questionbank.service.abstraction.question;

import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;

import java.util.List;

public interface TrueFalseQuestionService {
    void addTrueFalseQuestion(TrueFalseQuestion question);

    TrueFalseQuestion getQuestionById(Long id);

    List<TrueFalseQuestion> getAllQuestions();

    void deleteQuestion(TrueFalseQuestion trueFalseQuestion);

    void editQuestion(TrueFalseQuestion question);


    List<TrueFalseQuestion> getQuestionByOwnerUser(Long userId);

    List<TrueFalseQuestion> getQuestionByCourseInfo(Long courseID);

    List<TrueFalseQuestion> getTFQuestionForExam(Long userId, Long courseId);

    List<TrueFalseQuestion> getSpecificUserPublicTrueFalseQuestion(Long userId);

    List<TrueFalseQuestion> getPublicTFQuestions();
}
