package ir.rabbit.group.questionbank.service.impl.question;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TrueFalseQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.question.TrueFalseQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.question.TrueFalseQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrueFalseQuestionServiceImpl implements TrueFalseQuestionService {
    @Autowired
    TrueFalseQuestionRepository trueFalseQuestionDao;

    @Autowired
    TrueFalseQuestionNativeDao trueFalseQuestionNativeDao;

    @Override
    public void addTrueFalseQuestion(TrueFalseQuestion question) {
        trueFalseQuestionDao.save(question);
    }

    @Override
    public TrueFalseQuestion getQuestionById(Long id) {
        return trueFalseQuestionDao.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<TrueFalseQuestion> getAllQuestions() {
        return trueFalseQuestionDao.findAll();
    }

    @Override
    public void deleteQuestion(TrueFalseQuestion trueFalseQuestion) {
        trueFalseQuestionDao.delete(trueFalseQuestion);
    }

    @Override
    public void editQuestion(TrueFalseQuestion question) {
        TrueFalseQuestion trueFalseQuestion = trueFalseQuestionDao.findById(question.getId()).orElseThrow(NotFoundException::new);
        question.setId(trueFalseQuestion.getId());
        trueFalseQuestionDao.save(question);
    }

    @Override
    public List<TrueFalseQuestion> getQuestionByOwnerUser(Long userId) {
        return trueFalseQuestionDao.findByUser_Id(userId);
    }

    @Override
    public List<TrueFalseQuestion> getQuestionByCourseInfo(Long courseID) {
        return trueFalseQuestionDao.findByCourseInfo_Id(courseID);
    }


    @Override
    public List<TrueFalseQuestion> getTFQuestionForExam(Long userId, Long courseId) {
        return trueFalseQuestionNativeDao.getTFQuestionForExam(userId, courseId);
    }

    @Override
    public List<TrueFalseQuestion> getSpecificUserPublicTrueFalseQuestion(Long userId) {
        return trueFalseQuestionDao.findByIsPublicTrueAndUser_Id(userId);
    }

    @Override
    public List<TrueFalseQuestion> getPublicTFQuestions() {
        return trueFalseQuestionDao.findByIsPublicTrue();

    }
}
