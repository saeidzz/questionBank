package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.repository.user.UserRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImp implements UserService {
    @Autowired
    private UserRepository userDao;

    public void addUser(User user) {
        userDao.save(user);
    }

    @Override
    public void deleteUser(User Student) {
        userDao.delete(Student);
    }

    public User getUserById(Long studentId) {
        return userDao.findById(studentId).orElseThrow(NotFoundException::new);
    }

    public List<User> getAllUsers() {
        return userDao.findAll();
    }

    public User getUserByUserName(String username) {
        return userDao.findByUserName(username);
    }

    @Override
    public boolean existMailAdd(String mail) {
        return userDao.existsByEmailEquals(mail);
    }

    @Override
    public boolean existUserName(String username) {
        return userDao.existsByUserNameEquals(username);
    }

    @Override
    public void editUser(User user) {
        User u = userDao.findById(user.getId()).orElseThrow(NotFoundException::new);
        user.setId(u.getId());
        userDao.save(user);
    }

}
