package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersTrueFalseQuestion;

import java.util.List;

public interface GuestUserAnswersTrueFalseQuestionService {

    void addGuestUserAnswersTrueFalseQuestion(GuestUserAnswersTrueFalseQuestion guestUserAnswersTrueFalseQuestion);

    void deleteGuestUserAnswersQuestion(GuestUserAnswersTrueFalseQuestion guestUserAnswersTrueFalseQuestion);

    List<GuestUserAnswersTrueFalseQuestion> getAllGuestUsersAnswersTrueFalseQuestion();

    List<GuestUserAnswersTrueFalseQuestion> getAllGuestUsersAnswersTrueFalseQuestionsByGPIEId(Long id);

}
