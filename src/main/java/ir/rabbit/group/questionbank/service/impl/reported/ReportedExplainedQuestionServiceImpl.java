package ir.rabbit.group.questionbank.service.impl.reported;

import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedExplainedQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.ExplainedQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.reported.ReportedExplainedQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.reported.ReportedExplainedQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportedExplainedQuestionServiceImpl implements ReportedExplainedQuestionService {

    @Autowired
    ReportedExplainedQuestionRepository reportedExplainedQuestionDao;
    @Autowired
    ExplainedQuestionNativeDao explainedQuestionNativeDao;

    @Override
    public void addToMyReportedExplainedQuestions(ReportedExplainedQuestion favoriteExplainedQuestion) {
        reportedExplainedQuestionDao.save(favoriteExplainedQuestion);
    }

    @Override
    public void deleteFromMyReportedExplainedQuestions(ReportedExplainedQuestion favoriteExplainedQuestion) {
        reportedExplainedQuestionDao.delete(favoriteExplainedQuestion);
    }

    @Override
    public List<ExplainedQuestion> getAllUserReportedExplainedQuestions() {
        return explainedQuestionNativeDao.getAllUserReportedExplainedQuestions();
    }
}
