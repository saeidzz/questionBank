package ir.rabbit.group.questionbank.service.impl.favorit;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.Favorit.FavoriteTestQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TestQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.favorite.FavoriteTestQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteTestQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteTestQuestionServiceImpl implements FavoriteTestQuestionService {


    @Autowired
    FavoriteTestQuestionRepository favoriteTestQuestionDao;

    @Autowired
    TestQuestionNativeDao testQuestionNativeDao;

    @Override
    public void addToMyFavoriteTestQuestions(FavoriteTestQuestion favoriteTestQuestion) {
        favoriteTestQuestionDao.save(favoriteTestQuestion);
    }

    @Override
    public void deleteFromMyFavoriteTestQuestions(FavoriteTestQuestion favoriteTestQuestion) {
        favoriteTestQuestionDao.delete(favoriteTestQuestion);
    }

    @Override
    public FavoriteTestQuestion getMyFavoriteTestQuestionsById(Long userId, Long TQuestionId) {
        return favoriteTestQuestionDao.findByUserTestQuestionId_UserIdAndUserTestQuestionId_TestQuestionId(userId, TQuestionId).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<FavoriteTestQuestion> getAllUserFavoriteTestQuestions(Long userId) {
        return favoriteTestQuestionDao.findByUserTestQuestionId_UserId(userId);
    }

    @Override
    public List<String> getFavoriteTQIdOfSpecificUser(Long id) {
        return favoriteTestQuestionDao.getFavoriteTQIdOfSpecificUser(id);
    }

    @Override
    public List<TestQuestion> getFavoriteTQuestionsOfSpecificUser(Long id) {
        return testQuestionNativeDao.getFavoriteTQuestionsOfSpecificUser(id);
    }
}
