package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersTrueFalseQuestion;

import java.util.List;

public interface UserAnswersTrueFalseQuestionService {
    void addUserAnswersTrueFalseQuestions(UserAnswersTrueFalseQuestion userAnswersTrueFalseQuestion);

    List<UserAnswersTrueFalseQuestion> getUserAnswersTrueFalseQuestions(Long userId, Long examID);

}
