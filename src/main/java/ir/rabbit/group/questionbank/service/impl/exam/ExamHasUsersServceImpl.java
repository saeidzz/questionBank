package ir.rabbit.group.questionbank.service.impl.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasUser;
import ir.rabbit.group.questionbank.model.user.User;
import ir.rabbit.group.questionbank.repository.abstraction.UserNativeDao;
import ir.rabbit.group.questionbank.repository.exam.ExamHasUserRepository;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamHasUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamHasUsersServceImpl implements ExamHasUserService {
    @Autowired
    ExamHasUserRepository examHasUserDao;
    @Autowired
    UserNativeDao userNativeDao;


    public void addExamHasUser(ExamHasUser examHasUser) {
        examHasUserDao.save(examHasUser);
    }

    public List<ExamHasUser> getAllExamHasUsers() {
        return examHasUserDao.findAll();
    }

    public List<ExamHasUser> getExamHasUsersByExamID(Long examID) {
        return examHasUserDao.findByExam_Id(examID);
    }

    public List<ExamHasUser> getExamHasUserByUserID(Long userID) {
        return examHasUserDao.findByUser_Id(userID);
    }

    public List<ExamHasUser> getExamHasUserByUserScore(double score) {
        return examHasUserDao.findByScoreGreaterThan(score);
    }

    public List<ExamHasUser> getExamHasUserByExamDone() {
        return examHasUserDao.findByDoneTrue();
    }

    public ExamHasUser getExamHasUserById(Long id) {
        return examHasUserDao.findById(id).orElseThrow(NoClassDefFoundError::new);
    }

    public void deleteExamHasUser(Long examHasUserID) {
        examHasUserDao.deleteById(examHasUserID);
    }

    public ExamHasUser getExamHasUserByUserAndExamID(Long userId, Long examId) {
        return examHasUserDao.findByUser_IdAndExam_Id(userId, examId);
    }

    @Override
    public List<User> getExamAddedUsers(Long examID) {
        return userNativeDao.getExamAddedUsers(examID);

    }
}
