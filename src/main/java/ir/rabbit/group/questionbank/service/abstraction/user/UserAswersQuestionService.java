package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersTestQuestion;

import java.util.List;

public interface UserAswersQuestionService {
    void addUserAnswersQuestions(UserAnswersTestQuestion userAnswersQuestion);

    List<UserAnswersTestQuestion> getUserAnswerQuestionByExamAndUserId(Long userId, Long examID);
    //void deleteUserAnswersQuestionByExamId(long examID);
    //void deleteUserAnswersQuestionByUserId(long userId);
}
