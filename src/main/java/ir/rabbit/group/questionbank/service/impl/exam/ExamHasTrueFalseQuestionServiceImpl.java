package ir.rabbit.group.questionbank.service.impl.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasTrueFalseQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TrueFalseQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.exam.ExamHasTrueFalseQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamHasTrueFalseQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamHasTrueFalseQuestionServiceImpl implements ExamHasTrueFalseQuestionService {

    @Autowired
    ExamHasTrueFalseQuestionRepository examHasTrueFalseQuestionDao;
    @Autowired
    TrueFalseQuestionNativeDao trueFalseQuestionNativeDao;


    @Override
    public void addExamHasTrueFalseQuestion(ExamHasTrueFalseQuestion examHasTrueFalseQuestion) {
        examHasTrueFalseQuestionDao.save(examHasTrueFalseQuestion);
    }

    @Override
    public List<ExamHasTrueFalseQuestion> getExamHasTrueFalseQuestionByExamID(Long examID) {
        return examHasTrueFalseQuestionDao.findByExam_Id(examID);
    }

    @Override
    public ExamHasTrueFalseQuestion getExamHasTrueFalseQuestionByID(Long id) {
        return examHasTrueFalseQuestionDao.findById(id).orElseThrow(NoClassDefFoundError::new);
    }

    @Override
    public void deleteExamHasTrueFalseQuestion(Long examHasTFQ) {
        examHasTrueFalseQuestionDao.deleteById(examHasTFQ);
    }

    @Override
    public List<TrueFalseQuestion> getExamTQuestions(Long examID) {
        return trueFalseQuestionNativeDao.getExamTQuestions(examID);
    }

}
