package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersTrueFalseQuestion;
import ir.rabbit.group.questionbank.repository.user.GuestUserAnswersTrueFalseQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.GuestUserAnswersTrueFalseQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestUserAnswersTrueFalseQuestionServiceImpl implements GuestUserAnswersTrueFalseQuestionService {
    @Autowired
    GuestUserAnswersTrueFalseQuestionRepository guestUserAnswersTrueFalseQuestionDao;

    @Override
    public void addGuestUserAnswersTrueFalseQuestion(GuestUserAnswersTrueFalseQuestion guestUserAnswersTrueFalseQuestion) {
        guestUserAnswersTrueFalseQuestionDao.save(guestUserAnswersTrueFalseQuestion);
    }

    @Override
    public void deleteGuestUserAnswersQuestion(GuestUserAnswersTrueFalseQuestion guestUserAnswersTrueFalseQuestion) {
        guestUserAnswersTrueFalseQuestionDao.delete(guestUserAnswersTrueFalseQuestion);
    }

    @Override
    public List<GuestUserAnswersTrueFalseQuestion> getAllGuestUsersAnswersTrueFalseQuestion() {
        return guestUserAnswersTrueFalseQuestionDao.findAll();
    }

    @Override
    public List<GuestUserAnswersTrueFalseQuestion> getAllGuestUsersAnswersTrueFalseQuestionsByGPIEId(Long id) {
        return guestUserAnswersTrueFalseQuestionDao.findByGuestUser_Id(id);
    }
}
