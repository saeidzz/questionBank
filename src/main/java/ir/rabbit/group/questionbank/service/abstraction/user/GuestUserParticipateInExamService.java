package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.GuestUser;

import java.util.List;

public interface GuestUserParticipateInExamService {
    void addGUPIE(GuestUser guestUser);

    void deleteGUPIE(GuestUser guestUser);

    List<GuestUser> getGuestUserByExamId(Long examId);

    List<GuestUser> getAllGuesUsers();

    GuestUser getGuesUserById(Long gUserId);

    List<String> getAllGuestUsersMails(Long examId);

}
