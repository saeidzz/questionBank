package ir.rabbit.group.questionbank.service.impl.question;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.question.MatchingNode;
import ir.rabbit.group.questionbank.repository.question.MatchingNodeRepository;
import ir.rabbit.group.questionbank.service.abstraction.question.MatchingNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatchingNodeServiceImpl implements MatchingNodeService {
    @Autowired
    MatchingNodeRepository matchingNodeDao;

    @Override
    public void addMatchingnode(MatchingNode matchingNode) {
        matchingNodeDao.save(matchingNode);
    }

    @Override
    public void deleteMatchingnode(MatchingNode matchingNode) {
        matchingNodeDao.delete(matchingNode);
    }

    @Override
    public MatchingNode getMatchingnodeById(Long MNid) {
        return matchingNodeDao.findById(MNid).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<MatchingNode> getAllmatNodes() {
        return matchingNodeDao.findAll();
    }

    @Override
    public List<MatchingNode> getAllmatNodesByMatchingQId(Long matchingQID) {
        return matchingNodeDao.findByMatchingQuestion_Id(matchingQID);
    }

}
