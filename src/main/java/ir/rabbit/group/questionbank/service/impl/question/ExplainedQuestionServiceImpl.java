package ir.rabbit.group.questionbank.service.impl.question;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.ExplainedQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.question.ExplainedQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.question.ExplainedQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExplainedQuestionServiceImpl implements ExplainedQuestionService {

    @Autowired
    ExplainedQuestionRepository explainedQuestionDao;

    @Autowired
    ExplainedQuestionNativeDao explainedQuestionNativeDao;

    public void addQuestion(ExplainedQuestion question) {
        explainedQuestionDao.save(question);
    }

    public ExplainedQuestion getQuestionById(Long id) {
        return explainedQuestionDao.findById(id).orElseThrow(NotFoundException::new);
    }

    public List<ExplainedQuestion> getAllQuestions() {
        return explainedQuestionDao.findAll();
    }

    public void deleteQuestion(ExplainedQuestion explainedQuestion) {
        explainedQuestionDao.delete(explainedQuestion);

    }

    public void editQuestion(ExplainedQuestion question) {
        ExplainedQuestion e=explainedQuestionDao.findById(question.getId()).orElseThrow(NotFoundException::new);
        question.setId(e.getId());
        explainedQuestionDao.save(question);

    }


    public List<ExplainedQuestion> getByOwnerUser(Long id) {
        return explainedQuestionDao.findByUser_Id(id);
    }

    public List<ExplainedQuestion> getQuestionByCourseInfo(Long courseID) {
        return explainedQuestionDao.findByCourseInfo_Id(courseID);
    }

    public List<ExplainedQuestion> getPublicExplainedQuestions() {
        return explainedQuestionDao.findByIsPublicTrue();
    }

    public List<ExplainedQuestion> getEQuestionForExam(Long id, Long courseId) {
        return explainedQuestionNativeDao.getExplainedQuestionForExam(id, courseId);
    }

    @Override
    public List<ExplainedQuestion> getSpecificUserPublicExplainedQuestions(Long userId) {
        return explainedQuestionDao.findByIsPublicTrueAndUser_Id(userId);
    }
}

