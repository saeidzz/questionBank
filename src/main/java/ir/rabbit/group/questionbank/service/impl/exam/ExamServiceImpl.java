package ir.rabbit.group.questionbank.service.impl.exam;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.repository.exam.ExamRepository;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamServiceImpl implements ExamService {

    @Autowired
    ExamRepository examDao;

    public void addExam(Exam exam) {
        examDao.save(exam);
    }

    public Exam getExamById(Long id) {
        return examDao.findById(id).orElseThrow(NotFoundException::new);
    }

    public List<Exam> getAllExams() {
        return examDao.findAll();
    }

    public void deleteExam(Exam exam) {
        examDao.delete(exam);
    }

    public void editExam(Exam exam) {
        Exam e = examDao.findById(exam.getId()).orElseThrow(NotFoundException::new);
        exam.setId(e.getId());
        examDao.save(exam);

    }

    public List<Exam> getExamByCreatorUser(Long userId) {
        return examDao.findByUser_Id(userId);
    }

    public List<Exam> getPublicExams() {
        return examDao.findByIsPublicTrue();
    }

    @Override
    public List<Exam> getSpecificUserPublicExam(Long userId) {
        return examDao.findByIsPublicTrueAndUser_Id(userId);
    }


}
