package ir.rabbit.group.questionbank.service.abstraction.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasTestQuestion;
import ir.rabbit.group.questionbank.model.question.TestQuestion;

import java.util.List;

public interface ExamHasTestQuestionService {

    void addExamHasTestQuestion(ExamHasTestQuestion examHasTestQuestion);

    List<ExamHasTestQuestion> getExamHasTestQuestionByExamID(Long examID);

    ExamHasTestQuestion getExamHasTestQuestionByID(Long id);

    void deleteExamHasTest(Long examHasTestqID);

    List<TestQuestion> getExamTestQuestions(Long ExamId);

}
