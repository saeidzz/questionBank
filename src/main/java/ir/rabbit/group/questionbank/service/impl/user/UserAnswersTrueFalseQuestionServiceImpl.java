package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersTrueFalseQuestion;
import ir.rabbit.group.questionbank.repository.user.UserAnswerTrueFalseQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.UserAnswersTrueFalseQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAnswersTrueFalseQuestionServiceImpl implements UserAnswersTrueFalseQuestionService {
    @Autowired
    UserAnswerTrueFalseQuestionRepository userAnswerTrueFalseQuestionDao;

    @Override
    public void addUserAnswersTrueFalseQuestions(UserAnswersTrueFalseQuestion userAnswersTrueFalseQuestion) {
        userAnswerTrueFalseQuestionDao.save(userAnswersTrueFalseQuestion);
    }

    @Override
    public List<UserAnswersTrueFalseQuestion> getUserAnswersTrueFalseQuestions(Long userId, Long examID) {
        return userAnswerTrueFalseQuestionDao.findByUser_IdAndExam_Id(userId, examID);
    }
}
