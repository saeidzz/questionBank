package ir.rabbit.group.questionbank.service.abstraction.reported;

import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedExplainedQuestion;

import java.util.List;

public interface ReportedExplainedQuestionService {
    void addToMyReportedExplainedQuestions(ReportedExplainedQuestion favoriteExplainedQuestion);

    void deleteFromMyReportedExplainedQuestions(ReportedExplainedQuestion favoriteExplainedQuestion);

    List<ExplainedQuestion> getAllUserReportedExplainedQuestions();


}
