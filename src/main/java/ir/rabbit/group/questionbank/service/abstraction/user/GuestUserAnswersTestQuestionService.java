package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersTestQuestion;

import java.util.List;

public interface GuestUserAnswersTestQuestionService {
    void addGuestUserAnswersQuestion(GuestUserAnswersTestQuestion guestUserAnswersTestQuestion);

    void deleteGuestUserAnswersQuestion(GuestUserAnswersTestQuestion guestUserAnswersTestQuestion);

    List<GuestUserAnswersTestQuestion> getAllGuestUsersAnswersTestQuestions();

    List<GuestUserAnswersTestQuestion> getAllGuestUsersAnswersTestQuestionsByGPIEId(Long id);


}
