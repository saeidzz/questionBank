package ir.rabbit.group.questionbank.service.abstraction.reported;

import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedMatchingQuestion;

import java.util.List;

public interface ReportedMatchingQuestionService {
    void addToMyReportedMatchingQuestion(ReportedMatchingQuestion reportedMatchingQuestion);

    void deleteFromMyReportedMatchingQuestion(ReportedMatchingQuestion reportedMatchingQuestion);

    List<MatchingQuestion> getAllUserReportedMatchingQuestions();

}
