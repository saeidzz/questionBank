package ir.rabbit.group.questionbank.service.abstraction.question;

import ir.rabbit.group.questionbank.model.question.MatchingNode;

import java.util.List;

public interface MatchingNodeService {
    void addMatchingnode(MatchingNode matchingNode);

    void deleteMatchingnode(MatchingNode matchingNode);

    MatchingNode getMatchingnodeById(Long MNid);

    List<MatchingNode> getAllmatNodes();

    List<MatchingNode> getAllmatNodesByMatchingQId(Long matchingQID);
}
