package ir.rabbit.group.questionbank.service.abstraction.favorit;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteTrueFalseQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;

import java.util.List;

public interface FavoriteTrueFalseQuestionService {
    void addToMyFavoriteTrueFalseQuestions(FavoriteTrueFalseQuestion favoriteTrueFalseQuestion);

    void deleteFromMyFavoriteTrueFalseQuestions(FavoriteTrueFalseQuestion favoriteTrueFalseQuestion);

    FavoriteTrueFalseQuestion getMyFavoriteTrueFalseQuestionsById(Long userId, Long TFQuestionId);

    List<FavoriteTrueFalseQuestion> getAllUserFavoriteTrueFalseQuestions(Long userId);

    List<String> getFavoriteTFQuestionIdsOfSpecificUser(Long userId);

    List<TrueFalseQuestion> getFavoriteTFQuestionsOfSpecificUser(Long userId);
}
