package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.user.GuestUser;
import ir.rabbit.group.questionbank.repository.abstraction.GuestUserNativeDao;
import ir.rabbit.group.questionbank.repository.impl.GuestUserNativeDaoImpl;
import ir.rabbit.group.questionbank.repository.user.GuestUserRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.GuestUserParticipateInExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestUserParticipateInExamServiceImpl implements GuestUserParticipateInExamService {

    @Autowired
    GuestUserRepository guestUserParticipateInExamDao;

    @Autowired
    GuestUserNativeDao guestUserNativeDao;

    public void addGUPIE(GuestUser guestUser) {
        guestUserParticipateInExamDao.save(guestUser);
    }

    public void deleteGUPIE(GuestUser guestUser) {
        guestUserParticipateInExamDao.delete(guestUser);
    }

    public List<GuestUser> getGuestUserByExamId(Long examId) {
        return guestUserParticipateInExamDao.findByExam_Id(examId);
    }


    public List<GuestUser> getAllGuesUsers() {
        return guestUserParticipateInExamDao.findAll();
    }

    public GuestUser getGuesUserById(Long gUserId) {
        return guestUserParticipateInExamDao.findById(gUserId).orElseThrow(NotFoundException::new);
    }

    public List<String> getAllGuestUsersMails(Long examId) {
        return guestUserNativeDao.getAllGuestUsersMails(examId);
    }
}
