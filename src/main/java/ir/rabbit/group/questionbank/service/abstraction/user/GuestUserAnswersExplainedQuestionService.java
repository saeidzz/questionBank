package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersExplainedQuestion;

import java.util.List;

public interface GuestUserAnswersExplainedQuestionService {
    void addGuestUserAnswersEQuestion(GuestUserAnswersExplainedQuestion guestUserAnswersExplainedQuestion);

    void deleteGuestUserAnswersEQuestion(GuestUserAnswersExplainedQuestion guestUserAnswersExplainedQuestion);

    List<GuestUserAnswersExplainedQuestion> getAllguestUserAnswersExplainedxplainedQuestion();

    List<GuestUserAnswersExplainedQuestion> getAllGuestUsersAnswersExplainedQuestionsByGPIEId(Long id);
}
