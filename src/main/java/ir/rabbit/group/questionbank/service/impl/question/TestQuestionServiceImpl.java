package ir.rabbit.group.questionbank.service.impl.question;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TestQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.question.TestQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.question.TestQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestQuestionServiceImpl implements TestQuestionService {

    @Autowired
    private TestQuestionRepository testQuestionDao;

    @Autowired
    private TestQuestionNativeDao testQuestionNativeDao;


    public List<TestQuestion> getQuestionList() {
        return testQuestionDao.findAll();
    }

    public TestQuestion getQuestionById(Long questionId) {
        return testQuestionDao.findById(questionId).orElseThrow(NotFoundException::new);
    }

    public void addQuestion(TestQuestion question) {
        testQuestionDao.save(question);
    }

    public void editQuestion(TestQuestion question) {
        TestQuestion testQuestion=testQuestionDao.findById(question.getId()).orElseThrow(NotFoundException::new);
        question.setId(testQuestion.getId());
        testQuestionDao.save(question);
    }

    public void deleteQuestion(TestQuestion question) {
        testQuestionDao.delete(question);
    }

    public List<TestQuestion> getQuestionByCourseInfo(Long courseID) {
        return testQuestionDao.findByCourseInfo_Id(courseID);
    }

    public List<TestQuestion> getPublicTestQuestions() {
        return testQuestionDao.findByIsPublicTrue();
    }

    public List<TestQuestion> getTestQuestionForExam(Long id, Long courseId) {
        return testQuestionNativeDao.getTestQuestionForExam(id, courseId);
    }

    @Override
    public List<TestQuestion> getSpecificUserPublicTestQuestions(Long userId) {
        return testQuestionDao.findByIsPublicTrueAndUser_Id(userId);
    }


    public List<TestQuestion> getByOwnerUser(Long userId) {
        return testQuestionDao.findByUser_Id(userId);
    }

}
