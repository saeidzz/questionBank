package ir.rabbit.group.questionbank.service.impl.favorit;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.Favorit.FavoriteExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.ExplainedQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.favorite.FavoriteExplainedQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteExplainedQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteExplainedQuestionServiceImpl implements FavoriteExplainedQuestionService {

    @Autowired
    FavoriteExplainedQuestionRepository favoriteExplainedQuestionDao;

    @Autowired
    ExplainedQuestionNativeDao explainedQuestionNativeDao;

    @Override
    public void addToMyFavoriteExplainedQuestions(FavoriteExplainedQuestion favoriteExplainedQuestion) {
        favoriteExplainedQuestionDao.save(favoriteExplainedQuestion);
    }

    @Override
    public void deleteFromMyFavoriteExplainedQuestions(FavoriteExplainedQuestion favoriteExplainedQuestion) {
        favoriteExplainedQuestionDao.delete(favoriteExplainedQuestion);
    }

    @Override
    public FavoriteExplainedQuestion getMyFavoriteExplainedQuestionsById(Long userId, Long EQuestionId) {
        return favoriteExplainedQuestionDao.findByUserExplainedQuestionId_UserIdAndUserExplainedQuestionId_ExplainedQuestionId(userId, EQuestionId).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<FavoriteExplainedQuestion> getAllUserFavoriteExplainedQuestions(Long userId) {
        return favoriteExplainedQuestionDao.findByUserExplainedQuestionId_UserId(userId);
    }

    @Override
    public List<String> getFavoriteEQuestionIdsOfSpecificUser(Long id) {
        return favoriteExplainedQuestionDao.getFavoriteEQuestionIdsOfSpecificUser(id);
    }

    @Override
    public List<ExplainedQuestion> getFavoriteEQuestionsOfSpecificUser(Long id) {
        return explainedQuestionNativeDao.getFavoriteEQuestionsOfSpecificUser(id);
    }
}
