package ir.rabbit.group.questionbank.service.impl.captcha;

import ir.rabbit.group.questionbank.exception.CanNotBuildCaptchaException;
import ir.rabbit.group.questionbank.service.abstraction.captcha.CaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CaptchaServiceImpl implements CaptchaService {

    @Autowired
    private ResourceLoader classPathResource;


    // Defining Character Array you can change accordingly
    private static final char[] chars = {'1', 'A', 'a', 'B', 'b', 'C',
            'c', '2', 'D', 'd', 'E', 'e', 'F', 'f', '3', 'G', 'g', 'H', 'h',
            'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', '4', 'M', 'm', 'N', 'n',
            'O', 'o', '5', 'P', 'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T', 't',
            '6', '7', 'U', 'u', 'V', 'v', 'U', 'u', 'W', 'w', '8', 'X', 'x',
            'Y', 'y', 'Z', 'z', '9'};

    private static final Color[] colors = {Color.red, Color.gray, Color.blue, Color.CYAN, Color.GREEN, Color.YELLOW};


    @Override
    // Method for generating the Captcha Code
    public String generateCaptchaText(int k) {

        String randomStrValue = "";


        final int LENGTH = k; // Character Length

        StringBuffer sb = new StringBuffer();

        int index = 0;

        for (int i = 0; i < LENGTH; i++) {
            // Getting Random Number with in range(ie: 60 total character present)
            index = (int) (Math.random() * (chars.length - 1));
            sb.append(chars[index]); // Appending the character using StringBuffer
        }

        randomStrValue = String.valueOf(sb); // Assigning the Generated Password to String variable

        return randomStrValue;
    }

    @Override

    // Method used to render the Image for Captcha
    public BufferedImage renderImage(String value) {


        BufferedImage image = null;
        try {
            image = ImageIO.read(classPathResource.getResource("classpath:captchaImages\\background" + (int) (Math.random() * 6) + ".jpg").getInputStream());

        } catch (IOException e) {
            throw new CanNotBuildCaptchaException(e.getMessage());
        }
        Graphics g = image.getGraphics();

        g.setFont(getFont("classpath:fonts").get((int) (Math.random() * 5)));
        char[] c = value.toCharArray();

        int x = 0;//57

        for (int i = 0; i < c.length; i++) {
            x = x + 30;
            g.setColor(colors[(int) (Math.random() * 6)]);
            g.drawString(String.valueOf(c[i]), x, 70 - (int) (Math.random() * 15));
        }

        g.dispose();

        return image;

    }

    private  List<Font> getFont(String rootPath) {
        File root = null;
        try {
            root = classPathResource.getResource(rootPath).getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Font> fonts = new ArrayList<>();
        if (root.canRead()) {
            File[] fontFiles = root.listFiles();

            for (File fontFile : fontFiles) {
                try {
                    fonts.add(Font.createFont(Font.TRUETYPE_FONT,  fontFile).deriveFont(Font.TRUETYPE_FONT,70f));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return fonts;

    }

}
