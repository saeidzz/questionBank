package ir.rabbit.group.questionbank.service.abstraction.question;

import ir.rabbit.group.questionbank.model.question.TestQuestion;

import java.util.List;

public interface TestQuestionService {

    List<TestQuestion> getQuestionList();

    TestQuestion getQuestionById(Long questionId);

    void addQuestion(TestQuestion question);

    void editQuestion(TestQuestion question);

    void deleteQuestion(TestQuestion question);

    List<TestQuestion> getByOwnerUser(Long userId);

    List<TestQuestion> getQuestionByCourseInfo(Long courseID);

    List<TestQuestion> getPublicTestQuestions();

    List<TestQuestion> getTestQuestionForExam(Long userId, Long courseId);

    List<TestQuestion> getSpecificUserPublicTestQuestions(Long userId);

}
