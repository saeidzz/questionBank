package ir.rabbit.group.questionbank.service.impl.favorit;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.Favorit.FavoriteTrueFalseQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TrueFalseQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.favorite.FavoriteTrueFalseQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteTrueFalseQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteTrueFalseQuestionServiceImpl implements FavoriteTrueFalseQuestionService {

    @Autowired
    FavoriteTrueFalseQuestionRepository favoriteTrueFalseQuestionDao;

    @Autowired
    TrueFalseQuestionNativeDao trueFalseQuestionNativeDao;

    @Override
    public void addToMyFavoriteTrueFalseQuestions(FavoriteTrueFalseQuestion favoriteTrueFalseQuestion) {
        favoriteTrueFalseQuestionDao.save(favoriteTrueFalseQuestion);
    }

    @Override
    public void deleteFromMyFavoriteTrueFalseQuestions(FavoriteTrueFalseQuestion favoriteTrueFalseQuestion) {
        favoriteTrueFalseQuestionDao.delete(favoriteTrueFalseQuestion);
    }

    @Override
    public FavoriteTrueFalseQuestion getMyFavoriteTrueFalseQuestionsById(Long userId, Long qId) {
        return favoriteTrueFalseQuestionDao.findByUserTrueFalseQuestionId_UserIdAndUserTrueFalseQuestionId_TrueFalseQuestionId(userId,qId).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<FavoriteTrueFalseQuestion> getAllUserFavoriteTrueFalseQuestions(Long userId) {
        return favoriteTrueFalseQuestionDao.findByUserTrueFalseQuestionId_UserId(userId);
    }

    @Override
    public List<String> getFavoriteTFQuestionIdsOfSpecificUser(Long id) {
        return favoriteTrueFalseQuestionDao.getFavoriteTFQuestionIdsOfSpecificUser(id);
    }

    @Override
    public List<TrueFalseQuestion> getFavoriteTFQuestionsOfSpecificUser(Long id) {

        return trueFalseQuestionNativeDao.getFavoriteTFQuestionsOfSpecificUser(id);
    }
}
