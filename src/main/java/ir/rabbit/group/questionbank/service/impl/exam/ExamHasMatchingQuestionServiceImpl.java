package ir.rabbit.group.questionbank.service.impl.exam;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.exam.ExamHasMatchingQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.MatchingQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.exam.ExamHasMatchingQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamHasMatchingQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamHasMatchingQuestionServiceImpl implements ExamHasMatchingQuestionService {
    @Autowired
    ExamHasMatchingQuestionRepository examHasMatchingQuestionDao;

    @Autowired
    MatchingQuestionNativeDao matchingQuestionNativeDao;

    @Override
    public void addExamHasMatchingQuestion(ExamHasMatchingQuestion examHasMatchingQuestion) {
        examHasMatchingQuestionDao.save(examHasMatchingQuestion);
    }

    @Override
    public List<ExamHasMatchingQuestion> getExamHasMQuestionByExamID(Long examID) {
        return examHasMatchingQuestionDao.findByExam_Id(examID);
    }

    @Override
    public ExamHasMatchingQuestion getExamHasMatchingQuestionByID(Long id) {
        return examHasMatchingQuestionDao.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public void deleteExamHasMatchingQuestionById(ExamHasMatchingQuestion examHasMatchingQuestion) {
        examHasMatchingQuestionDao.delete(examHasMatchingQuestion);
    }

    @Override
    public List<MatchingQuestion> getExamMQuestions(Long examID) {
        return matchingQuestionNativeDao.getExamMatchingQuestions(examID);

    }
}
