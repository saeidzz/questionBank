package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersTestQuestion;
import ir.rabbit.group.questionbank.repository.user.UserAnswersQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.UserAswersQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAnswersQuestionserviceImpl implements UserAswersQuestionService {
    @Autowired
    UserAnswersQuestionRepository userAnswersQuestionDao;

    public void addUserAnswersQuestions(UserAnswersTestQuestion userAnswersQuestion) {
        userAnswersQuestionDao.save(userAnswersQuestion);
    }

    public List<UserAnswersTestQuestion> getUserAnswerQuestionByExamAndUserId(Long userId, Long examID) {
        return userAnswersQuestionDao.findByUser_IdAndExam_Id(userId, examID);
    }
}
