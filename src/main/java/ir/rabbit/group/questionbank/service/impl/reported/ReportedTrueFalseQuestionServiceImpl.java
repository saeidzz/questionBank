package ir.rabbit.group.questionbank.service.impl.reported;

import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedTrueFalseQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TrueFalseQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.reported.ReportedTrueFalseQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.reported.ReportedTrueFalseQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportedTrueFalseQuestionServiceImpl implements ReportedTrueFalseQuestionService {
    @Autowired
    ReportedTrueFalseQuestionRepository reportedTrueFalseQuestionDao;

    @Autowired
    TrueFalseQuestionNativeDao trueFalseQuestionNativeDao;

    @Override
    public void addToMyReportedTrueFalseQuestions(ReportedTrueFalseQuestion reportedTrueFalseQuestion) {
        reportedTrueFalseQuestionDao.save(reportedTrueFalseQuestion);
    }

    @Override
    public void deleteFromMyReportedTrueFalseQuestions(ReportedTrueFalseQuestion reportedTrueFalseQuestion) {
        reportedTrueFalseQuestionDao.delete(reportedTrueFalseQuestion);
    }

    @Override
    public List<TrueFalseQuestion> getAllUserReportedTrueFalseQuestions() {
        return trueFalseQuestionNativeDao.getAllUserReportedTrueFalseQuestions();
    }
}
