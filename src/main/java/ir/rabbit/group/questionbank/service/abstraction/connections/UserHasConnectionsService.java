package ir.rabbit.group.questionbank.service.abstraction.connections;

import ir.rabbit.group.questionbank.model.connection.UserHasConnections;
import ir.rabbit.group.questionbank.model.user.User;

import java.util.List;

public interface UserHasConnectionsService {
    void addUserHasConnections(UserHasConnections userHasConnections);

    List<UserHasConnections> getAllUserHasConnections();

    List<User> getDoesNotAcceptedFollowers(Long userId);

    List<User> getAcceptedFollowers(Long userId);

    List<User> getDoesNotAcceptedFollowings(Long userId);

    List<User> getAcceptedFollowings(Long userId);

    void removeUserHasFollowers(UserHasConnections userHasConnections);

    int countFollowers(Long userId);

    int countFollowings(Long userId);

    UserHasConnections getUserHasConnectionByConnectorAndConnected(Long connectToThisId, Long thisWanaConnectToID);

    void deleteUserHasConnectionByConnectorAndConnected(Long connectToThisId, Long thisWanaConnectToID);

    int countFollowRequests(Long userId);

    void updateUserHasConnections(UserHasConnections userHasConnections);
}
