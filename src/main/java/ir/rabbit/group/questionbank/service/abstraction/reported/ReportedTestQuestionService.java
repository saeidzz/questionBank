package ir.rabbit.group.questionbank.service.abstraction.reported;

import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedTestQuestion;

import java.util.List;

public interface ReportedTestQuestionService {
    void addToMy(ReportedTestQuestion reportedTestQuestion);

    void deleteFromMyReportedTestQuestion(ReportedTestQuestion reportedTestQuestion);


    List<TestQuestion> getAllUserReportedTestQuestion();


}
