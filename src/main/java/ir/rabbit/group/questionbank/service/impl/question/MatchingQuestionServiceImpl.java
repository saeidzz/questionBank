package ir.rabbit.group.questionbank.service.impl.question;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.question.MatchingNode;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.MatchingQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.question.MatchingNodeRepository;
import ir.rabbit.group.questionbank.repository.question.MatchingQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.question.MatchingQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatchingQuestionServiceImpl implements MatchingQuestionService {
    @Autowired
    MatchingQuestionRepository matchingQuestionDao;

    @Autowired
    MatchingQuestionNativeDao matchingQuestionNativeDao;

    @Autowired
    MatchingNodeRepository matchingNodeRepository;

    @Override
    public void addMatchingQuestion(MatchingQuestion question) {
        matchingQuestionDao.save(question);
    }

    @Override
    public MatchingQuestion getQuestionById(Long id) {
        return matchingQuestionDao.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<MatchingQuestion> getAllQuestions() {
        return matchingQuestionDao.findAll();
    }

    @Override
    public void deleteQuestion(MatchingQuestion matchingQuestion) {
        matchingQuestionDao.delete(matchingQuestion);
    }

    @Override
    public void editQuestion(MatchingQuestion question) {
        MatchingQuestion matchingQuestion=matchingQuestionDao.findById(question.getId()).orElseThrow(NotFoundException::new);
        question.setId(matchingQuestion.getId());
        matchingQuestionDao.save(question);
    }

    @Override
    public List<MatchingQuestion> getQuestionByOwnerUser(Long userId) {
        return matchingQuestionDao.findByUser_Id(userId);
    }

    @Override
    public List<MatchingQuestion> getQuestionByCourseInfo(Long courseID) {
        return matchingQuestionDao.findByCourseInfo_Id(courseID);
    }

    @Override
    public List<MatchingQuestion> getPublicMQuestions() {
        return matchingQuestionDao.findByIsPublicTrue();
    }

    @Override
    public List<MatchingQuestion> getMQuestionForExam(Long userId, Long courseId) {
        return matchingQuestionNativeDao.getMQuestionForExam(userId, courseId);
    }

    @Override
    public List<MatchingQuestion> getSpecificUserPublicMatchingQuestions(Long userId) {

        return matchingQuestionDao.findByIsPublicTrueAndUser_Id(userId);
    }

    @Override
    public List<MatchingNode> getMatchingNodeList(Long mQid) {
        return matchingNodeRepository.findByMatchingQuestion_Id(mQid);
    }


}
