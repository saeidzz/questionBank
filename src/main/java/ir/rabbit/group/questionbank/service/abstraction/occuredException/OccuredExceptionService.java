package ir.rabbit.group.questionbank.service.abstraction.occuredException;

import ir.rabbit.group.questionbank.model.occuredException.OccurredException;

import java.util.List;

public interface OccuredExceptionService {
    void addOccuredException(OccurredException e);

    List<OccurredException> getAllOccuredExceptions();
}
