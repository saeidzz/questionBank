package ir.rabbit.group.questionbank.service.abstraction.reported;

import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.reported.ReportedExam;

import java.util.List;

public interface ReportedExamService {
    void addToMyReportedExams(ReportedExam favoriteExam);

    void deleteFromMyReportedExams(ReportedExam favoriteExam);

    List<Exam> getAllUserReportedExams();


}
