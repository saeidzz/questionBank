package ir.rabbit.group.questionbank.service.abstraction.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasTrueFalseQuestion;
import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;

import java.util.List;

public interface ExamHasTrueFalseQuestionService {
    void addExamHasTrueFalseQuestion(ExamHasTrueFalseQuestion examHasTrueFalseQuestion);

    List<ExamHasTrueFalseQuestion> getExamHasTrueFalseQuestionByExamID(Long examID);

    ExamHasTrueFalseQuestion getExamHasTrueFalseQuestionByID(Long id);

    void deleteExamHasTrueFalseQuestion(Long examHasTFQ);

    List<TrueFalseQuestion> getExamTQuestions(Long examID);


}
