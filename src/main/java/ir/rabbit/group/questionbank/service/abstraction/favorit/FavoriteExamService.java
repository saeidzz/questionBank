package ir.rabbit.group.questionbank.service.abstraction.favorit;

import ir.rabbit.group.questionbank.model.Favorit.FavoriteExam;
import ir.rabbit.group.questionbank.model.exam.Exam;

import java.util.List;

public interface FavoriteExamService {
    void addToMyFavoriteExams(FavoriteExam favoriteExam);

    void deleteFromMyFavoriteExams(FavoriteExam favoriteExam);

    FavoriteExam getMyFavoriteExamById(Long userId, Long examId);

    List<FavoriteExam> getAllUserFavoriteExams(Long userId);

    List<String> getFavoriteExamsIdOfSpecificUser(Long userId);

    List<Exam> getFavoriteExamsOfSpecificUser(Long userId);

}
