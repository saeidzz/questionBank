package ir.rabbit.group.questionbank.service.impl.reported;

import ir.rabbit.group.questionbank.model.question.TestQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedTestQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.TestQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.reported.ReportedTestQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.reported.ReportedTestQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportedTestQuestionServiceImpl implements ReportedTestQuestionService {

    @Autowired
    ReportedTestQuestionRepository reportedTestQuestionDao;

    @Autowired
    TestQuestionNativeDao testQuestionNativeDao;
    @Override
    public void addToMy(ReportedTestQuestion reportedTestQuestion) {
        reportedTestQuestionDao.save(reportedTestQuestion);
    }

    @Override
    public void deleteFromMyReportedTestQuestion(ReportedTestQuestion reportedTestQuestion) {
        reportedTestQuestionDao.delete(reportedTestQuestion);
    }

    @Override
    public List<TestQuestion> getAllUserReportedTestQuestion() {
        return testQuestionNativeDao.getAllUserReportedTestQuestion();
    }
}
