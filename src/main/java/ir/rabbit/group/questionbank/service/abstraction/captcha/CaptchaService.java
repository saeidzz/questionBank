package ir.rabbit.group.questionbank.service.abstraction.captcha;

import java.awt.image.BufferedImage;
import java.io.IOException;

public interface CaptchaService {

    String generateCaptchaText(int k);

    BufferedImage renderImage(String value) throws IOException;


}
