package ir.rabbit.group.questionbank.service.impl.field_and_course;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.course_and_field.CourseInfo;
import ir.rabbit.group.questionbank.repository.courseinfo.CourseInfoRepository;
import ir.rabbit.group.questionbank.service.abstraction.field_and_course.CourseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseInfoServiceImpl implements CourseInfoService {
    @Autowired
    private CourseInfoRepository courseInfoDao;

    public void addCourseInfo(CourseInfo courseInfo) {
        courseInfoDao.save(courseInfo);
    }

    public CourseInfo getCourseInfoById(Long id) {
        return courseInfoDao.findById(id).orElseThrow(NotFoundException::new);
    }

    public List<CourseInfo> getAllCourseInfos() {
        return courseInfoDao.findAll();
    }

    public void deleteCourseInfo(CourseInfo courseInfo) {
        courseInfoDao.delete(courseInfo);
    }

    public Long ExistCourse(CourseInfo courseInfo) {
        return courseInfoDao.findByTitleEquals(courseInfo.getTitle()).getId();
    }
}
