package ir.rabbit.group.questionbank.service.abstraction.question;

import ir.rabbit.group.questionbank.model.question.MatchingNode;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;

import java.util.List;

public interface MatchingQuestionService {
    void addMatchingQuestion(MatchingQuestion question);

    MatchingQuestion getQuestionById(Long id);

    List<MatchingQuestion> getAllQuestions();

    void deleteQuestion(MatchingQuestion matchingQuestion);

    void editQuestion(MatchingQuestion question);

    List<MatchingQuestion> getQuestionByOwnerUser(Long userId);

    List<MatchingQuestion> getQuestionByCourseInfo(Long courseID);

    List<MatchingQuestion> getPublicMQuestions();

    List<MatchingQuestion> getMQuestionForExam(Long userId, Long courseId);

    List<MatchingQuestion> getSpecificUserPublicMatchingQuestions(Long userId);

    List<MatchingNode> getMatchingNodeList(Long mQid);
}
