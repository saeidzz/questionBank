package ir.rabbit.group.questionbank.service.abstraction.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasMatchingQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;

import java.util.List;

public interface ExamHasMatchingQuestionService {
    void addExamHasMatchingQuestion(ExamHasMatchingQuestion examHasMatchingQuestion);

    List<ExamHasMatchingQuestion> getExamHasMQuestionByExamID(Long examID);

    ExamHasMatchingQuestion getExamHasMatchingQuestionByID(Long id);

    void deleteExamHasMatchingQuestionById(ExamHasMatchingQuestion examHasMatchingQuestion);

    List<MatchingQuestion> getExamMQuestions(Long examID);
}
