package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.User;

import java.util.List;

public interface UserService {
    void addUser(User Student);

    void deleteUser(User Student);

    User getUserById(Long StudentId);

    List<User> getAllUsers();

    User getUserByUserName(String username);

    boolean existMailAdd(String mail);

    boolean existUserName(String username);

    void editUser(User user);
}
