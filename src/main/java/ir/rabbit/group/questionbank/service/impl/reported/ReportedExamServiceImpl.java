package ir.rabbit.group.questionbank.service.impl.reported;

import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.model.reported.ReportedExam;
import ir.rabbit.group.questionbank.repository.abstraction.ExamNativeDao;
import ir.rabbit.group.questionbank.repository.reported.ReportedExamRepository;
import ir.rabbit.group.questionbank.service.abstraction.reported.ReportedExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportedExamServiceImpl implements ReportedExamService {

    @Autowired
    ReportedExamRepository reportedExamDao;
    @Autowired
    ExamNativeDao examNativeDao;

    @Override
    public void addToMyReportedExams(ReportedExam favoriteExam) {
        reportedExamDao.save(favoriteExam);
    }

    @Override
    public void deleteFromMyReportedExams(ReportedExam favoriteExam) {
        reportedExamDao.delete(favoriteExam);
    }

    @Override
    public List<Exam> getAllUserReportedExams() {
        return examNativeDao.getAllUserReportedExams();
    }
}
