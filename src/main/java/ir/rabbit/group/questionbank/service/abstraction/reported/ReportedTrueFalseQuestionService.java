package ir.rabbit.group.questionbank.service.abstraction.reported;

import ir.rabbit.group.questionbank.model.question.TrueFalseQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedTrueFalseQuestion;

import java.util.List;

public interface ReportedTrueFalseQuestionService {
    void addToMyReportedTrueFalseQuestions(ReportedTrueFalseQuestion reportedTrueFalseQuestion);

    void deleteFromMyReportedTrueFalseQuestions(ReportedTrueFalseQuestion reportedTrueFalseQuestion);

    List<TrueFalseQuestion> getAllUserReportedTrueFalseQuestions();

}
