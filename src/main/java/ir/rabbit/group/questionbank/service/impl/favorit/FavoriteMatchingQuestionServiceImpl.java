package ir.rabbit.group.questionbank.service.impl.favorit;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.Favorit.FavoriteMatchingQuestion;
import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.MatchingQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.favorite.FavoriteMatchingQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteMatchingQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteMatchingQuestionServiceImpl implements FavoriteMatchingQuestionService {

    @Autowired
    FavoriteMatchingQuestionRepository favoriteMatchingQuestionDao;

    @Autowired
    MatchingQuestionNativeDao matchingQuestionNativeDao;

    @Override
    public void addToMyFavoriteMatchingQuestion(FavoriteMatchingQuestion favoriteMatchingQuestion) {
        favoriteMatchingQuestionDao.save(favoriteMatchingQuestion);
    }

    @Override
    public void deleteFromMyFavoriteMatchingQuestion(FavoriteMatchingQuestion favoriteMatchingQuestion) {
        favoriteMatchingQuestionDao.delete(favoriteMatchingQuestion);
    }

    @Override
    public FavoriteMatchingQuestion getMyFavoriteMatchingQuestionById(Long userId, Long MQuestionId) {
        return favoriteMatchingQuestionDao.findByUserMatchingQuestionId_UserIdAndUserMatchingQuestionId_MatchingQuestionId(userId, MQuestionId).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<FavoriteMatchingQuestion> getAllUserFavoriteMatchingQuestions(Long userId) {
        return favoriteMatchingQuestionDao.findByUserMatchingQuestionId_UserId(userId);
    }

    @Override
    public List<String> getFavoriteMQuestionIdsOfSpecificUser(Long id) {
        return favoriteMatchingQuestionDao.getFavoriteMQuestionIdsOfSpecificUser(id);
    }

    @Override
    public List<MatchingQuestion> getFavoriteMQuestionsOfSpecificUser(Long userId) {
        return matchingQuestionNativeDao.getFavoriteMQuestionsOfSpecificUser(userId);
    }
}
