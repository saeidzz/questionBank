package ir.rabbit.group.questionbank.service.impl.exam;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.exam.ExamHasExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.ExplainedQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.exam.ExamHasExplainedQuestionsRepository;
import ir.rabbit.group.questionbank.service.abstraction.exam.ExamHasExplainedQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamHasExplainedQuestionServiceImpl implements ExamHasExplainedQuestionService {
    @Autowired
    ExamHasExplainedQuestionsRepository examHasExplainedQuestionsDao;
    @Autowired
    ExplainedQuestionNativeDao explainedQuestionNativeDao;

    public void addExamHasExpalinQuestions(ExamHasExplainedQuestion examHasExpalinQuestions) {
        examHasExplainedQuestionsDao.save(examHasExpalinQuestions);
    }

    public List<ExamHasExplainedQuestion> getExamHasExpalinQuestionsByExam(Long examHasExplainedQID) {
        return examHasExplainedQuestionsDao.findByExam_Id(examHasExplainedQID);
    }

    public ExamHasExplainedQuestion getExamHasExpalinQuestionsByID(Long id) {
        return examHasExplainedQuestionsDao.findById(id).orElseThrow(NotFoundException::new);
    }

    public void deleteExamHasExpalinQuestions(Long examHasExpalinQuestionsID) {
        examHasExplainedQuestionsDao.deleteById(examHasExpalinQuestionsID);
    }

    @Override
    public List<ExplainedQuestion> getExamEQuestions(Long examID) {
        return explainedQuestionNativeDao.getExamExplainedQuestions(examID);
    }



}
