package ir.rabbit.group.questionbank.service.impl.reported;

import ir.rabbit.group.questionbank.model.question.MatchingQuestion;
import ir.rabbit.group.questionbank.model.reported.ReportedMatchingQuestion;
import ir.rabbit.group.questionbank.repository.abstraction.MatchingQuestionNativeDao;
import ir.rabbit.group.questionbank.repository.reported.ReportedMatchingQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.reported.ReportedMatchingQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportedMatchingQuestionServiceImpl implements ReportedMatchingQuestionService {

    @Autowired
    ReportedMatchingQuestionRepository reportedMatchingQuestionDao;

    @Autowired
    MatchingQuestionNativeDao matchingQuestionNativeDao;

    @Override
    public void addToMyReportedMatchingQuestion(ReportedMatchingQuestion reportedMatchingQuestion) {
        reportedMatchingQuestionDao.save(reportedMatchingQuestion);
    }

    @Override
    public void deleteFromMyReportedMatchingQuestion(ReportedMatchingQuestion reportedMatchingQuestion) {
        reportedMatchingQuestionDao.delete(reportedMatchingQuestion);
    }

    @Override
    public List<MatchingQuestion> getAllUserReportedMatchingQuestions() {
        return matchingQuestionNativeDao.getAllUserReportedMatchingQuestions();
    }
}
