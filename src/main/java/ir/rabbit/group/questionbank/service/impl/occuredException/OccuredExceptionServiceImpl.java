package ir.rabbit.group.questionbank.service.impl.occuredException;

import ir.rabbit.group.questionbank.model.occuredException.OccurredException;
import ir.rabbit.group.questionbank.repository.occuredException.OccurredExceptionRepository;
import ir.rabbit.group.questionbank.service.abstraction.occuredException.OccuredExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OccuredExceptionServiceImpl implements OccuredExceptionService {

    @Autowired
    OccurredExceptionRepository occurredExceptionDao;

    @Override
    public void addOccuredException(OccurredException e) {
        occurredExceptionDao.save(e);
    }

    @Override
    public List<OccurredException> getAllOccuredExceptions() {
        return occurredExceptionDao.findAll();
    }
}
