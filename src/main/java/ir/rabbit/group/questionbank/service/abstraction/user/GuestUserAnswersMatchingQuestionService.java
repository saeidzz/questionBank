package ir.rabbit.group.questionbank.service.abstraction.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersMatchingQuestion;

import java.util.List;

public interface GuestUserAnswersMatchingQuestionService {
    void addGuestUserAnswersMQuestion(GuestUserAnswersMatchingQuestion guestUserAnswersMatchingQuestion);

    void deleteGuestUserAnswersQuestion(GuestUserAnswersMatchingQuestion guestUserAnswersMatchingQuestion);

    List<GuestUserAnswersMatchingQuestion> getAllGuestUsersAnswersMatchingQuestions();

    List<GuestUserAnswersMatchingQuestion> getAllGuestUsersAnswersMatchingQuestionsByGPIEId(Long id);

}
