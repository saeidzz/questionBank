package ir.rabbit.group.questionbank.service.impl.favorit;

import ir.rabbit.group.questionbank.exception.NotFoundException;
import ir.rabbit.group.questionbank.model.Favorit.FavoriteExam;
import ir.rabbit.group.questionbank.model.exam.Exam;
import ir.rabbit.group.questionbank.repository.abstraction.ExamNativeDao;
import ir.rabbit.group.questionbank.repository.favorite.FavoriteExamRepository;
import ir.rabbit.group.questionbank.service.abstraction.favorit.FavoriteExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteExamServiceImpl implements FavoriteExamService {


    @Autowired
    FavoriteExamRepository favoriteExamDao;

    @Autowired
    ExamNativeDao examNativeDao;

    @Override
    public void addToMyFavoriteExams(FavoriteExam favoriteExam) {
        favoriteExamDao.save(favoriteExam);
    }

    @Override
    public void deleteFromMyFavoriteExams(FavoriteExam favoriteExam) {
        favoriteExamDao.delete(favoriteExam);
    }

    @Override
    public FavoriteExam getMyFavoriteExamById(Long userId, Long examId) {
        return favoriteExamDao.findByUserExamId_UserIdAndUserExamId_ExamId(userId,examId).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<FavoriteExam> getAllUserFavoriteExams(Long userId) {
        return favoriteExamDao.findByUserExamId_UserId(userId);
    }

    @Override
    public List<String> getFavoriteExamsIdOfSpecificUser(Long id) {
        return favoriteExamDao.getFavoriteExamsIdOfSpecificUser(id);
    }

    @Override
    public List<Exam> getFavoriteExamsOfSpecificUser(Long id) {
        return examNativeDao.getFavoriteExamsOfSpecificUser(id);
    }
}
