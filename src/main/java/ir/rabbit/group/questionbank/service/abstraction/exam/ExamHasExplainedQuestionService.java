package ir.rabbit.group.questionbank.service.abstraction.exam;

import ir.rabbit.group.questionbank.model.exam.ExamHasExplainedQuestion;
import ir.rabbit.group.questionbank.model.question.ExplainedQuestion;

import java.util.List;

public interface ExamHasExplainedQuestionService {
    void addExamHasExpalinQuestions(ExamHasExplainedQuestion examHasExpalinQuestions);

    List<ExamHasExplainedQuestion> getExamHasExpalinQuestionsByExam(Long examHasExplainedQID);

    ExamHasExplainedQuestion getExamHasExpalinQuestionsByID(Long id);

    void deleteExamHasExpalinQuestions(Long examHasExpalinQuestionsID);

    List<ExplainedQuestion> getExamEQuestions(Long examID);

}
