package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.GuestUserAnswersMatchingQuestion;
import ir.rabbit.group.questionbank.repository.user.GuestUserAnswersMatchingQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.GuestUserAnswersMatchingQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestUserAnswersMatchingQuestionServiceImpl implements GuestUserAnswersMatchingQuestionService {

    @Autowired
    GuestUserAnswersMatchingQuestionRepository guestUserAnswersMatchingQuestionDao;

    @Override
    public void addGuestUserAnswersMQuestion(GuestUserAnswersMatchingQuestion guestUserAnswersMatchingQuestion) {
        guestUserAnswersMatchingQuestionDao.save(guestUserAnswersMatchingQuestion);
    }

    @Override
    public void deleteGuestUserAnswersQuestion(GuestUserAnswersMatchingQuestion guestUserAnswersMatchingQuestion) {
        guestUserAnswersMatchingQuestionDao.delete(guestUserAnswersMatchingQuestion);
    }

    @Override
    public List<GuestUserAnswersMatchingQuestion> getAllGuestUsersAnswersMatchingQuestions() {
        return guestUserAnswersMatchingQuestionDao.findAll();
    }

    @Override
    public List<GuestUserAnswersMatchingQuestion> getAllGuestUsersAnswersMatchingQuestionsByGPIEId(Long id) {
        return guestUserAnswersMatchingQuestionDao.findByGuestUser_Id(id);
    }
}
