package ir.rabbit.group.questionbank.service.impl.user;

import ir.rabbit.group.questionbank.model.user.UserAnswersExplainedQuestion;
import ir.rabbit.group.questionbank.repository.user.UserAnswersExplainedQuestionRepository;
import ir.rabbit.group.questionbank.service.abstraction.user.UserAnswersExplainedQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAnswersExplainedQuestionServiceImpl implements UserAnswersExplainedQuestionService {

    @Autowired
    UserAnswersExplainedQuestionRepository userAnswersExplainedQuestionDao;

    @Override
    public void addUserAnswersExplainedQuestions(UserAnswersExplainedQuestion userAnswersExplainedQuestion) {
        userAnswersExplainedQuestionDao.save(userAnswersExplainedQuestion);
    }

    @Override
    public List<UserAnswersExplainedQuestion> getUserAnswersExplainedQuestions(Long userId, Long examID) {
        return userAnswersExplainedQuestionDao.findByUser_IdAndExam_Id(userId, examID);
    }
}
